# Setting up SQLite database files for development

When the app starts up (or the integration tests run), `better-sqlite3` will
create the database file if it does not exist and set up the needed tables.
