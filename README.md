# About

This is a work-in-progress desktop application for contra dance callers, to help
them manage their dance collections and create dance programs. It works with
[the Caller's Box](https://www.ibiblio.org/contradance/thecallersbox/)
website and its extensive database of dances.

The name is just a temporary working title, and will likely change.

Free software, licensed under the GNU General Public License (GPL3).

Built with Electron, TypeScript, React, Mobx, SQLite, Bulma, Ramda.

# Development

Documentation for developers is work-in-progress, so please reach
out if you'd like to contribute. Send an email to Paul W. Morris at his address
as follows (without any spaces or parentheses): (first name) at (first name)
(middle initial, just a single letter, no period) (last name) dot com

To build the app, you will first need to have installed relatively recent
versions of Node.js (e.g. 12.14.1) and npm (e.g. 6.14.2).

To get the source code, clone the git repo:

```
git clone https://gitlab.com/paulmorris/the-callers-app.git
```

Then move to the root directory of the repo. Except where noted the commands
shown here should be run from that directory.

```
cd the-callers-app/
```

Install dependencies:

```
npm install
```

SQLite3 is a native Node.js module so it can't be used directly with Electron
without rebuilding it to target Electron. Rebuild it:

```
./node_modules/.bin/electron-rebuild  -f -w better-sqlite3
```

To build the app (mostly compiling TypeScript to JavaScript):

```
npm run build
```

To build and run the app all in one go:

```
npm run start
```

To run all the tests:

```
npm run test:all
```

To run just unit tests:

```
npm run test:unit
```

To run just integration tests:

```
npm run test:integration
```

To run Prettier on all the TypeScript files in the project:

```
prettier --write "**/*.ts"
prettier --write "**/*.tsx"
```
