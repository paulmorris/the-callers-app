/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import Database from "better-sqlite3"
import { TableSpec, JsType } from "../../../shared/types"

type DbRow<T> = Record<keyof T, any>

export const db = new Database("database/database.db", {
  verbose: console.log,
})

/**
 * Create a database table if it doesn't exist yet.
 *
 * @param tableSpec - Spec for the table.
 * @return Result of running the creation statement.
 */
export const dbCreateTableIfNotExists = <T>(
  tableSpec: TableSpec<T>,
): Database.RunResult => {
  const colEntries = [...tableSpec.columns.entries()]

  const columns = colEntries
    .map(([colName, { sqlType, defaultValue }]) => {
      return `${colName} ${sqlType}${
        defaultValue ? ` DEFAULT "${defaultValue}"` : ""
      }`
    })
    .join(", ")

  const primaryKeys = colEntries
    .map(([colName, { primaryKey }]) => (primaryKey ? colName : false))
    .filter(Boolean)

  const sql = `
    CREATE TABLE IF NOT EXISTS ${tableSpec.name} (
      ${columns}
      ${primaryKeys.length && `, PRIMARY KEY ( ${primaryKeys.join(", ")} )`}
      ${tableSpec.foreignKeys ? `, ${tableSpec.foreignKeys}` : ""})`

  const statement = db.prepare(sql)
  const info = statement.run()
  return info
}

export const dbClearTable = <T>(tableSpec: TableSpec<T>) => {
  const dropTable = db.prepare(`DROP TABLE ${tableSpec.name}`)

  const transaction = db.transaction(() => {
    const info = dropTable.run()
    dbCreateTableIfNotExists(tableSpec)
  })
  transaction()
}

// Data Conversions, Incoming and Outgoing
// When data enters and leaves the database it needs to be converted:
//
//   JS <--> SQL
//   -----------
//   false <--> 0
//   true <--> 1
//   undefined <--> null
//   object, array, etc. <--> JSON string
//
// A null SQL value may just be deleted/omitted when converting from SQL to JS.

/**
 * Get values from a record (object) for storing in the database table,
 * converting them as needed. If a value is missing from the record, use the
 * default value from the table spec, if there is one.
 *
 * @param tableSpec - Spec for the database table.
 * @param record - An object with data to be stored.
 * @return The values to store in the database.
 */
export const dbGetValuesForStorage = <T, P extends Partial<T>>(
  tableSpec: TableSpec<T>,
  record: P,
): Array<string | number | null> => {
  const columns = [...tableSpec.columns.entries()]

  const values = columns.map(([colName, { jsType, defaultValue }]) => {
    const val = record[colName]
    return val ? convertValueForStorage(jsType, val) : defaultValue || null
  })

  return values
}

/**
 * Prepares a single value for storage in the database, converting it as needed.
 *
 * @param tableSpec - Spec for the database table.
 * @param column - A database column.
 * @param value - A value to be stored.
 * @return The value, converted if necessary.
 */
export const dbGetOneValueForStorage = <T>(
  tableSpec: TableSpec<T>,
  column: keyof T,
  value: any,
): string | number | null => {
  const colData = tableSpec.columns.get(column)
  return colData ? convertValueForStorage(colData.jsType, value) : null
}

/**
 * Convert a value for storage in the database.
 *
 * @param jsType - The type of conversion to use, if any.
 * @param value - The value to convert.
 * @return The converted value.
 */
const convertValueForStorage = (
  jsType: JsType,
  value: any,
): string | number | null => {
  if (value === undefined) {
    return null
  } else if (jsType === "json") {
    return maybeStringifyJson(value)
  } else if (jsType === "boolean") {
    return value ? 1 : 0
  }
  return value
}

/**
 * Converts a value to a JSON string before storing it in the database. For
 * `null` and `undefined` return `null` so we don't store them as strings.
 *
 * @param value - The value to convert.
 * @return A JSON string or null.
 */
export const maybeStringifyJson = (value: any): string | null => {
  return value === undefined || value === null ? null : JSON.stringify(value)
}

/**
 * Convert database row data into JS values, where conversion is needed,
 * e.g. booleans and json.
 *
 * @param tableSpec - Spec of the database table where the row comes from.
 * @param row - The row data to convert.
 * @return An object with row data in JS formats.
 */
export const dbRowToJs = <T>(tableSpec: TableSpec<T>, row: DbRow<T>): T => {
  for (const [colName, { jsType }] of tableSpec.columns.entries()) {
    if (row[colName] !== undefined) {
      if (row[colName] === null) {
        delete row[colName]
      } else if (jsType === "json") {
        row[colName] = JSON.parse(row[colName])
      } else if (jsType === "boolean") {
        row[colName] = Boolean(row[colName])
      }
    }
  }
  return row
}

/**
 * Create an insertion statement for a given table.
 *
 * @param tableSpec - Spec for the database table.
 */
export const dbPrepareInsertStatement = <T>(tableSpec: TableSpec<T>) => {
  const size = tableSpec.columns.size
  const values = Array(size).fill("?", 0, size).join(", ")
  return db.prepare(`INSERT INTO ${tableSpec.name} VALUES ( ${values} )`)
}

/**
 * Insert an item into a database table with default values.
 *
 * @param tableName The table.
 * @return The row ID for the added item.
 */
export const insertDefaultItem = (tableName: "Dances" | "Programs"): number => {
  const insert = db.prepare(`INSERT INTO ${tableName} DEFAULT VALUES;`)
  let info = insert.run()
  if (info.changes === 1) {
    const rowid = info.lastInsertRowid as number
    return rowid
  }
  throw new Error("Item was not inserted into the database")
}

/**
 * Delete an item in a database table.
 * @param tableName The table.
 * @param idColumn The column containing the ID / primary key.
 * @param id The ID.
 */
export const deleteItem = (tableName: string, idColumn: string, id: number) => {
  const statement = db.prepare(`DELETE FROM ${tableName} WHERE ${idColumn} = ?`)
  statement.run(id)
}

/**
 * Export and download the database to a JSON file.
 */
export const dbExport = () => {}
