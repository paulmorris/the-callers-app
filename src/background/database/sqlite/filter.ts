/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import { Dance, DancePart, SearchQuery } from "../../../shared/types"

export interface FilterFunction {
  (dance: Dance): boolean
}

/**
 * Return a filter function for filtering dance records. The returned function
 * may run multiple filter functions.
 *
 * @param query - Search query data that's been parsed from the search form.
 * @return A filter function or null.
 */
export function makeFilterFunction(
  query: SearchQuery,
): FilterFunction | undefined {
  const filters = filterFunctionsFromQuery(query)

  if (filters.length === 0) {
    return undefined
  } else if (filters.length === 1) {
    return filters[0]
  } else {
    return (dance) => {
      for (let fn of filters) {
        if (!fn(dance)) {
          // A filter function did not succeed (returned false).
          return false
        }
      }
      // All the filter functions succeeded (returned true).
      return true
    }
  }
}

/**
 * Assemble filter functions needed for a given query.
 *
 * @param query - Search query data that's been parsed from the search form.
 * @return An array of filter functions.
 */
function filterFunctionsFromQuery(query: SearchQuery): FilterFunction[] {
  const filters = []

  const modesToFilters = {
    any_any: anyLinesAnyOrder,
    all_any: allLinesAnyOrder,
    all_given: allLinesGivenOrder,
  }

  if (query.pos_lines) {
    const fn = modesToFilters[query.pos_mode]
    const searchLines: RegExp[] = query.pos_lines.map((ln) => RegExp(ln, "i"))

    filters.push(fn.bind(undefined, searchLines, true))
  }
  if (query.neg_lines) {
    const fn = modesToFilters[query.neg_mode]
    const searchLines: RegExp[] = query.neg_lines.map((ln) => RegExp(ln, "i"))

    filters.push(fn.bind(undefined, searchLines, false))
  }

  return filters
}

/**
 * Used to query dances by their figures (any lines in any order). The first two
 * params are typically bound to produce a FilterFunction.
 *
 * @param searchLines - Regexes from text entered into figures search input.
 * @param success - What counts as a successful boolean return value. Allows
 *                  us to "reverse the polarity" and use the same function for
 *                  positive and negative, (inclusive and exclusive) searches.
 * @param dance - The dance to check.
 * @return Whether any lines matched in any order.
 */
function anyLinesAnyOrder(
  searchLines: RegExp[],
  success: boolean = true,
  dance: Dance,
): boolean {
  if (!dance.figures.length && !dance.tcbFigures.length) {
    return false
  }
  const matched =
    (dance.figures.length &&
      anyLinesAnyOrderMatch(searchLines, dance.figures)) ||
    (dance.tcbFigures.length &&
      anyLinesAnyOrderMatch(searchLines, dance.tcbFigures))

  return matched ? success : !success
}

/**
 * Determines whether any of the search lines match the dance lines (figures),
 * in any order.
 *
 * @param searchLines - Regexes from text entered into figures search input.
 * @param danceLines - Dance figures to match against.
 * @return Whether any lines matched, in any order.
 */
function anyLinesAnyOrderMatch(
  searchLines: RegExp[],
  danceLines: DancePart[],
): boolean {
  for (const searchLine of searchLines) {
    for (const danceLine of danceLines) {
      if (searchLine.test(danceLine.text)) {
        // A search line matched.
        return true
      }
    }
  }
  // No search lines matched.
  return false
}

/**
 * Used to query dances by their figures (all lines in any order). The first two
 * params are typically bound to produce a FilterFunction.
 *
 * @param searchLines - Regexes from text entered into figures search input.
 * @param success - What counts as a successful boolean return value. Allows
 *                  us to "reverse the polarity" and use the same function for
 *                  positive and negative, (inclusive and exclusive) searches.
 * @param dance - The dance to check.
 * @return Whether all lines matched in any order.
 */
function allLinesAnyOrder(
  searchLines: RegExp[],
  success: boolean = true,
  dance: Dance,
): boolean {
  if (!dance.figures.length && !dance.tcbFigures.length) {
    return false
  }
  const matched =
    (dance.figures.length &&
      allLinesAnyOrderMatch(searchLines, dance.figures)) ||
    (dance.tcbFigures.length &&
      allLinesAnyOrderMatch(searchLines, dance.tcbFigures))

  return matched ? success : !success
}

/**
 * Determines whether all the search lines match the dance lines (figures),
 * in any order.
 *
 * @param searchLines - Regexes from text entered into figures search input.
 * @param danceLines - Dance figures to match against.
 * @return Whether all lines matched, in any order.
 */
function allLinesAnyOrderMatch(
  searchLines: RegExp[],
  danceLines: DancePart[],
): boolean {
  for (const searchLine of searchLines) {
    let matchFound = false
    for (const danceLine of danceLines) {
      if (searchLine.test(danceLine.text)) {
        // A search line matched.
        matchFound = true
        break
      }
    }
    if (!matchFound) {
      // No matches were found for one of the regexes.
      return false
    }
  }
  // All search lines matched.
  return true
}

/**
 * Used to query dances by their figures (all lines in given order). The first
 * two params are typically bound to produce a FilterFunction.
 *
 * @param searchLines - Regexes from text entered into figures search input.
 * @param success - What counts as a successful boolean return value. Allows
 *                  us to "reverse the polarity" and use the same function for
 *                  positive and negative, (inclusive and exclusive) searches.
 * @param dance - The dance to check.
 * @return Whether all search lines matched in order.
 */
function allLinesGivenOrder(
  searchLines: RegExp[],
  success: boolean = true,
  dance: Dance,
): boolean {
  if (!dance.figures.length && !dance.tcbFigures.length) {
    return false
  }
  const matched =
    (dance.figures.length &&
      allLinesGivenOrderMatch(searchLines, dance.figures)) ||
    (dance.tcbFigures.length &&
      allLinesGivenOrderMatch(searchLines, dance.tcbFigures))

  return matched ? success : !success
}

/**
 * Determines whether all the search lines match the dance lines (figures),
 * in the given order.
 *
 * @param searchLines - Regexes from text entered into figures search input.
 * @param danceLines - Dance figures to match against.
 * @return Whether all search lines matched in order.
 */
function allLinesGivenOrderMatch(
  searchLines: RegExp[],
  danceLines: DancePart[],
): boolean {
  let index = 0
  for (const danceLine of danceLines) {
    const matched = searchLines[index].test(danceLine.text)
    if (matched) {
      if (index === searchLines.length - 1) {
        // All search lines matched.
        return true
      }
      index += 1
    }
  }
  return false
}
