/*
Copyright 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import { db } from "./database"
import { getDefaultDanceAttributes } from "./defaultDanceAttributes"

export interface SettingsTableRow {
  id: number
  danceAttributes: string
}

const id = "1"

// Make sure table exists so statements can be prepared below.
// Create a table with only one primary key, so there is only ever one row.
const createTable = db.prepare(`
  CREATE TABLE IF NOT EXISTS Settings (
    id INTEGER PRIMARY KEY DEFAULT ${id} CHECK (id = ${id}),
    danceAttributes TEXT NOT NULL DEFAULT '${JSON.stringify(
      getDefaultDanceAttributes(),
    )}'
  )
`)
createTable.run()

export const dbClearSettingsTable = () => {
  const dropTable = db.prepare(`DROP TABLE Settings`)
  const transaction = db.transaction(() => {
    dropTable.run()
    createTable.run()
  })
  transaction()
}

export const getSettings = (): SettingsTableRow => {
  const settings: SettingsTableRow = selectSettings.get()
  return settings || initializeTheOnlyRow()
}

const selectSettings = db.prepare(`SELECT * FROM Settings WHERE id is ${id}`)

const initializeTheOnlyRow = (): SettingsTableRow => {
  const insert = db.prepare(`INSERT INTO Settings DEFAULT VALUES;`)
  insert.run()
  return selectSettings.get()
}

export const setDanceAttributes = (attributes: string): number => {
  const info = updateSettings.run(attributes)
  return info.changes
}

const updateSettings = db.prepare(
  `UPDATE Settings SET danceAttributes = ? WHERE id is ${id}`,
)
