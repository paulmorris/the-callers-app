/*
Copyright 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import R from "ramda"
import {
  isDance,
  isProgramSpacer,
  Program,
  ProgramDanceDbRow,
  ProgramItem,
  ProgramPatch,
  ProgramSpacer,
  TableSpec,
} from "../../../shared/types"

import {
  dbGetDanceById,
  db,
  dbCreateTableIfNotExists,
  dbClearTable,
  dbPrepareInsertStatement,
  dbGetOneValueForStorage,
} from "../../background-modules"

const programsTableSpec: TableSpec<Program> = {
  name: "Programs",
  columns: new Map([
    ["programId", { sqlType: "INTEGER NOT NULL", primaryKey: true }],
    ["location", { sqlType: "TEXT" }],
    ["date", { sqlType: "TEXT" }],
    ["music", { sqlType: "TEXT" }],
    ["notes", { sqlType: "TEXT" }],
  ]),
}

const programsDancesTableSpec: TableSpec<ProgramDanceDbRow> = {
  name: "ProgramsDances",
  columns: new Map([
    ["programDanceId", { sqlType: "INTEGER NOT NULL", primaryKey: true }],
    ["programId", { sqlType: "INTEGER NOT NULL" }],
    ["danceId", { sqlType: "INTEGER" }],
    ["danceIndex", { sqlType: "INTEGER NOT NULL" }],
    ["spacer", { sqlType: "INTEGER", jsType: "boolean" }],
  ]),
  foreignKeys: `
    FOREIGN KEY (programId)
      REFERENCES Programs (programId)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    FOREIGN KEY (danceId)
      REFERENCES Dances (danceId)
        ON UPDATE CASCADE
        ON DELETE CASCADE`,
}

dbCreateTableIfNotExists(programsTableSpec)
dbCreateTableIfNotExists(programsDancesTableSpec)

export const clearProgramsTables = (): void => {
  dbClearTable(programsTableSpec)
  dbClearTable(programsDancesTableSpec)
}

/**
 * Delete a program from database tables.
 * @param programId
 * @return 1 if the delete was successful, 0 if not.
 */
export const deleteProgram = db.transaction((programId: number): number => {
  const resultOne = deleteProgramFromPrograms.run(programId)
  const resultTwo = deleteProgramFromProgramsDances.run(programId)
  return resultOne.changes === 1 && resultTwo.changes === 1 ? 1 : 0
})

const deleteProgramFromPrograms = db.prepare(
  `DELETE FROM Programs WHERE programId = ?`,
)
const deleteProgramFromProgramsDances = db.prepare(`
  DELETE FROM ProgramsDances WHERE programId = ?`)

/**
 * Update a program, including both basic properties and dances list.
 * @param patch
 * @return 1 if the update was successful, 0 if not.
 */
export const updateProgram = db.transaction((patch: ProgramPatch): number => {
  if (patch.changes.dances) {
    updateProgramDances(patch.programId, patch.changes.dances)
  }

  const changesSubset = R.omit(["dances", "programId"], patch.changes)
  const entries = Object.entries(changesSubset)
  if (entries.length > 0) {
    const placeholders = entries.map(([column]) => `${column} = ?`)

    const values = entries.map((entry) => {
      // TS Placation.
      const column = entry[0] as keyof Omit<Program, "programId" | "dances">
      const value = entry[1]
      return dbGetOneValueForStorage(programsTableSpec, column, value)
    })

    const statement = db.prepare(
      `UPDATE Programs SET ${placeholders.join(", ")} WHERE programId = ${
        patch.programId
      }`,
    )
    const info = statement.run(values)
    return info.changes
  }
  return 0
})

/**
 * Restore a program's dances to a given state, e.g. to revert any changes.
 * - Delete all ProgramDanceDbRow rows for the program.
 * - Re-create all ProgramDanceDbRow rows.
 */
export const updateProgramDances = db.transaction(
  (programId: number, programItems: ProgramItem[]): void => {
    deleteProgramFromProgramsDances.run(programId)

    programItems.forEach((item, index) => {
      if (isProgramSpacer(item)) {
        addSpacerToProgram(programId, index)
      } else {
        addDanceToProgram(programId, item.dance.danceId, index)
      }
    })
  },
)

/**
 * Get all programs, not including dances lists, e.g. for listing them.
 */
export const getAllPrograms = (): Program[] => selectAllPrograms.all()
const selectAllPrograms = db.prepare("SELECT * FROM Programs")

/**
 * Get the number of dances and spacers in a given program.
 * @param programId
 * @return The number of dances and spacers.
 */
const getItemsCountForProgram = (programId: number): number => {
  return itemsCountForProgramId.get(programId)
}
const itemsCountForProgramId = db
  .prepare("SELECT COUNT(*) FROM ProgramsDances WHERE programId = ?")
  .pluck()

/**
 * Add a dance to a program.
 * @param programId
 * @param danceId
 * @return The number of dances inserted, 1 or 0.
 */
export const addDanceToProgram = db.transaction(
  (
    programId: number,
    danceId: number,
    index = getItemsCountForProgram(programId),
  ): number => {
    const info = insertProgramsDancesRow.run(
      null,
      programId,
      danceId,
      index,
      null,
    )
    return info.changes
  },
)

/**
 * Add a spacer to a program.
 * @param programId
 * @return 1 if the spacer was added, or 0 if not.
 */
export const addSpacerToProgram = db.transaction(
  (programId: number, index = getItemsCountForProgram(programId)): number => {
    const info = insertProgramsDancesRow.run(null, programId, null, index, 1)
    return info.changes
  },
)

const insertProgramsDancesRow = dbPrepareInsertStatement(
  programsDancesTableSpec,
)

const shiftDanceIndexesMinusOne = db.prepare(`
  UPDATE ProgramsDances
    SET danceIndex = danceIndex - 1
    WHERE programId = ?
    AND danceIndex >= ?
    AND danceIndex <= ?;`)

const shiftDanceIndexesPlusOne = db.prepare(`
  UPDATE ProgramsDances
    SET danceIndex = danceIndex + 1
    WHERE programId = ?
    AND danceIndex >= ?
    AND danceIndex <= ?;`)

const shiftDanceIndexesMinusOneToEnd = db.prepare(`
  UPDATE ProgramsDances
    SET danceIndex = danceIndex - 1
    WHERE programId = ?
    AND danceIndex >= ?`)

const shiftDanceIndexesPlusOneToEnd = db.prepare(`
  UPDATE ProgramsDances
    SET danceIndex = danceIndex + 1
    WHERE programId = ?
    AND danceIndex >= ?`)

const updateDanceIndex = db.prepare(`
  UPDATE ProgramsDances
    SET danceIndex = ?
    WHERE programId = ?
    AND danceIndex = ?`)

const getDanceIndex = db
  .prepare(
    `SELECT danceIndex FROM ProgramsDances
       WHERE programId = ?
       AND danceId = ?`,
  )
  .pluck()

const setDanceIndex = db.prepare(`
    UPDATE ProgramsDances
      SET danceIndex = ?
      WHERE programId = ?
      AND danceIndex = ?`)

/**
 * Remove a dance or spacer from a program.
 * @param programDanceId
 */
export const removeItemFromProgram = db.transaction(
  (programDanceId: number): number => {
    // TODO: error handling
    const { programId, danceIndex } = getProgramsDancesRowById.get(
      programDanceId,
    )
    const deleteInfo = deleteProgramsDancesRow.run(programDanceId)
    const shiftInfo = shiftDanceIndexesMinusOneToEnd.run(programId, danceIndex)

    return deleteInfo.changes === 1 && shiftInfo.changes == 1 ? 1 : 0
  },
)

const getProgramsDancesRowById = db.prepare(`
  SELECT * FROM ProgramsDances WHERE programDanceId = ?`)

const deleteProgramsDancesRow = db.prepare(`
    DELETE FROM ProgramsDances
      WHERE programDanceId = ?`)

/**
 * Get all the dances for a given program.
 * @param programId
 */
export const getDancesForProgramId = (programId: number): ProgramItem[] => {
  const rows = getRowsForProgramId.all(programId)
  // TODO: Better to use some kind of SQL JOIN for this.
  const dances = []
  for (const row of rows) {
    if (row.spacer === 1) {
      dances.push(makeSpacer(row.programDanceId))
    } else {
      const dance = dbGetDanceById(row.danceId)
      // Elsewhere we only allow full dance records to be added to programs,
      // so the isDance check is just TS placation.
      if (dance && isDance(dance)) {
        dances.push({
          dance,
          programDanceId: row.programDanceId,
        })
      } else {
        console.error(
          `Dance not found in database or is not full record, ID: ${row.danceId}`,
        )
      }
    }
  }
  return dances
}

export const getProgram = (programId: number): Program => {
  const program = getProgramsRowById.get(programId)
  program.dances = getDancesForProgramId(programId)
  return program
}
const getProgramsRowById = db.prepare(`
  SELECT * FROM Programs WHERE programId = ?`)

const makeSpacer = (programDanceId: number): ProgramSpacer => {
  return {
    programDanceId,
    spacer: true,
  }
}

const getRowsForProgramId = db.prepare(`
  SELECT * FROM ProgramsDances
    WHERE programId = ?
    ORDER BY danceIndex ASC
  `)

/**
 * Move a dance in a program, adjusting all the indexes accordingly.
 * @param programId
 * @param newIndex
 * @param oldIndex
 */
export const moveDanceInProgram = db.transaction(
  (programId: number, newIndex: number, oldIndex: number): void => {
    // TODO: error handling
    const distance = newIndex - oldIndex
    const setInfo1 = setDanceIndex.run(-100, programId, oldIndex)

    if (distance < 0) {
      // Dance moves up, others move down (plus one).
      const shiftInfo = shiftDanceIndexesPlusOne.run(
        programId,
        newIndex,
        oldIndex - 1,
      )
    } else if (distance > 0) {
      // Dance moves down, others move up (minus one).
      const shiftInfo = shiftDanceIndexesMinusOne.run(
        programId,
        oldIndex + 1,
        newIndex,
      )
    }
    const setInfo2 = setDanceIndex.run(newIndex, programId, -100)
  },
)

export const searchPrograms = (text: string): Program[] => {
  const text2 = `%${text}%`
  const programs = searchProgramsStatement.all(text2, text2, text2, text2)
  return programs
}

const searchProgramsStatement = db.prepare(`
  SELECT * FROM Programs
    WHERE date LIKE ?
    OR location LIKE ?
    OR music LIKE ?
    OR notes LIKE ?
`)

/**
 * Get all program rows that have a given dance.
 */
export const getProgramsForDanceId = (danceId: number): Program[] => {
  const programIds: number[] = getProgramIdsForDanceId.pluck().all(danceId)

  if (programIds.length === 0) {
    return []
  }
  const getProgramsRowsByIds = db.prepare(`
    SELECT * FROM Programs
      WHERE programId IN (${programIds.join(", ")})
      ORDER BY date DESC
  `)

  return getProgramsRowsByIds.all()
}

const getProgramIdsForDanceId = db.prepare(`
  SELECT DISTINCT programId FROM ProgramsDances
    WHERE danceId = ?
`)
