/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import {
  Dance,
  DancePatch,
  SearchQuery,
  LocalSearchResults,
  SearchResultsDance,
  SearchResultsDanceNoId,
  FiguresType,
  TableSpec,
  StorageStatus,
} from "../../../shared/types"
import { compareByTitle, tokenize } from "../../../shared/utils"
import {
  makeFilterFunction,
  FilterFunction,
  makeWhereClause,
  db,
  dbClearTable,
  dbCreateTableIfNotExists,
  dbPrepareInsertStatement,
  dbGetValuesForStorage,
  dbGetOneValueForStorage,
  dbRowToJs,
  getProgramsForDanceId,
} from "../../background-modules"

const dancesTableSpec: TableSpec<Dance> = {
  name: "Dances",
  columns: new Map([
    ["danceId", { sqlType: "INTEGER NOT NULL", primaryKey: true }],
    ["tcbId", { sqlType: "TEXT UNIQUE" }],
    ["title", { sqlType: "TEXT" }],
    ["authors", { sqlType: "TEXT", jsType: "json" }],
    ["formationBase", { sqlType: "TEXT" }],
    ["status", { sqlType: "TEXT" }],
    ["hasFigures", { sqlType: "INTEGER", jsType: "boolean" }],
    ["hasLink", { sqlType: "INTEGER", jsType: "boolean" }],
    ["hasVideo", { sqlType: "INTEGER", jsType: "boolean" }],
    ["storageStatus", { sqlType: "TEXT" }],
    ["permission", { sqlType: "TEXT" }],
    ["formationDetail", { sqlType: "TEXT" }],
    ["figures", { sqlType: "TEXT", jsType: "json" }],
    ["progression", { sqlType: "TEXT" }],
    ["direction", { sqlType: "TEXT" }],
    ["otherTitles", { sqlType: "TEXT", jsType: "json" }],
    ["basedOn", { sqlType: "TEXT", jsType: "json" }],
    ["mixer", { sqlType: "TEXT" }],
    ["phraseStructure", { sqlType: "TEXT" }],
    ["music", { sqlType: "TEXT", jsType: "json" }],
    ["callingNotes", { sqlType: "TEXT", jsType: "json" }],
    ["appearances", { sqlType: "TEXT", jsType: "json" }],
    ["videos", { sqlType: "TEXT", jsType: "json" }],
    ["variantVideos", { sqlType: "TEXT", jsType: "json" }],
    ["tcbFigures", { sqlType: "TEXT", jsType: "json" }],
    ["dateAdded", { sqlType: "TEXT" }],
    ["tcbBackup", { sqlType: "TEXT", jsType: "json" }],
    ["tags", { sqlType: "TEXT", jsType: "json" }],
    [
      "attributes",
      { sqlType: "TEXT NOT NULL", jsType: "json", defaultValue: "{}" },
    ],
  ]),
}
// Make sure dances table exists so statements can be prepared below.
dbCreateTableIfNotExists(dancesTableSpec)

export const dbClearDancesTable = dbClearTable.bind(undefined, dancesTableSpec)

/**
 * @return tcbIds of all TCB dances in the local collection.
 */
export const dbGetAllTcbIds = (): string[] => {
  return tcbIdsForStorageStatus.all("", "in")
}

const tcbIdsForStorageStatus = db
  .prepare(
    `SELECT tcbId FROM Dances WHERE tcbId IS NOT ? AND storageStatus IS ?`,
  )
  .pluck()

/**
 * @return The number of dances "in the local collection".
 */
const dbGetDanceCount = (): number => {
  return countForStorageStatus.get("in")
}

const countForStorageStatus = db
  .prepare(`SELECT COUNT(*) FROM Dances WHERE storageStatus is ?`)
  .pluck()

/**
 * Retrieve dances from the database.
 */

const danceById = db.prepare(`SELECT * FROM Dances WHERE danceId IS ?`)

const selectStatementDanceBy = (idType: string): string => `
  SELECT danceId, tcbId, title, authors, formationBase, status, hasFigures,
         hasLink, hasVideo, storageStatus
  FROM Dances WHERE ${idType} is ?`

const searchResultsDanceById = db.prepare(selectStatementDanceBy("danceId"))
const searchResultsDanceByTcbId = db.prepare(selectStatementDanceBy("tcbId"))

/**
 * Retrieve a dance from the database by its danceId.
 *
 * @param danceId - A danceId.
 * @return A dance.
 */
export const dbGetDanceById = (
  danceId: number,
): Dance | SearchResultsDance | undefined => {
  const row = danceById.get(danceId)
  const dance = row ? dbRowToJs(dancesTableSpec, row) : undefined
  if (dance) {
    dance.programs = getProgramsForDanceId(dance.danceId)
  }
  return dance
}

export const dbGetSearchResultsDanceById = (
  danceId: number,
): SearchResultsDance | undefined => {
  const row = searchResultsDanceById.get(danceId)
  return row ? dbRowToJs(dancesTableSpec, row) : undefined
}

export const dbGetSearchResultsDanceByTcbId = (
  tcbId: string,
): SearchResultsDance | undefined => {
  const row = searchResultsDanceByTcbId.get(tcbId)
  return row ? dbRowToJs(dancesTableSpec, row) : undefined
}

const danceStorageStatus = db
  .prepare(`SELECT storageStatus FROM Dances WHERE danceId is ?`)
  .pluck()

export const getDanceStorageStatus = (danceId: number): StorageStatus => {
  return danceStorageStatus.get(danceId)
}

/**
 * Search the local database with user input from the search form.
 *
 * @param query - The search query data.
 * @return Object with the search results and total number of "in" dances.
 */
export const dbSearchForDances = (query: SearchQuery): LocalSearchResults => {
  const filterFunction = makeFilterFunction(query)
  const filterColumns = filterFunction ? ", figures, tcbFigures" : ""

  const totalLocalDances = dbGetDanceCount()

  const [whereClause, whereParams] = makeWhereClause(query)

  const sqlSelect = `
    SELECT danceId, tcbId, title, authors, formationBase, status, hasFigures,
           hasLink, hasVideo, storageStatus ${filterColumns}
    FROM Dances ${whereClause}`

  const localDances = filterFunction
    ? dbSearchForDancesWithFilter(sqlSelect, whereParams, filterFunction)
    : dbSearchForDancesNoFilter(sqlSelect, whereParams)

  localDances.sort(compareByTitle)

  return { totalLocalDances, localDances }
}

/**
 * Searches the local database for dances. Takes a filter function e.g. to
 * handle complex figures queries.  Use `db.each` to check each result against
 * the filter function to avoid loading all results into memory at once and
 * then not using many of them.
 *
 * @param sqlSelect - The search query.
 * @param filter - A function to further filter the dance results.
 * @return An array of dance records.
 */
const dbSearchForDancesWithFilter = (
  sqlSelect: string,
  whereParams: string[],
  filter: FilterFunction,
): Dance[] => {
  const statement = db.prepare(sqlSelect)
  const rowsIterator = statement.iterate(whereParams)
  const foundDances = []

  for (const row of rowsIterator) {
    // As an optimization, we could convert only these JSON columns before
    // calling the filter function: authors, figures, tcbFigures.  And then the
    // rest of the JSON columns after?
    const dance = dbRowToJs(dancesTableSpec, row)
    if (filter(dance)) {
      dance.figures = []
      dance.tcbFigures = []
      foundDances.push(dance)
    }
  }

  return foundDances
}

/**
 * Search the local database for dances, without any post-search filtering.
 * Used when figures are not part of the query.
 *
 * @param sqlSelect - The search query.
 * @return An array of dance records.
 */
const dbSearchForDancesNoFilter = (
  sqlSelect: string,
  whereParams: string[],
): Dance[] => {
  const statement = db.prepare(sqlSelect)
  const rows = statement.all(whereParams)

  for (const row of rows) {
    dbRowToJs(dancesTableSpec, row)
  }
  return rows
}

/**
 * Search the figures in the database and return matching figures ordered by
 * frequency of occurrence so more common figures are listed first.
 *
 * @param figuresType - Type of figures to search, e.g. "figures", "tcbFigures".
 * @param query - The terms to search for.
 * @return Figures strings and count, e.g. [["(8) Partner swing", 34], ...]
 */
export const dbSearchForFigures = (
  figuresType: FiguresType,
  query: string,
): [string, number][] => {
  const searchParams = tokenize(query).map((token) => `%${token}%`)
  const searchClause = searchParams.map((token) => `AND atom LIKE ?`).join(" ")

  const sqlSelect = `
    SELECT atom, COUNT(atom)
    FROM Dances, json_tree(Dances.${figuresType})
    WHERE atom IS NOT NULL
      AND json_tree.key='text'
      ${searchClause}
    GROUP BY atom
    ORDER BY COUNT(atom) DESC;
  `
  const statement = db.prepare(sqlSelect)
  const rows = statement.all(searchParams)
  const result = rows.reduce((result, row) => {
    result.push([row.atom, row["COUNT(atom)"]])
    return result
  }, [])

  return result
}

/**
 * Add a TCB search results dance to the database.
 *
 * @param dance - The dance to add.
 * @return The added dance, now with its just-assigned danceId.
 */
export const dbAddTcbSearchResultsDance = (
  dance: SearchResultsDanceNoId,
): SearchResultsDance => {
  const values = dbGetValuesForStorage(dancesTableSpec, dance)

  const info = insertDance.run(values)

  if (info.changes === 1) {
    let newDance = dbGetSearchResultsDanceById(info.lastInsertRowid as number)
    if (newDance) {
      return newDance
    } else {
      throw new Error("Something went wrong getting a dance from the database.")
    }
  } else {
    throw new Error("Dance was not added to the database.")
  }
}

const insertDance = dbPrepareInsertStatement(dancesTableSpec)

/**
 * Updates a dance by modifying only certain properties.
 *
 * @param patch - Object with the ID of dance and the changes to make to it.
 * @return - Number of records updated (0 or 1).
 */
export const dbUpdateDance = (patch: DancePatch): number => {
  const placeholders = []
  const values = []

  for (const [column, value] of Object.entries(patch.changes)) {
    if (column !== "danceId" && column !== "programs") {
      placeholders.push(`${column} = ?`)
      values.push(
        dbGetOneValueForStorage(dancesTableSpec, column as keyof Dance, value),
      )
    }
  }

  const statement = db.prepare(
    `UPDATE Dances SET ${placeholders.join(", ")} WHERE danceId = ${
      patch.danceId
    }`,
  )
  const info = statement.run(values)
  return info.changes
}

/**
 * Takes an array of search results dances without IDs (from the Caller's Box)
 * and retrieves the same dances from the database, adding them if they are not
 * there already. Thus the dances returned are all in the database and have IDs.
 *
 * @param tcbDances - Dances without IDs from searching the Caller's Box.
 * @return The same dances from the database, with IDs.
 */
export const dbTcbDancesToAndFromDb = db.transaction(
  (tcbDances: SearchResultsDanceNoId[]): SearchResultsDance[] => {
    const newTcbDances = [] as SearchResultsDance[]

    // db.transaction does not work with `.map`, needs a for loop.
    for (let dance of tcbDances) {
      // TS placation: dances from TCB should always have a tcbId.
      const danceInDb =
        (dance.tcbId && dbGetSearchResultsDanceByTcbId(dance.tcbId)) ||
        dbAddTcbSearchResultsDance(dance)

      newTcbDances.push(danceInDb)
    }

    return newTcbDances
  },
)
