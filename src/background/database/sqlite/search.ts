/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import { SearchQuery, ProgressionOption } from "../../../shared/types"

/**
 * Return an SQL `WHERE` clause for a given search query.
 *
 * @param query - Search query data after parsing it from the search form.
 * @return An SQL WHERE clause.
 */
export type MakeWhereClause = (query: SearchQuery) => [string, string[]]

export const makeWhereClause: MakeWhereClause = (query) => {
  const clauseParts: string[] = [`storageStatus IS ?`]
  const whereParams: string[] = ["in"]

  if (query.title) {
    clauseParts.push(`title LIKE ?`)
    whereParams.push("%" + query.title + "%")
  }
  if (query.authors) {
    clauseParts.push(`authors LIKE ?`)
    whereParams.push("%" + query.authors + "%")
  }
  if (query.formationBase) {
    clauseParts.push(`formationBase LIKE ?`)
    whereParams.push("%" + query.formationBase + "%")
  }
  if (query.progression) {
    const { clause, param } = makeProgressionWherePart(query.progression)
    clauseParts.push(clause)
    whereParams.push(param)
  }
  if (query.tags && query.tags.length > 0) {
    const [wherePart, params] = makeBooleanWherePart(query.tags, "tags")
    clauseParts.push(wherePart)
    params.forEach((param) => whereParams.push(param))
  }

  const whereClause = `WHERE ${clauseParts.join(" AND ")}`
  return [whereClause, whereParams]
}

/**
 * Return pieces of an SQL `WHERE` clause for searching by progression. The
 * "More than Quadruple" dances in the Caller's Box are: Nonuple, Quintuple,
 * Sextuple, Septuple, Octuple. (At least as of 2020/08/02.)
 *
 * @param input - The search input for progression.
 * @return - A clause and a param to use as part of a WHERE clause.
 */
const makeProgressionWherePart = (
  input: Exclude<ProgressionOption, "">,
): { clause: string; param: string } => {
  switch (input) {
    case "None":
    case "Single":
    case "Double":
    case "Triple":
    case "Quadruple":
      return {
        clause: "progression LIKE ?",
        param: input,
      }
    case "More than Quadruple":
    case "Other/Weird":
      return {
        clause: `(
          progression NOT IN ('None', 'Single', 'Double', 'Triple', 'Quadruple')
          AND progression ${
            input === "More than Quadruple" ? "LIKE ?" : "NOT LIKE ?"
          })`,
        param: "%uple%",
      }
  }
}

/**
 * Given an array of search term tokens that include AND/OR/NOT operators,
 * convert them into part of an SQL `WHERE` clause to do an AND/OR/NOT
 * query.
 *
 * @param tokens - An array of search terms and boolean operators (AND/OR/NOT).
 * @return Part of an SQL WHERE clause, to do a boolean search.
 */
export type MakeBooleanWherePart = (
  tokens: string[],
  column: string,
) => [string, string[]]

const makeBooleanWherePart: MakeBooleanWherePart = (tokens, column) => {
  const result: string[] = []
  const params: string[] = []
  const tokensWithOrs = insertOrs(tokens)

  tokensWithOrs.forEach((token, index) => {
    if (token === "AND" || token === "OR") {
      result.push(token)
    } else if (token === "NOT") {
      // Do nothing.
    } else if (tokensWithOrs[index - 1] === "NOT") {
      result.push(`${column} NOT LIKE ?`)
      params.push("%" + token + "%")
    } else {
      result.push(`${column} LIKE ?`)
      params.push("%" + token + "%")
    }
  })
  const wherePart = `(${result.join(" ")})`
  return [wherePart, params]
}

/**
 * Non-destructively "insert" "OR" operators in order to normalize a logical
 * expression.  A user may not separate all the search terms with operators, so
 * this function adds "OR"s to ensure that the output array has all terms
 * separated by operators.  (Absence of operators defaults to OR logic.)
 *
 * @param tokens - Array of search terms and boolean operators (AND/OR/NOT).
 * @return The same array with "OR"s inserted where operators were omitted.
 */
export type InsertOrs = (tokens: string[]) => string[]

const insertOrs: InsertOrs = (tokens) => {
  if (tokens.length < 2) {
    return tokens
  }
  const result = [tokens[0]]

  tokens.slice(1).forEach((token) => {
    const previous = result[result.length - 1]
    if (
      previous !== "AND" &&
      previous !== "OR" &&
      previous !== "NOT" &&
      token !== "AND" &&
      token !== "OR"
    ) {
      // previous is a search term and token is either a search term or "NOT".
      result.push("OR")
    }
    result.push(token)
  })

  return result
}
