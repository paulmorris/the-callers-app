/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import Dexie from "dexie"
import "dexie-export-import"
import download from "downloadjs"

import { Dance, SearchQuery } from "../../../shared/types"
import { notUndefinedOrNull } from "../../../shared/utils"
import { makeFilterFunction } from "./search"

class CallersDatabase extends Dexie {
  // Declare implicit table properties.
  // (Just to inform Typescript. Instanciated by Dexie in stores() method.)
  dances: Dexie.Table<Dance, number>
  //...other tables go here...

  // Declare dexie-export-import functions:
  // import: Function;
  // export: Function;

  constructor(databaseName: string) {
    super(databaseName)
    this.version(1).stores({
      dances:
        "++id, tcbId, title, authors, formationBase, storageStatus, *tags",
      //...other tables go here...
    })
  }
}

const db = new CallersDatabase("CallersDatabase")

db.open().catch((err) => {
  console.error(`Open failed: ${err.stack}`)
})

export const dbGetAllTcbIds = async (
  where: {} = {
    storageStatus: "in",
  },
): Promise<string[]> => {
  const tcbIds = await db.dances
    .where(where)
    .toArray((dances) => dances.map((d) => d.tcbId))

  return tcbIds.filter(notUndefinedOrNull)
}

export const dbGetDanceCount = (
  where: {} = {
    storageStatus: "in",
  },
): Promise<number> => db.dances.where(where).count()

/**
 * Search the local database with user input from the search form.
 */
export function dbSearchForDances(
  query: SearchQuery,
  where: {} = { storageStatus: "in" },
): Promise<Dance[]> {
  const collection = db.dances.where(where)

  const filtered = query
    ? collection.filter(makeFilterFunction(query))
    : collection

  return filtered.toArray()
}

export const dbAddDance = async (dance: Dance): Promise<Dance> => {
  const danceId = await db.dances.add(dance)
  dance.danceId = danceId
  return dance
}

/**
 * Updates a dance by modifying only certain properties.
 *
 * @param id - ID of dance to update.
 * @param changes - Changes to make to the dance.
 * @return - Number of records updated (0 or 1).
 */
export const dbUpdateDance = async (
  id: number,
  changes: Partial<Dance>,
): Promise<number> => {
  const recordsUpdatedCount = await db.dances.update(id, changes)
  return recordsUpdatedCount
}

export const dbGetDance = (
  id?: number,
  tcbId?: string,
): Dexie.Promise<Dance | undefined> => {
  if (id) {
    return db.dances.get({ id })
  } else if (tcbId) {
    return db.dances.get({ tcbId })
  }
  return Dexie.Promise.resolve(undefined)
}

/**
 * Export and download the database to a JSON file.
 */
export const dbExport = async () => {
  try {
    const blob = await db.export({ prettyJson: true })
    download(blob, "callers-app-export.json", "application/json")
  } catch (error) {
    console.error("Error exporting database: " + error)
  }
}

// Currently unused functions.

// const dbHasDanceWithTcbId = (tcbId: string): Dexie.Promise<boolean> =>
//   db.dances
//     .where("tcbId")
//     .equals(tcbId)
//     .count()
//     .then(count => count > 0);

// const dbGetAllTcbIds = (): Dexie.Promise<any[]> =>
//   db.dances
//     .where("tcbId")
//     .notEqual(-1)
//     .keys();

// const dbGetAllDances = () => db.dances.toArray();

// const dbDeleteDance = (dance: Dance): Dexie.Promise<void> =>
//   db.transaction("rw", db.dances, async () => {
//     const id =
//       dance.id ||
//       (await dbGetDance({ tcbId: dance.tcbId }).then(dance => dance.id));
//     return db.dances.delete(id);
//   });

// const dbDeleteDanceViaTcbId = (tcbId: string): Dexie.Promise<void> =>
//   db.transaction("rw", db.dances, async () => {
//     const dance = await dbGetDance({ tcbId });
//     return db.dances.delete(dance.id);
//   });
