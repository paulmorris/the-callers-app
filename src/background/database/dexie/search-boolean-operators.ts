/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import { append, drop, head, init, last, nth, tail, concat } from "ramda"

type BinaryOperator = "AND" | "OR"
type UnaryOperator = "NOT"
type Node = string | [BinaryOperator, Node, Node] | [UnaryOperator, Node]

/**
 * Takes an array of tokens/strings and returns an abstract syntax tree. Takes
 * binary (infix) AND and OR, unary NOT operators, and the terms they operate
 * on, and assembles them into a prefix tree structure. For example:
 *
 *   ["aaa", "AND", "NOT" "bbb", "OR" "ccc"] ->
 *   ["OR", ["AND", "aaa", ["NOT", "bbb"]], "ccc"]
 *
 * @param input - The input (not modified).
 * @return - The input in AST form.
 */
export const tokensToAst = (input: string[]): Node => {
  const input2 = insertOrs(input)
  const ast = parseBinary("OR", parseBinary("AND", parseUnary("NOT", input2)))
  if (ast.length === 0) {
    return ""
  }
  if (ast.length > 1) {
    // Should never happen.
    console.error("Something went wrong when parsing search input:", ast)
  }
  return ast[0]
}

/**
 * Recursive function for doing a single pass in the process of converting an
 * array of tokens, into an abstract syntax tree structure. For a given infix
 * binary operator it takes the operator and the two terms it operates on and
 * groups them into a prefix order. For example:
 *
 *   ["x", "AND", "y", "AND", "z"] -> [["AND", ["AND", "x", "y"], "z"]].
 *
 * @param operator - A binary operator like AND or OR.
 * @param input - The input (not modified).
 * @param output - The output assembled with each call and finally returned.
 * @return - The final version of the output.
 */
const parseBinary = (
  operator: BinaryOperator,
  input: Node[],
  output: Node[] = [],
): Node[] => {
  if (input.length === 0) {
    return output
  }

  let newInput, newOutput

  if (input[1] === operator) {
    const inputIndex0 = nth(0, input)
    const inputIndex2 = nth(2, input)
    if (inputIndex0 && inputIndex2) {
      newInput = drop(3, input)
      newOutput = append([operator, inputIndex0, inputIndex2], output)
    }
  } else if (input[0] === operator) {
    const outputLast = last(output)
    const inputIndex1 = nth(1, input)
    if (outputLast && inputIndex1) {
      newInput = drop(2, input)
      newOutput = append([operator, outputLast, inputIndex1], init(output))
    }
  } else {
    const inputHead = head(input)
    if (inputHead) {
      newInput = tail(input)
      newOutput = append(inputHead, output)
    }
  }

  if (!newInput || !newOutput) {
    // TODO: Handle bad input.
    console.warn("Error: bad input with boolean operators.")
    return []
  }
  return parseBinary(operator, newInput, newOutput)
}

/**
 * Recursive function for doing a single pass in the process of converting an
 * array of tokens, into an abstract syntax tree structure. For a given
 * unary operator it takes the operator and the term it operates on and
 * groups them together. For example: ["x", "NOT", "y"] - > ["x", ["NOT", "y"]]
 *
 * @param operator - A unary operator like NOT.
 * @param input - The input (not modified).
 * @param output - The output assembled with each call and finally returned.
 * @return - The final version of the output.
 */
const parseUnary = (
  operator: UnaryOperator,
  input: Node[],
  output: Node[] = [],
): Node[] => {
  if (input.length === 0) {
    return output
  }

  let newInput, newOutput

  if (input[0] === operator) {
    const inputIndex1 = nth(1, input)
    if (inputIndex1) {
      newInput = drop(2, input)
      newOutput = append([operator, inputIndex1], output)
    }
  } else {
    const inputHead = head(input)
    if (inputHead) {
      newInput = tail(input)
      newOutput = append(inputHead, output)
    }
  }

  if (!newInput || !newOutput) {
    // TODO: Handle bad input.
    console.warn("Error: bad input with boolean operators.")
    return []
  }
  return parseUnary(operator, newInput, newOutput)
}

const isBooleanOperator = (node: Node): boolean =>
  node === "OR" || node === "AND"

/**
 * Non-destructively inserts "OR" operators in order to normalize a logical
 * expression.  A user may not separate all the search terms with operators, so
 * this function adds "OR"s to ensure that the output array has all terms
 * separated by operators.  (Absence of operators defaults to OR logic.)
 *
 * @param input - The input (not modified).
 * @param output - The output assembled with each call and finally returned.
 * @return - The final version of the output.
 */
const insertOrs = (input: Node[], output: Node[] = []): Node[] => {
  const inputHead = head(input)
  if (inputHead) {
    if (input.length === 1) {
      return append(inputHead, output)
    } else if (
      input[0] !== "NOT" &&
      !isBooleanOperator(input[0]) &&
      !isBooleanOperator(input[1])
    ) {
      // Insert an "OR".
      return insertOrs(tail(input), concat(output, [inputHead, "OR"]))
    } else {
      return insertOrs(tail(input), append(inputHead, output))
    }
  } else {
    // TypeScript placation: this should never happen.
    console.warn("Error when normalizing input with boolean operators.")
    return output
  }
}

type F = (arg0: string[]) => boolean

const orFn = (fnA: F, fnB: F, xs: string[]): boolean => fnA(xs) || fnB(xs)

const andFn = (fnA: F, fnB: F, xs: string[]): boolean => fnA(xs) && fnB(xs)

const notFn = (fnA: F, xs: string[]): boolean => !fnA(xs)

const matchFn = (query: string, xs: string[]): boolean => xs.includes(query)

/**
 * Takes an abstract syntax tree of boolean operators and search terms and
 * produces a function that implements that search query. The produced function
 * takes an array of strings to be searched and returns a boolean indicating
 * whether the query succeeded.
 *
 * (Note that because arrays and strings both have an 'includes' method, which
 * is used in the `matchFn`, the returned function could also take a string as
 * its argument, with the needed adjustments to the types.)
 *
 * @param input - An abstract syntax tree of boolean operators and search terms.
 * @return - A function that implements the logic of that search query.
 */
export const makeBooleanQueryFn = (input: Node): F => {
  // TS placation, the `!` on the `input[2]!` below shouldn't be necessary.
  if (typeof input === "string") {
    return matchFn.bind(undefined, input)
  } else if (input[0] === "AND") {
    return andFn.bind(
      undefined,
      makeBooleanQueryFn(input[1]),
      makeBooleanQueryFn(input[2]!),
    )
  } else if (input[0] === "OR") {
    return orFn.bind(
      undefined,
      makeBooleanQueryFn(input[1]),
      makeBooleanQueryFn(input[2]!),
    )
  } else if (input[0] === "NOT") {
    return notFn.bind(undefined, makeBooleanQueryFn(input[1]))
  }
  // TS placation, should never happen.
  return () => false
}
