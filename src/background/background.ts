/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import { ipcRenderer, IpcRendererEvent } from "electron"

import {
  Dance,
  SearchResults,
  SearchResultsState,
  StorageStatus,
  FiguresType,
  DancePatch,
  SearchQuery,
  LocalSearchResults,
  Program,
  SearchResultsDance,
  ProgramPatch,
  ProgramItem,
  isDance,
} from "../shared/types"
import {
  fetchTcbDanceData,
  fetchSearchResults,
  deleteItem,
  insertDefaultItem,
  dbGetDanceById,
  dbGetAllTcbIds,
  dbUpdateDance,
  dbSearchForDances,
  dbSearchForFigures,
  getDanceStorageStatus,
  addDanceToProgram,
  addSpacerToProgram,
  deleteProgram,
  getAllPrograms,
  getDancesForProgramId,
  getProgram,
  moveDanceInProgram,
  removeItemFromProgram,
  searchPrograms,
  updateProgram,
} from "./background-modules"
import {
  getSettings,
  setDanceAttributes,
  SettingsTableRow,
} from "./database/sqlite/settings"

/**
 * Receives and handles messages from the foreground process, sending responses
 * back, resolving or rejecting the associated promises.
 */
ipcRenderer.on(
  "message-to-background",
  async (
    event: IpcRendererEvent,
    message: {
      command: keyof ToBackground
      ipcId: number
      payload: any
    },
  ): Promise<void> => {
    const { command, ipcId, payload } = message
    try {
      const handler = toBackground[command]
      // @ts-ignore
      const result = await handler(payload)

      ipcRenderer.send("message-to-foreground", {
        command: "resolveIpcPromise",
        payload: result,
        ipcId,
      })
    } catch (e) {
      console.error(e)

      ipcRenderer.send("message-to-foreground", {
        command: "rejectIpcPromise",
        payload: e,
        ipcId,
      })
    }
  },
)

/**
 * This interface is to allow type-checking of the message passing between
 * foreground and background processes.  Functions in this interface have
 * instances in both the foreground and the background.  The payloads and
 * promise-wrapped return values are type checked on both sides. Calling
 * one of the functions in the foreground is basically equivalent to calling
 * the same function in the background, with some message passing plumbing
 * in between.
 *
 * Every function has to have a single payload argument and return a promise.
 *
 * This means the functions are async in the backend when they
 * don't really need to be, except to enable the type checking.
 */
export interface ToBackground {
  addDanceToProgram: (payload: {
    programId: number
    danceId: number
    tcbId: string | undefined
  }) => Promise<ProgramItem[]>

  addSpacerToProgram: (payload: { programId: number }) => Promise<ProgramItem[]>

  // TODO - return void or null?
  deleteItem: (payload: {
    tableName: string
    idColumn: string
    id: number
  }) => Promise<void>

  deleteProgram: (programId: number) => Promise<number>

  fetchTcbDanceByTcbId: (payload: {
    danceId: number
    tcbId: string
  }) => Promise<Dance>

  getAllPrograms: (_unusedPayload: any) => Promise<Program[]>

  getDanceById: (payload: {
    danceId: number
  }) => Promise<Dance | SearchResultsDance | undefined>

  getDancesForProgramId: (programId: number) => Promise<ProgramItem[]>

  getProgram: (programId: number) => Promise<Program>

  getSettings: () => Promise<SettingsTableRow>

  insertDefaultItem: (payload: "Dances" | "Programs") => Promise<number>

  moveDanceInProgram: (payload: {
    programId: number
    newIndex: number
    oldIndex: number
  }) => Promise<ProgramItem[]>

  /**
   * Return includes the removed dance so its object on the front end can be
   * updated, namely its programs list.  It's likely it no longer belongs to
   * the program it was removed from (unless it was in the program more than
   * once).
   */
  removeItemFromProgram: (payload: {
    programDanceId: number
    programId: number
    danceId: number | undefined
  }) => Promise<{
    items: ProgramItem[]
    removedItem: Dance | undefined
  }>

  searchCallersBoxDances: (payload: {
    url: string
    state: SearchResultsState
  }) => Promise<Partial<SearchResults>>

  searchFigures: (payload: string) => Promise<[string, number][]>

  searchLocalDances: (payload: {
    query: SearchQuery
    state: SearchResultsState
  }) => Promise<LocalSearchResults>

  searchPrograms: (text: string) => Promise<Program[]>

  searchTcbFigures: (payload: string) => Promise<[string, number][]>

  setDanceAttributes: (attributes: string) => Promise<number>

  updateExistingDance: (
    payload: DancePatch,
  ) => Promise<Dance | SearchResultsDance | undefined>

  updateProgram: (payload: ProgramPatch) => Promise<Program>

  updateStorageStatus: (payload: {
    newStorageStatus: StorageStatus
    oldStorageStatus: StorageStatus
    danceId: number
    tcbId: string | undefined
  }) => Promise<DancePatch>
}

/**
 * Background instantiation of message passing handler functions, in parallel
 * with the same functions in the foreground.
 */
const toBackground: ToBackground = {
  async addDanceToProgram(payload) {
    const { programId, danceId, tcbId } = payload

    const storageStatus = getDanceStorageStatus(danceId)
    // TS Placation, tcbId should always exist here.
    if (storageStatus !== "out-full" && storageStatus !== "in" && tcbId) {
      await fetchAndUpdateTcbDance(danceId, tcbId)
    }

    addDanceToProgram(programId, danceId)
    return getDancesForProgramId(programId)
  },

  async addSpacerToProgram(payload) {
    const { programId } = payload
    addSpacerToProgram(programId)
    return getDancesForProgramId(programId)
  },

  async deleteItem(payload) {
    return deleteItem(payload.tableName, payload.idColumn, payload.id)
  },

  async deleteProgram(programId) {
    return deleteProgram(programId)
  },

  /**
   * Fetch a single dance from the Caller's Box, store/update it in the DB and
   * return it to the foreground.
   */
  async fetchTcbDanceByTcbId(payload) {
    return fetchAndUpdateTcbDance(payload.danceId, payload.tcbId)
  },

  async getAllPrograms(_unusedPayload) {
    return getAllPrograms()
  },

  async getDanceById(payload) {
    return dbGetDanceById(payload.danceId)
  },

  async getDancesForProgramId(programId) {
    return getDancesForProgramId(programId)
  },

  async getProgram(programId) {
    return getProgram(programId)
  },

  async getSettings() {
    return getSettings()
  },

  async insertDefaultItem(payload) {
    return insertDefaultItem(payload)
  },

  async moveDanceInProgram(payload) {
    const { programId, newIndex, oldIndex } = payload
    moveDanceInProgram(programId, newIndex, oldIndex)
    return getDancesForProgramId(programId)
  },

  async removeItemFromProgram(payload) {
    const { programDanceId, programId, danceId } = payload
    removeItemFromProgram(programDanceId)
    const items = getDancesForProgramId(programId)

    const dance = danceId !== undefined && dbGetDanceById(danceId)
    // TS placation, only full dance records will be in progams.
    const removedItem = dance && isDance(dance) ? dance : undefined
    return { items, removedItem }
  },

  async searchCallersBoxDances(payload) {
    return searchCallersBoxDances(payload)
  },

  async searchFigures(payload) {
    return searchFigures("figures", payload)
  },

  async searchLocalDances(payload) {
    const results = dbSearchForDances(payload.query)
    return {
      localDances: results.localDances,
      totalLocalDances: results.totalLocalDances,
      state: payload.state,
    }
  },

  async searchPrograms(payload) {
    return searchPrograms(payload)
  },

  async searchTcbFigures(payload) {
    return searchFigures("tcbFigures", payload)
  },

  async setDanceAttributes(attributes) {
    return setDanceAttributes(attributes)
  },

  async updateExistingDance(payload) {
    const count = dbUpdateDance(payload)
    if (count !== 1) {
      throw new Error("Failed to update a dance in the database.")
    }
    return dbGetDanceById(payload.danceId)
  },

  async updateProgram(payload) {
    updateProgram(payload)
    return getProgram(payload.programId)
  },

  async updateStorageStatus(payload) {
    return updateStorageStatus(
      payload.newStorageStatus,
      payload.oldStorageStatus,
      payload.danceId,
      payload.tcbId,
    )
  },
}

/**
 * Search the database for figures and include common section headings.
 *
 * @param figuresType - Type of figures to search, e.g. "figures", "tcbFigures".
 * @param query - The terms to search for.
 * @return Figure strings and frequency, e.g [["(8) Partner swing", 34], ...]
 */
const searchFigures = (
  figuresType: FiguresType,
  query: string,
): [string, number][] => {
  return [
    ...searchForSectionHeadings(query),
    ...dbSearchForFigures(figuresType, query),
  ]
}

/**
 * When searching for figures, we want to include section headings as well.
 *
 * @param query - The terms to search for.
 * @return Heading strings and bogus frequency, e.g. [["# A1", 0]]
 */
const searchForSectionHeadings = (query: string): [string, number][] => {
  const headings = ["# A1", "# A2", "# B1", "# B2"]
  const upperCaseTerms = query.toUpperCase()
  return headings
    .filter((heading) => heading.includes(upperCaseTerms))
    .map((heading) => [heading, 0])
}

/**
 * Fetches a dance from the Caller's Box and adds it to the database. It
 * temporarily stores the promise that will resolve after the dance has been
 * fetched and added, so that if a dance is already being fetched and added,
 * it just returns the stored promise and doesn't fetch and add again. This
 * prevents a potential async race condition when you click to view a new
 * dance and then quickly click to toggle its storage status, or vice-versa,
 * which previously could cause the same dance to be stored in the database
 * twice.
 */
const fetching: Record<string, Promise<Dance>> = {}

const fetchAndUpdateTcbDance = (
  danceId: number,
  tcbId: string,
): Promise<Dance> => {
  if (!fetching[tcbId]) {
    fetching[tcbId] = fetchTcbDanceData(tcbId, danceId)
      .then((dance: Dance) => {
        const result = dbUpdateDance({ danceId, changes: dance })
        if (result === 1) {
          const updatedDance = dbGetDanceById(danceId) as Dance
          if (updatedDance) {
            return updatedDance
          }
        }
        throw new Error("Dance not updated in the database after fetching")
      })
      .catch((error) => {
        throw error
      })
      .finally(() => {
        delete fetching[tcbId]
      })
  }

  return fetching[tcbId]
}

/**
 * @param payload.url - The URL for fetching results from The Caller's Box.
 * @param payload.state - The state of the search results when they are displayed.
 */
const searchCallersBoxDances = async (payload: {
  url: string
  state: SearchResultsState
}): Promise<Partial<SearchResults>> => {
  const [results, tcbIds] = await Promise.all([
    fetchSearchResults(payload.url),
    dbGetAllTcbIds(),
  ])

  // Set storageStatus for "in" dances.
  results.tcbDances.forEach((dance) => {
    // TS placation, all of these dances will have tcbIds.
    if (dance.tcbId && tcbIds.includes(dance.tcbId)) {
      dance.storageStatus = "in"
    }
  })
  // TS placation.
  ;(results as SearchResults).state = payload.state

  return results
}

const updateStorageStatus = async (
  newStorageStatus: StorageStatus,
  oldStorageStatus: StorageStatus,
  danceId: number,
  tcbId: string | undefined,
): Promise<DancePatch> => {
  // TS Placation: out-partial dances should always have a tcbId
  const dance =
    oldStorageStatus === "out-partial" && tcbId
      ? await fetchAndUpdateTcbDance(danceId, tcbId)
      : dbGetDanceById(danceId)

  if (dance) {
    const count = dbUpdateDance({
      danceId,
      changes: { storageStatus: newStorageStatus },
    })

    if (count !== 1) {
      throw new Error(
        "Storage status for dance was not updated in the database.",
      )
    }

    return {
      danceId,
      changes: { storageStatus: newStorageStatus },
    }
  } else {
    throw new Error(`No dance found for danceId "${danceId}".`)
  }
}
