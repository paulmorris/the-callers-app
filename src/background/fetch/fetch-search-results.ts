/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import { fetchDocument } from "./fetch-utils"
import {
  TcbSearchResults,
  DanceStatus,
  SearchResultsDanceNoId,
} from "../../shared/types"
import { tcbIdFromUrl } from "../../shared/utils"
import { dbTcbDancesToAndFromDb } from "../background-modules"

export async function fetchSearchResults(
  searchUrl: string,
): Promise<TcbSearchResults> {
  const doc = await fetchDocument(searchUrl)
  const searchResults = parseSearchResults(doc.body)
  return searchResults
}

function parseSearchResults(body: HTMLElement): TcbSearchResults {
  const table = body.getElementsByTagName("table")?.[0]
  const dancesFromTable = table ? getDancesFromTable(table) : []
  const tcbDances = dbTcbDancesToAndFromDb(dancesFromTable)

  const headingElement = body.getElementsByTagName("h2")?.[2]
  const dbUpdatedElement = headingElement?.nextElementSibling

  const codeUpdatedElement = dbUpdatedElement?.nextElementSibling

  const criteriaElement =
    codeUpdatedElement?.nextElementSibling?.nextElementSibling

  const matchesElement = criteriaElement?.nextElementSibling

  // If there are no results there is an extra <div> before the <p>.
  const matchesElement2 =
    matchesElement?.nodeName === "P"
      ? matchesElement
      : matchesElement?.nextElementSibling

  const totalDancesMatch = matchesElement2?.textContent?.match(
    /Of (\d*) dances in the/,
  )

  const dateRegex = /\d\d\d\d-\d\d-\d\d/g
  const dbUpdatedMatch = dbUpdatedElement?.textContent?.match(dateRegex)
  const codeUpdatedMatch = codeUpdatedElement?.textContent?.match(dateRegex)

  return {
    tcbDances,
    tcbDatabaseUpdated: dbUpdatedMatch?.[0],
    tcbSiteCodeUpdated: codeUpdatedMatch?.[0],
    criteria: criteriaElement?.textContent?.trim(),
    totalTcbDances: totalDancesMatch?.[1]
      ? Number(totalDancesMatch[1])
      : undefined,
  }
}

function getDancesFromTable(table: HTMLTableElement): SearchResultsDanceNoId[] {
  let dances: SearchResultsDanceNoId[] = []
  const rows = table.getElementsByTagName("tr")
  for (let row of rows) {
    const cells = row.getElementsByTagName("td")
    const [tcbId, title] = idAndTitleFromCell(cells[3])

    const dance: SearchResultsDanceNoId = {
      hasFigures: cells[0].children.length !== 0,
      hasLink: cells[1].children.length !== 0,
      hasVideo: cells[2].children.length !== 0,
      tcbId,
      title,
      status: statusFromCell(cells[3]),
      authors: [cells[4].innerText],
      formationBase: cells[5].innerText,
      storageStatus: "out-partial",
    }
    dances.push(dance)
  }
  return dances
}

const idAndTitleFromCell = (
  cell: HTMLTableDataCellElement,
): [string | undefined, string] => {
  const titleAnchor = cell.firstElementChild as HTMLAnchorElement
  const tcbId = tcbIdFromUrl(titleAnchor.href)
  const title = titleAnchor.innerText
  return [tcbId, title]
}

const statusFromCell = (cell: HTMLTableDataCellElement): DanceStatus => {
  const span = cell.getElementsByTagName("span")[0]

  return span?.innerText === "(deprecated)"
    ? "deprecated"
    : span?.innerText === "(broken)"
    ? "broken"
    : "none"
}
