/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import { fetchDocument } from "./fetch-utils"

const sourcesUrl =
  "https://www.ibiblio.org/contradance/thecallersbox/sources.php"

export async function fetchSources(): Promise<Map<string, string>> {
  const doc = await fetchDocument(sourcesUrl)
  const anchors = doc.body.querySelectorAll("td > a") as NodeListOf<
    HTMLAnchorElement
  >

  const sources = new Map()
  for (const anchor of anchors) {
    if (anchor.href.startsWith("http")) {
      sources.set(anchor.innerText, anchor.href)
    }
  }
  return sources
}
