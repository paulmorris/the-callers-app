/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import R from "ramda"

import { DancePart, makeDance, Dance } from "../../shared/types"
import { danceJsonUrl } from "../../shared/utils"
import { fetchSources } from "./fetch-sources"

// Dances that have obscure properties:
//
// "40 Years of Penguin Pam {original}"
//     status: deprecated
//     many figures: C1 C2
//     progression
//     direction
//     phraseStructure
// "The Bunch of Fives"
//     formationDetail
//     progression
//     direction
//     callingNotes
//     figure durations like: (1-8)
// "Abbot's Bromely" by Fred Park
//     music
//     phraseStructure
// "Al's Bent Heyday"
//     otherNames
// "Another Slice of Pinewoods"
//     indented sub-lines in figures
//     more than one author
// "Bob and Laura's 35th"
//     mixer
//     indented sub-lines in figures
//     progression
//     direction
// "Discombobulation"
//     permission: no_figures
//     multiple variants in variant videos
// "The Nice Combination" by Gene Hubert
//     variantVideos
// "Nice Combination {variant by Erik Hoffman}"
//     basedOn
// "20 Below"
//     multiple variant videos
// "The Devil's Backbone"
//     URLs in calling notes

interface TcbDanceIncoming {
  ID: string
  Name: string
  Authors: string[]
  Permission: "search" | "full" | "no_figures"
  Status: "deprecated" | "broken" | ""
  BasedOn: string[]
  FormationBase: string
  FormationDetail: string
  Progression: string
  Direction: string
  "Mixer?": "Yes" | ""
  PhraseStructure: string
  Music: string[]
  phrases: TcbPhrasesRow[]
  CallingNotes: string[]
  Appearances: TcbAppearance[]
  OtherNames: string[]
  Videos: string[]
  VariantVideos: string[]
  request: string
  download_date: string
}

interface TcbPhrasesRow {
  name: string
  figures: string[]
}

interface TcbAppearance {
  source: string
  p: string
  no: string
  vol: string
  altnames: string[]
  link: string
}

/** Cached data from the TCB sources page, a map from source label to URL. */
let cachedSources = fetchSources()

/**
 * Given a TCB ID, fetch JSON data for that dance and convert it into the form
 * used internally.
 */
export async function fetchTcbDanceData(
  tcbId: string,
  danceId: number,
): Promise<Dance> {
  const sources = await cachedSources
  const response = await fetch(danceJsonUrl(tcbId))
  const jsonString = await response.text()

  const tcbDance: TcbDanceIncoming = JSON.parse(jsonString)
  return convertTcbDance(tcbDance, sources, danceId)
}

/**
 * Convert TCB data for a given dance (from JSON download) into the form
 * used internally.
 */
function convertTcbDance(
  tcbDance: TcbDanceIncoming,
  sources: Map<string, string>,
  danceId: number,
): Dance {
  const dance = makeDance(danceId, "out-full")

  if (tcbDance.ID) {
    dance.tcbId = tcbDance.ID
  }
  if (tcbDance.Name) {
    dance.title = tcbDance.Name
  }
  if (tcbDance.Authors?.length > 0) {
    dance.authors = tcbDance.Authors
  }
  if (tcbDance.Permission) {
    dance.permission = tcbDance.Permission
    dance.hasFigures = dance.permission === "full"
  }
  if (tcbDance.Status) {
    dance.status = tcbDance.Status
  }
  if (tcbDance.BasedOn?.length > 0) {
    dance.basedOn = tcbDance.BasedOn
  }
  if (tcbDance.FormationBase) {
    dance.formationBase = tcbDance.FormationBase
  }
  if (tcbDance.FormationDetail) {
    dance.formationDetail = tcbDance.FormationDetail
  }
  if (tcbDance.Progression) {
    dance.progression = tcbDance.Progression
  }
  if (tcbDance.Direction) {
    dance.direction = tcbDance.Direction
  }
  if (tcbDance["Mixer?"] === "Yes") {
    dance.mixer = "yes"
  }
  if (tcbDance.PhraseStructure) {
    dance.phraseStructure = tcbDance.PhraseStructure
  }
  if (tcbDance.Music?.length > 0) {
    dance.music = tcbDance.Music
  }
  if (tcbDance.phrases.length > 0) {
    dance.tcbFigures = convertTcbPhrases(tcbDance.phrases)
  }
  if (tcbDance.CallingNotes?.length > 0) {
    dance.callingNotes = tcbDance.CallingNotes
  }
  if (tcbDance.Appearances?.length > 0) {
    const { appearances, hasLink } = convertTcbAppearances(
      tcbDance.Appearances,
      sources,
    )
    dance.appearances = appearances
    dance.hasLink = hasLink
  }
  if (tcbDance.OtherNames?.length > 0) {
    // So each will be shown on its own line, insert empty strings, because
    // markdown and UI consistency.
    dance.otherTitles = R.intersperse("", tcbDance.OtherNames)
  }
  if (tcbDance.Videos?.length > 0) {
    dance.videos = convertTcbVideos(tcbDance.Videos)
  }
  if (tcbDance.VariantVideos?.length > 0) {
    dance.variantVideos = convertTcbVariantVideos(tcbDance.VariantVideos)
  }
  if (tcbDance.Videos.length > 0 || tcbDance.VariantVideos.length > 0) {
    dance.hasVideo = true
  }
  return dance
}

/**
 * Convert TCB "phrases" (dance figures) data (from JSON download) into the
 * form used internally.
 */
function convertTcbPhrases(phrases: TcbPhrasesRow[]): DancePart[] {
  const beatsRegex = /^\((\d+)\)/
  const danceParts: DancePart[] = []

  for (const row of phrases) {
    for (const figure of row.figures) {
      // When a line starts with five spaces it is an indented sub-part.
      const isSubpart = figure.slice(0, 5) === "     "

      const dancePart: DancePart = {
        text: isSubpart ? figure.slice(5) : figure,
        section: row.name,
      }

      if (isSubpart) {
        dancePart.subpart = true
      }

      const beatsMatch = dancePart.text.match(beatsRegex)
      if (beatsMatch) {
        dancePart.beats = Number(beatsMatch[1])
      }

      danceParts.push(dancePart)
    }
  }

  return danceParts
}

/**
 * Convert TCB "appearances" data (from JSON download) into the form used
 * internally. Also check if there is a linked appearance for hasLink status.
 */
function convertTcbAppearances(
  tcbAppearances: TcbAppearance[],
  sources: Map<string, string>,
): { appearances: string[]; hasLink: boolean } {
  let hasLink = false

  const formatted = tcbAppearances.map((appearance: TcbAppearance): string => {
    const { source, p, no, vol, altnames, link } = appearance

    // Add a link if it is not in the JSON but on the TCB sources page.
    const link2 = link || sources.get(source)

    if (!hasLink && link2) {
      hasLink = true
    }

    const linkedSource = link2 ? `[${source}](${link2} "${link2}")` : source

    const finalAltnames = altnames
      ? altnames.map((name) => `"${name}"`).join(", ")
      : undefined

    return (
      (linkedSource ? linkedSource : "") +
      (no ? `, no ${no}` : "") +
      (vol ? `, vol ${vol}` : "") +
      (p ? `, p ${p}` : "") +
      (finalAltnames ? ` (${finalAltnames})` : "")
    )
  })

  // We want each appearance to be shown on its own line (not all on the same
  // line), so because markdown and for consistency, we insert an empty line
  // between each line.
  const appearances = R.intersperse("", formatted)
  return { appearances, hasLink }
}

/**
 * Convert TCB "videos" data (from JSON download) into the form used internally.
 */
function convertTcbVideos(videoUrls: string[]): string[] {
  return videoUrls.map((url, index) => `[${index + 1}](${url} "${url}")`)
}

/**
 * Convert TCB "variant videos" data (from JSON download) into the form used
 * internally.
 */
function convertTcbVariantVideos(videos: string[]): string[] {
  return formatVariantVideos(...groupVariantVideos(videos))
}

/**
 * Takes flat variant videos data and converts it into a nested form that
 * groups urls by variation.
 *
 * @param videos - Video items that typically look like this:
 *                 `http://example.com/video/234 {A1: a variation description}`
 *                 There may be more than one video for a given variation.
 * @return - (1) A Map that maps each variation to an array of urls, and
 *           (2) an array of raw video strings that didn't have the typical form
 *           and so we pass them through unchanged. The latter might not be
 *           necessary, just a fallback in case the input is ever inconsistent.
 */
function groupVariantVideos(
  videos: string[],
): [Map<string, string[]>, string[]] {
  const variantVideosRegex = /(\S*)\s*\{(.*)\}/
  const collected: Map<string, string[]> = new Map()
  const unmatched = []

  for (const video of videos) {
    const matched = video.match(variantVideosRegex)

    if (matched) {
      const [fullMatch, url, variation] = matched
      const urlsForVariation = collected.get(variation)

      if (urlsForVariation) {
        urlsForVariation.push(url)
      } else {
        collected.set(variation, [url])
      }
    } else {
      unmatched.push(video)
    }
  }
  return [collected, unmatched]
}

/**
 * Convert variant videos data into its final form for storage.
 */
function formatVariantVideos(
  collected: Map<string, string[]>,
  unmatched: string[],
): string[] {
  const textLines = []
  const pushLine = (line: string) => textLines.push(line)

  // We want a variation description to be shown on its own line, and then
  // all of the videos for it on the next line, so because markdown and for
  // consistency, we add empty lines to get the result we want.
  for (const [variation, urls] of collected.entries()) {
    textLines.push(variation)
    textLines.push("")
    convertTcbVideos(urls).forEach(pushLine)
    textLines.push("")
  }
  unmatched.forEach(pushLine)

  return textLines
}
