/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import R from "ramda"

import { Dance, SearchResultsDance, DancePart } from "./types"

export const tcbBaseUrl = "http://www.ibiblio.org/contradance/thecallersbox/"

export const danceUrl = (tcbId: string): string =>
  `${tcbBaseUrl}dance.php?id=${tcbId}`

export const danceJsonUrl = (tcbId: string): string =>
  danceUrl(tcbId) + "&format=JSON"

export const tcbIdFromUrl = (url: string): string | undefined => {
  const matched = url.match(/\?id=(\d*)/)
  if (matched) {
    return matched[1]
  } else {
    console.warn("Was not able to get ID from URL.")
    return undefined
  }
}

// Use '\r\n' because '\n' breaks string equality checks when editing a dance.
export const newline = "\r\n"
export const twoNewlines = newline + newline

const newlineRegex = /\r\n|\r|\n/

export const splitAtNewlines = (str: string): string[] => {
  return str.split(newlineRegex)
}

const compareStrings = (a: string, b: string): number => {
  // Ignore case, and if strings are the same return 0.
  const aUpper = a.toUpperCase()
  const bUpper = b.toUpperCase()
  return aUpper < bUpper ? -1 : aUpper > bUpper ? 1 : 0
}

export const compareByTitle = (a: Dance, b: Dance): number =>
  compareStrings(a.title || "", b.title || "")

export function notUndefinedOrNull<T>(item: T | undefined | null): item is T {
  return !!item
}

/**
 * Takes a string and splits it into words/tokens (where there is white space).
 */
export const tokenize = (input: string): string[] =>
  input
    .split(" ")
    .map((item) => item.trim())
    .filter((item) => item)

export const figuresStringFromData = (figures: DancePart[]): string => {
  return figures
    .reduce(
      (
        result: {
          currentSection: string
          lines: string[]
        },
        part: DancePart,
      ) => {
        if (result.currentSection !== part.section) {
          result.lines.push("# " + part.section)
          result.currentSection = part.section
        }
        const line = part.subpart ? "> " + part.text : part.text
        result.lines.push(line)
        return result
      },
      { currentSection: "", lines: [] },
    )
    .lines.join("\n")
}

export const figuresDataFromString = (figures: string): DancePart[] => {
  return splitAtNewlines(figures).reduce(
    (
      result: {
        currentSection: string
        parts: DancePart[]
      },
      rawLine: string,
    ) => {
      const line = rawLine.trim()

      if (!line) {
        // Ignore whitespace only lines.
        return result
      } else if (line.startsWith("#")) {
        result.currentSection = line.slice(1).trim()
      } else {
        const subpart = line.startsWith(">")
        const line2 = subpart ? line.slice(1).trim() : line

        const dancePart: DancePart = {
          text: line2,
          section: result.currentSection,
        }

        // TODO globalize this regex, it is used elsewhere too
        const beatsMatch = line2.match(/^\((\d+)\)/)
        if (beatsMatch) {
          dancePart.beats = Number(beatsMatch[1])
        }

        if (subpart) {
          dancePart.subpart = true
        }

        result.parts.push(dancePart)
      }
      return result
    },
    { currentSection: "", parts: [] },
  ).parts
}

/**
 * Takes a full dance record and returns the search results subset of it.
 *
 * @param dance - A full dance record.
 * @return A search results dance record.
 */
export const searchResultFromDance = (dance: Dance): SearchResultsDance => {
  return R.pick(
    [
      "danceId",
      "tcbId",
      "title",
      "authors",
      "formationBase",
      "status",
      "hasFigures",
      "hasLink",
      "hasVideo",
      "storageStatus",
    ],
    dance,
  )
}

/**
 * Predicate to determine if a number is not `NaN`.
 */
export const isNotNaN = (n: Number): boolean => !Number.isNaN(n)
