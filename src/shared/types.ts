/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import {
  figuresStringFromData,
  figuresDataFromString,
  tokenize,
  splitAtNewlines,
} from "./utils"
import R from "ramda"
import { DanceAttribute } from "../foreground/stores/dance-attributes-store"

export type HtmlInputElements =
  | HTMLInputElement
  | HTMLTextAreaElement
  | HTMLSelectElement

export interface DancePart {
  text: string
  section: string
  beats?: number
  subpart?: boolean
}

// DanceStatus of "none" is used for TCB and non-TCB dances.
// DancePermission of "none" is only used for non-TCB dances.
export type DanceStatus = "deprecated" | "broken" | "none"
export type DancePermission = "search" | "full" | "no_figures" | "none"
export type DanceMixer = "yes" | ""

/**
 * Toggling storage status works like this:
 *
 *   out-partial -> out-full
 *   out-partial -> incoming
 *   out-full -> incoming
 *   incoming -> in
 *   in -> removed
 *   removed -> in
 *
 * Thus we keep track of the dances that have ever been `in`.
 * Then like the recycle bin in an OS, we can offer an unobtrusive UI where
 * 'removed' dances (particularly locally created ones) can be set back to 'in'.
 *
 * 'incoming' is a temporary transitional status for a dance that is moving
 * from 'out' to 'in' and is being fetched from the Caller's Box website.
 *
 * 'out-partial' dances are partial records (from TCB searches) in the database.
 * 'out-full' dances are full records in the database.
 */
export type StorageStatus =
  | "out-partial"
  | "out-full"
  | "incoming"
  | "in"
  | "removed"

// formationBase: In order to handle "other" values, formationBase has to be a
// string type rather than a more specific union type of specific strings.
// But we do use the typical string values for drop-down menus, abbreviations,
// etc.
const formationAbbreviations: Record<string, string> = {
  "Duple Minor - Improper": "DMI",
  "Duple Minor - Becket": "DMB",
  "Duple Minor - Proper": "DMP",
  "Duple Minor - Indecent": "DMIN",
  "Duple Minor - Reverse progression improper": "DMRPI",
  "Duple Minor - Progressed improper": "DMPI",
  "Duple Minor - Cross": "DMC",
  "Duple Minor - Other": "DMO",
  "Triple Minor": "TM",
  "Three Facing Three": "3F3",
  "Four Facing Four": "4F4",
  Triplet: "T",
  "Longways: 5+ couples": "L5+",
  "Other Longways": "OL",
  "Circle Mixer": "CM",
  "Circle of Threesomes": "C3",
  "Sicilian Circle": "SC",
  "Scatter Mixer": "SM",
  "Grid Contra": "GC",
  "Grid Square": "GS",
  Zia: "Z",
  Other: "O",
}

/**
 * Given a formationBase, return an abbreviation for it, or "O" for "Other".
 *
 * Note that "other" appears in formation menus on the Caller's Box site, but
 * the formationBase values in the dances are never "other" but rather specific
 * strings like "Non-longways for 3 couples". Hence this function is needed to
 * provide a catch all of "O". And we want "Other" as a key in the formation
 * abbreviations object so we can use the list of keys for menus, etc.
 *
 * @param formationBase
 * @return An abbreviation of the formationBase or "O" for "Other".
 */
export const getFormationAbbreviation = (formationBase: string): string => {
  return formationAbbreviations[formationBase] || "O"
}

export const formationOptions = [
  "",
  "Duple Minor",
  ...Object.keys(formationAbbreviations),
]

export type ProgressionOption =
  | ""
  | "None"
  | "Single"
  | "Double"
  | "Triple"
  | "Quadruple"
  | "More than Quadruple"
  | "Other/Weird"

export const progressionOptions: ProgressionOption[] = [
  "",
  "None",
  "Single",
  "Double",
  "Triple",
  "Quadruple",
  "More than Quadruple",
  "Other/Weird",
]

export const isProgressionOption = (
  option: string,
): option is ProgressionOption => {
  return progressionOptions.includes(option as ProgressionOption)
}

/**
 * `tcbId` is The Caller's Box id.
 * `figures` is equivalent to `phrases` in the Caller's Box.
 * We have dedicated boolean `hasFigures`, `hasLink`, and `hasVideo` properties
 * because dance data from TCB search results only has this metadata, not the
 * full values. Also `hasLink` and `hasVideo` are useful because the fields
 * (appearances, videos, etc.) can have text but not links and we need to know
 * if they have links. `hasFigures` provides consistency with the other two.
 */
export interface Dance {
  // TCB search results properties:
  tcbId?: string
  title: string
  authors: string[]
  formationBase: string
  status: DanceStatus
  hasFigures: boolean
  hasLink: boolean
  hasVideo: boolean
  storageStatus: StorageStatus

  // TCB full dance properties:
  permission: DancePermission
  formationDetail: string
  figures: DancePart[]
  progression: string
  direction: string
  otherTitles: string[]
  basedOn: string[]
  mixer: DanceMixer
  phraseStructure: string
  music: string[]
  callingNotes: string[]
  appearances: string[]
  videos: string[]
  variantVideos: string[]

  // Additional properties:
  danceId: number
  tcbFigures: DancePart[]
  dateAdded?: string
  tcbBackup?: TcbBackup
  tags: string[]
  attributes: Record<string, string>
  programs: Program[]
}

export function isDance(dance: Dance | SearchResultsDance): dance is Dance {
  return (dance as Dance).figures !== undefined
}

export type FiguresType = "figures" | "tcbFigures"

/**
 * Create a minimal Dance object with sensible default values for the
 * required properties.
 */
export const makeDance = (
  danceId: number,
  storageStatus: StorageStatus,
): Dance => ({
  // TCB search results properties.
  danceId,
  title: "",
  authors: [],
  formationBase: "",
  status: "none",
  hasFigures: false,
  hasLink: false,
  hasVideo: false,
  storageStatus,

  // TCB full dance properties:
  permission: "none",
  formationDetail: "",
  figures: [],
  progression: "",
  direction: "",
  otherTitles: [],
  basedOn: [],
  mixer: "",
  phraseStructure: "",
  music: [],
  callingNotes: [],
  appearances: [],
  videos: [],
  variantVideos: [],

  // Additional properties:
  tcbFigures: [],
  dateAdded: new Date().toJSON(),
  tags: [],
  attributes: {},
  programs: [],
})

export type SearchResultsDance = Pick<
  Dance,
  | "danceId"
  | "tcbId"
  | "title"
  | "authors"
  | "formationBase"
  | "status"
  | "hasFigures"
  | "hasLink"
  | "hasVideo"
  | "storageStatus"
>

export type SearchResultsDanceNoId = Omit<SearchResultsDance, "danceId">

/**
 * Dance.tcbBackup is for storing the original unedited TCB values for fields
 * that have been edited by the user. When a field is edited the original
 * TCB value (if it exists) is copied into tcbBackup.
 *
 * Then when a dance changes in the Caller's Box, we can compare the new version
 * with these earlier stored values and let the user know what has changed.
 *
 * The idea is to provide both the ability to edit all fields to make local
 * changes, but also be able to keep local records in sync with the latest
 * version in the Caller's Box dances, by default and as desired by the user.
 * Also, the current values we care about are always in the same place, whether
 * they came from TCB, were created locally, or were TCB values that were then
 * edited locally. (As opposed to keeping TCB and local versions of each field,
 * located next to each other at the same level.)
 *
 * `figures` is a special case, so is not included. The TCB figures are not
 * stored in `Dance.tcbBackup.figures` but in `Dance.tcbFigures`.
 */
export type TcbBackup = Partial<
  Pick<
    Dance,
    | "title"
    | "authors"
    | "formationBase"
    | "status"
    | "permission"
    | "formationDetail"
    | "progression"
    | "direction"
    | "otherTitles"
    | "basedOn"
    | "mixer"
    | "phraseStructure"
    | "music"
    | "callingNotes"
    | "appearances"
    | "videos"
    | "variantVideos"
  >
>

const TcbBackupKeys = [
  "title",
  "authors",
  "formationBase",
  "status",
  "permission",
  "formationDetail",
  "progression",
  "direction",
  "otherTitles",
  "basedOn",
  "mixer",
  "phraseStructure",
  "music",
  "callingNotes",
  "appearances",
  "videos",
  "variantVideos",
]

export function isTcbBackupField(key: string): key is keyof TcbBackup {
  return TcbBackupKeys.includes(key)
}

/**
 * Standard Dance type plus special "editable" versions of some properies, in
 * string form for use in the create/edit form.
 */
export type EditableDance = Omit<
  Dance,
  | "figures"
  | "tcbFigures"
  | "tags"
  | "authors"
  | "callingNotes"
  | "otherTitles"
  | "basedOn"
  | "music"
  | "appearances"
  | "videos"
  | "variantVideos"
> & {
  editableFigures: string
  editableTcbFigures: string
  editableTags: string
  editableAuthors: string
  editableCallingNotes: string
  editableOtherTitles: string
  editableBasedOn: string
  editableMusic: string
  editableAppearances: string
  editableVideos: string
  editableVariantVideos: string
}

/**
 * Construct an editable dance from a dance. If no dance is supplied create and
 * use a default one. Used to prepare dances for use with the dance edit form.
 * Converts some of the data from storable form (e.g. objects, arrays) into
 * editable form (e.g. strings).
 *
 * @param dance - Optional dance to make editable.
 * @return A dance that's editable for use in the dance edit form.
 */
export const makeEditableDance = (dance: Dance): EditableDance => {
  const omitted = R.omit(
    [
      "figures",
      "tcbFigures",
      "tags",
      "authors",
      "callingNotes",
      "otherTitles",
      "basedOn",
      "music",
      "appearances",
      "videos",
      "variantVideos",
    ],
    dance,
  )

  const editableDance = Object.assign({}, omitted, {
    editableFigures: figuresStringFromData(dance.figures),
    editableTcbFigures: figuresStringFromData(dance.tcbFigures),
    editableTags: dance.tags.join(" "),
    editableAuthors: dance.authors.join("\n"),
    editableCallingNotes: dance.callingNotes.join("\n"),
    editableOtherTitles: dance.otherTitles.join("\n"),
    editableBasedOn: dance.basedOn.join("\n"),
    editableMusic: dance.music.join("\n"),
    editableAppearances: dance.appearances.join("\n"),
    editableVideos: dance.videos.join("\n"),
    editableVariantVideos: dance.variantVideos.join("\n"),
  })
  return editableDance
}

/**
 * Convert an editable dance into a non-editable dance object. Used when saving
 * dances that have been edited, to convert the editable forms of the data into
 * storable forms.
 *
 * @param danceIn - The editable dance to convert.
 * @return A non-editable dance.
 */
export const danceFromEditableDance = (danceIn: EditableDance): Dance => {
  const omitted = R.omit(
    [
      "editableFigures",
      "editableTcbFigures",
      "editableTags",
      "editableAuthors",
      "editableCallingNotes",
      "editableOtherTitles",
      "editableBasedOn",
      "editableMusic",
      "editableAppearances",
      "editableVideos",
      "editableVariantVideos",
    ],
    danceIn,
  )

  const danceOut = Object.assign(omitted, {
    figures: figuresDataFromString(danceIn.editableFigures),
    tcbFigures: figuresDataFromString(danceIn.editableTcbFigures),
    tags: tokenize(danceIn.editableTags),
    authors: makeNotEditable(danceIn.editableAuthors),
    callingNotes: makeNotEditable(danceIn.editableCallingNotes),
    otherTitles: makeNotEditable(danceIn.editableOtherTitles),
    basedOn: makeNotEditable(danceIn.editableBasedOn),
    music: makeNotEditable(danceIn.editableMusic),
    appearances: makeNotEditable(danceIn.editableAppearances),
    videos: makeNotEditable(danceIn.editableVideos),
    variantVideos: makeNotEditable(danceIn.editableVariantVideos),
  })
  return danceOut
}

const makeNotEditable = (value: string): string[] => {
  return value === "" ? [] : splitAtNewlines(value)
}

export type FiguresSearchMode = "any_any" | "all_any" | "all_given"

export interface SearchQuery {
  title?: string
  authors?: string
  formationBase?: string
  progression?: ProgressionOption
  pos_lines?: string[]
  neg_lines?: string[]
  pos_mode: FiguresSearchMode
  neg_mode: FiguresSearchMode
  tags: string[]
}

/**
 * Create a minimal SearchQuery object with defaults for required properties.
 *
 * @param props - Properties to set on the new SearchQuery object.
 */
export const makeSearchQuery = (
  props: Partial<SearchQuery> = {},
): SearchQuery => {
  return Object.assign(
    {
      pos_mode: "all_given",
      neg_mode: "any_any",
      tags: [],
    },
    props,
  )
}

export type DanceToView = Dance | SearchResultsDance

export type View =
  | "searchDances"
  | "editDance"
  | "programsBrowse"
  | "programsEdit"
  | "admin"
  | "adminAddDanceAttribute"

export type SearchType = "local" | "tcb" | "local_tcb"

export interface SearchInput {
  title: string
  authors: string
  formationBase: string
  progression: ProgressionOption
  pos_mode: FiguresSearchMode
  pos_lines: string
  neg_mode: FiguresSearchMode
  neg_lines: string
  tags: string
  search_type: SearchType
}

export const makeSearchInput = (
  props: Partial<SearchInput> = {},
): SearchInput => {
  return Object.assign(
    {
      title: "",
      authors: "",
      formationBase: "",
      progression: "",
      pos_mode: "all_given",
      pos_lines: "",
      neg_mode: "any_any",
      neg_lines: "",
      tags: "",
      search_type: "local_tcb",
    },
    props,
  )
}

export type SearchResultsState =
  | "combined"
  | "combinedStillSearching"
  | "local"
  | "callersBox"
  | "callersBoxSearching"

export interface SearchResults {
  state: SearchResultsState
  searchInput: SearchInput
  localDances: SearchResultsDance[]
  totalLocalDances: number

  // TcbSearchResults properties
  tcbDances: SearchResultsDance[]
  totalTcbDances: number | undefined
  tcbDatabaseUpdated?: string
  tcbSiteCodeUpdated?: string
  criteria?: string
}

export type LocalSearchResults = Pick<
  SearchResults,
  "localDances" | "totalLocalDances"
>

export type TcbSearchResults = Pick<
  SearchResults,
  | "tcbDances"
  | "totalTcbDances"
  | "tcbDatabaseUpdated"
  | "tcbSiteCodeUpdated"
  | "criteria"
>

export interface SearchResultsMessages {
  header?: string
  criteria?: string
  footer?: string
}

export interface DancePatch {
  danceId: number
  changes: Partial<Dance>
}

/**
 * On default values: tables are created with SQL DEFAULT values if they are
 * specified, but we aren't relying on sqlite for creating default values.
 * Apparently you have to have an INSERT statement that omits a given column,
 * and then sqlite will use the default value for that column instead. However,
 * we create INSERT statements ahead of time with all the columns included,
 * so we handle any default values in JS/TS before insertion into the database.
 */
export interface TableSpec<T> {
  name: string
  columns: Map<
    keyof T,
    {
      sqlType: string
      jsType?: JsType
      primaryKey?: boolean
      defaultValue?: any
    }
  >
  foreignKeys?: string
}

export type JsType = "boolean" | "json" | undefined

export interface Program {
  programId: number
  location: string
  date: string
  music: string
  notes: string
  dances?: ProgramItem[]
}

export const makeProgram = (id: number): Program => ({
  programId: id,
  location: "",
  date: "",
  music: "",
  notes: "",
  dances: [],
})

export interface ProgramPatch {
  programId: number
  changes: Partial<Program>
}

export interface ProgramDanceDbRow {
  programDanceId: number
  programId: number
  danceId: number
  danceIndex: number
  spacer: number
}

export type ProgramItem = ProgramDance | ProgramSpacer

export interface ProgramDance {
  programDanceId: number
  dance: Dance
}

export interface ProgramSpacer {
  programDanceId: number
  spacer: true
}

export function isProgramSpacer(item: ProgramItem): item is ProgramSpacer {
  return (item as ProgramSpacer).spacer !== undefined
}

export function isProgramDance(item: ProgramItem): item is ProgramDance {
  return (item as ProgramDance).dance !== undefined
}

export interface Settings {
  id: 1
  danceAttributes: DanceAttribute[]
}
