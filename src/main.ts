/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import { app, BrowserWindow, ipcMain, IpcMainEvent, Menu } from "electron"
import * as path from "path"

import { CommandToForeground } from "./foreground/stores/inter-process-communication"
import { View } from "./shared/types"

// app.allowRendererProcessReuse needs to be set to false for now, until
// better-sqlite3 supports NAPI.
// https://github.com/electron/electron/issues/18397
// https://github.com/JoshuaWise/better-sqlite3/issues/458
app.allowRendererProcessReuse = false

const isMac = process.platform === "darwin"

// Keep a global reference to window objects, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let foregroundWindow: Electron.BrowserWindow | undefined
let backgroundWindow: Electron.BrowserWindow | undefined

function createBackgroundWindow() {
  // Create a hidden window to use as a background process.
  backgroundWindow = new BrowserWindow({
    // For development purposes, keep the background window visible for now.
    // show: false,
    webPreferences: { nodeIntegration: true },
  })

  backgroundWindow.loadFile(
    path.join(__dirname, "../src/background/background.html"),
  )
}

function createForegroundWindow() {
  // Create the main browser window. Create and load it last so it is the one
  // that is focused for integration tests.
  foregroundWindow = new BrowserWindow({
    height: 768,
    width: 1366,
    webPreferences: { nodeIntegration: true },
  })

  // mainWindow.maximize();

  // and load the index.html of the app.
  foregroundWindow.loadFile(
    path.join(__dirname, "../src/foreground/index.html"),
  )

  // Open the DevTools. Note that tests will fail with this uncommented.
  // mainWindow.webContents.openDevTools();
  // backgroundWindow.webContents.openDevTools();

  // Emitted when the window is closed.
  foregroundWindow.on("closed", () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    foregroundWindow = undefined

    // Quit the app on foreground window close, unless we are on macOS.
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q.
    if (!isMac) {
      app.quit()
    }
  })

  // TODO: React Dev Tools Extension
  // const os = require("os");
  // BrowserWindow.addDevToolsExtension(
  //   path.join(
  //     os.homedir(),
  //     "/.config/chromium/Default/Extensions/fmkadmapgofadopljbjfkapdkoienihi/4.0.6_0"
  //   )
  // );
}

export type CommandToMain = "viewChanged"

function setUpCommunication() {
  ipcMain.on(
    "message-to-background",
    (event: IpcMainEvent, message: any): void => {
      if (backgroundWindow) {
        backgroundWindow.webContents.send("message-to-background", message)
      }
    },
  )
  ipcMain.on(
    "message-to-foreground",
    (event: IpcMainEvent, message: any): void => {
      if (foregroundWindow) {
        foregroundWindow.webContents.send("message-to-foreground", message)
      }
    },
  )
  ipcMain.on(
    "message-to-main",
    (
      event: IpcMainEvent,
      message: { command: CommandToMain; payload: any },
    ): void => {
      const { command, payload } = message
      switch (command) {
        case "viewChanged":
          onViewChanged(payload)
          break
      }
    },
  )
}

// The menu items (by ID) to enable for each view.
const viewToEnabledMenuItemIds: Map<View, Set<string>> = new Map([
  [
    "searchDances",
    new Set([
      "new-dance",
      "new-program",
      "change-view-dances",
      "change-view-programs",
      "change-view-admin",
      "all-dances-in-local-collection",
    ]),
  ],
  [
    "programsBrowse",
    new Set([
      "new-dance",
      "new-program",
      "change-view-dances",
      "change-view-programs",
      "change-view-admin",
    ]),
  ],
  [
    "admin",
    new Set([
      "new-dance",
      "new-program",
      "change-view-dances",
      "change-view-programs",
      "change-view-admin",
    ]),
  ],
  ["editDance", new Set()],
  ["programsEdit", new Set([])],
])

function onViewChanged(view: View) {
  const enableIds = viewToEnabledMenuItemIds.get(view)
  for (const id of [
    // IDs of all disable-able menu items.
    "new-dance",
    "new-program",
    "change-view-dances",
    "change-view-programs",
    "change-view-admin",
    "all-dances-in-local-collection",
  ]) {
    menu.getMenuItemById(id).enabled = Boolean(enableIds?.has(id))
  }
}

function mainToForeground(command: CommandToForeground, payload: any): void {
  if (foregroundWindow) {
    foregroundWindow.webContents.send("message-to-foreground", {
      command,
      payload,
    })
  }
}

/**
 * Allow links with `target="_blank"` to open in the user's default browser.
 */
function setUpExternalLinks() {
  if (foregroundWindow) {
    foregroundWindow.webContents.on("new-window", (event, url) => {
      event.preventDefault()
      require("electron").shell.openExternal(url)
    })
  }
}

const menuTemplate: any = [
  // { role: 'appMenu' }
  ...(isMac
    ? [
        {
          label: app.name,
          submenu: [
            { role: "about" },
            { type: "separator" },
            { role: "services" },
            { type: "separator" },
            { role: "hide" },
            { role: "hideothers" },
            { role: "unhide" },
            { type: "separator" },
            { role: "quit" },
          ],
        },
      ]
    : []),
  // { role: 'fileMenu' }
  {
    label: "File",
    submenu: [
      {
        label: "New Dance",
        accelerator: "CommandOrControl+Shift+D",
        click: () => mainToForeground("createNewDance", undefined),
        id: "new-dance",
      },
      {
        label: "New Program",
        accelerator: "CommandOrControl+N",
        click: () => mainToForeground("createNewProgram", undefined),
        id: "new-program",
      },
      { type: "separator" },
      isMac ? { role: "close" } : { role: "quit" },
    ],
  },
  // { role: 'editMenu' }
  {
    label: "Edit",
    submenu: [
      { role: "undo" },
      { role: "redo" },
      { type: "separator" },
      { role: "cut" },
      { role: "copy" },
      { role: "paste" },
      ...(isMac
        ? [
            { role: "pasteAndMatchStyle" },
            { role: "delete" },
            { role: "selectAll" },
            { type: "separator" },
            {
              label: "Speech",
              submenu: [{ role: "startspeaking" }, { role: "stopspeaking" }],
            },
          ]
        : [{ role: "delete" }, { type: "separator" }, { role: "selectAll" }]),
    ],
  },
  // { role: 'viewMenu' }
  {
    label: "View",
    submenu: [
      {
        label: "Dances",
        accelerator: "CommandOrControl+D",
        click: () => mainToForeground("setView", "searchDances"),
        id: "change-view-dances",
      },
      {
        label: "Programs",
        accelerator: "CommandOrControl+P",
        click: () => mainToForeground("setView", "programsBrowse"),
        id: "change-view-programs",
      },
      {
        label: "Admin",
        accelerator: "CommandOrControl+Shift+A",
        click: () => mainToForeground("setView", "admin"),
        id: "change-view-admin",
      },
      { type: "separator" },
      {
        label: "All Dances in Local Collection",
        accelerator: "CommandOrControl+L",
        click: () => mainToForeground("startLocalSearch", undefined),
        id: "all-dances-in-local-collection",
      },
      { type: "separator" },
      { role: "reload" },
      { role: "forcereload" },
      { role: "toggledevtools" },
      { type: "separator" },
      { role: "resetzoom" },
      { role: "zoomin" },
      { role: "zoomout" },
      { type: "separator" },
      { role: "togglefullscreen" },
    ],
  },
  // { role: 'windowMenu' }
  {
    label: "Window",
    submenu: [
      { role: "minimize" },
      { role: "zoom" },
      ...(isMac
        ? [
            { type: "separator" },
            { role: "front" },
            { type: "separator" },
            { role: "window" },
          ]
        : [{ role: "close" }]),
    ],
  },
  {
    role: "help",
    submenu: [
      {
        label: "Learn More",
        click: async () => {
          const { shell } = require("electron")
          await shell.openExternal(
            "https://gitlab.com/paulmorris/the-callers-app",
          )
        },
      },
    ],
  },
]

const menu = Menu.buildFromTemplate(menuTemplate)
Menu.setApplicationMenu(menu)

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", () => {
  createBackgroundWindow()
  createForegroundWindow()
  setUpCommunication()
  setUpExternalLinks()
})

app.on("activate", () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (foregroundWindow === null) {
    createForegroundWindow()
  }
})
