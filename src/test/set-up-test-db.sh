#!/bin/bash

set -o errexit # Exit on error

if [ -f ./database/database.db ]; then
  mv ./database/database.db ./database/database-temp-renamed.db
fi
