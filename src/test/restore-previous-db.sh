#!/bin/bash

set -o errexit # Exit on error

if [ -f ./database/database.db ]; then
  rm ./database/database.db
fi

if [ -f ./database/database-temp-renamed.db ]; then
  mv ./database/database-temp-renamed.db ./database/database.db
fi
