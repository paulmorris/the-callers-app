/*
Copyright 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

/* globals describe it */

import * as chai from "chai"

import {
  tokensToAst,
  makeBooleanQueryFn,
} from "../../background/database/dexie/search-boolean-operators"
import { tokenize } from "../../shared/utils"

const expect = chai.expect

describe("Dexie Search Boolean Operators", () => {
  it("can handle simple AND operator queries", () => {
    const query = "aaa AND bbb"
    const ast = tokensToAst(tokenize(query))
    const queryFn = makeBooleanQueryFn(ast)

    expect(ast).to.eql(["AND", "aaa", "bbb"])
    expect(queryFn([])).to.equal(false)
    expect(queryFn(["aaa"])).to.equal(false)
    expect(queryFn(["bbb"])).to.equal(false)
    expect(queryFn(["aaa", "bbb"])).to.equal(true)
  })

  it("can handle simple OR operator queries", () => {
    const query = "aaa OR bbb"
    const ast = tokensToAst(tokenize(query))
    const queryFn = makeBooleanQueryFn(ast)

    expect(ast).to.eql(["OR", "aaa", "bbb"])
    expect(queryFn([])).to.equal(false)
    expect(queryFn(["aaa"])).to.equal(true)
    expect(queryFn(["bbb"])).to.equal(true)
    expect(queryFn(["aaa", "bbb"])).to.equal(true)
  })

  it("can handle simple NOT operator queries", () => {
    const query = "NOT aaa"
    const ast = tokensToAst(tokenize(query))
    const queryFn = makeBooleanQueryFn(ast)

    expect(ast).to.eql(["NOT", "aaa"])
    expect(queryFn([])).to.equal(true)
    expect(queryFn(["aaa"])).to.equal(false)
  })

  it("can handle basic AND operator queries", () => {
    const query = "aaa AND bbb AND ccc"
    const ast = tokensToAst(tokenize(query))
    const queryFn = makeBooleanQueryFn(ast)

    expect(ast).to.eql(["AND", ["AND", "aaa", "bbb"], "ccc"])
    expect(queryFn([])).to.equal(false)
    expect(queryFn(["aaa"])).to.equal(false)
    expect(queryFn(["bbb"])).to.equal(false)
    expect(queryFn(["ccc"])).to.equal(false)
    expect(queryFn(["aaa", "bbb"])).to.equal(false)
    expect(queryFn(["aaa", "ccc"])).to.equal(false)
    expect(queryFn(["bbb", "ccc"])).to.equal(false)
    expect(queryFn(["aaa", "bbb", "ccc"])).to.equal(true)
  })

  it("can handle basic AND, OR, NOT operator queries", () => {
    const query = "aaa AND NOT bbb OR ccc"
    const ast = tokensToAst(tokenize(query))
    const queryFn = makeBooleanQueryFn(ast)

    expect(ast).to.eql(["OR", ["AND", "aaa", ["NOT", "bbb"]], "ccc"])
    expect(queryFn([])).to.equal(false)
    expect(queryFn(["aaa"])).to.equal(true)
    expect(queryFn(["bbb"])).to.equal(false)
    expect(queryFn(["ccc"])).to.equal(true)
    expect(queryFn(["aaa", "bbb"])).to.equal(false)
    expect(queryFn(["aaa", "ccc"])).to.equal(true)
    expect(queryFn(["bbb", "ccc"])).to.equal(true)
    expect(queryFn(["aaa", "bbb", "ccc"])).to.equal(true)
  })

  it("can handle more complex boolean queries", () => {
    const query =
      "zero AND NOT one   OR   blue AND two    " +
      "OR    NOT five    OR    NOT three AND six"

    const ast = tokensToAst(tokenize(query))
    const queryFn = makeBooleanQueryFn(ast)

    const expectedAst = [
      "OR",
      [
        "OR",
        ["OR", ["AND", "zero", ["NOT", "one"]], ["AND", "blue", "two"]],
        ["NOT", "five"],
      ],
      ["AND", ["NOT", "three"], "six"],
    ]

    expect(ast).to.eql(expectedAst)
    expect(queryFn([])).to.eql(true) // NOT five
    expect(queryFn(["five", "six"])).to.eql(true) // six AND NOT three
    expect(queryFn(["five", "six", "three"])).to.eql(false) // six AND NOT three
    expect(queryFn(["five", "blue", "two"])).to.eql(true) // blue AND two
    expect(queryFn(["five", "zero"])).to.eql(true) // zero AND NOT one
    expect(queryFn(["five", "zero", "one"])).to.eql(false) // zero AND NOT one
  })

  it("converts queries without operators to OR queries", () => {
    const query = "aaa bbb ccc"
    const ast = tokensToAst(tokenize(query))
    const queryFn = makeBooleanQueryFn(ast)

    expect(ast).to.eql(["OR", ["OR", "aaa", "bbb"], "ccc"])
    expect(queryFn(["aaa"])).to.equal(true)
    expect(queryFn(["bbb"])).to.equal(true)
    expect(queryFn(["ccc"])).to.equal(true)
  })

  it("inserts OR as needed in queries with operators", () => {
    const query = "aaa bbb AND ccc"
    const ast = tokensToAst(tokenize(query))
    const queryFn = makeBooleanQueryFn(ast)

    expect(ast).to.eql(["OR", "aaa", ["AND", "bbb", "ccc"]])
    expect(queryFn(["aaa"])).to.equal(true)
    expect(queryFn(["bbb"])).to.equal(false)
    expect(queryFn(["ccc"])).to.equal(false)
    expect(queryFn(["bbb", "ccc"])).to.equal(true)
  })

  it("inserts OR as needed in queries with NOT operators", () => {
    const query = "aaa bbb NOT ccc"
    const ast = tokensToAst(tokenize(query))
    const queryFn = makeBooleanQueryFn(ast)

    expect(ast).to.eql(["OR", ["OR", "aaa", "bbb"], ["NOT", "ccc"]])
    expect(queryFn(["aaa"])).to.equal(true)
    expect(queryFn(["bbb"])).to.equal(true)
    expect(queryFn(["ccc"])).to.equal(false)
    expect(queryFn(["bbb", "ccc"])).to.equal(true)
  })
})
