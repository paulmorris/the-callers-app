/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

/* globals describe it */

import * as chai from "chai"
import rewire from "rewire"

import { SearchQuery, makeSearchQuery } from "../../shared/types"
import { tokenize } from "../../shared/utils"
import {
  MakeBooleanWherePart,
  MakeWhereClause,
  InsertOrs,
} from "../../background/database/sqlite/search"

const expect = chai.expect

// Use 'rewire' to unit test functions that are not exported.
const Search = rewire("../../background/database/sqlite/search")

const makeWhereClause: MakeWhereClause = Search.makeWhereClause

const makeBooleanWherePart: MakeBooleanWherePart = Search.__get__(
  "makeBooleanWherePart",
)

const insertOrs: InsertOrs = Search.__get__("insertOrs")

describe("SQLite Search: SQL WHERE clauses", () => {
  it("'insertOrs' inserts 'OR' into boolean search queries", () => {
    const queries: Array<[string, Array<string>]> = [
      ["aaa bbb ccc", ["aaa", "OR", "bbb", "OR", "ccc"]],
      ["aaa bbb OR ccc", ["aaa", "OR", "bbb", "OR", "ccc"]],
      ["aaa bbb AND ccc", ["aaa", "OR", "bbb", "AND", "ccc"]],
      ["aaa bbb NOT ccc", ["aaa", "OR", "bbb", "OR", "NOT", "ccc"]],
    ]
    for (const [query, result] of queries) {
      const queryWithOrs = insertOrs(tokenize(query))
      expect(queryWithOrs).to.eql(result)
    }
  })

  it("'makeBooleanWherePart' makes WHERE clause parts for boolean AND/OR/NOT searches", () => {
    const queries: Array<[string, string, string[]]> = [
      ["aaa AND bbb", "(tags LIKE ? AND tags LIKE ?)", ["%aaa%", "%bbb%"]],
      ["aaa OR bbb", "(tags LIKE ? OR tags LIKE ?)", ["%aaa%", "%bbb%"]],
      ["NOT aaa", "(tags NOT LIKE ?)", ["%aaa%"]],
      [
        "aaa AND bbb AND ccc",
        "(tags LIKE ? AND tags LIKE ? AND tags LIKE ?)",
        ["%aaa%", "%bbb%", "%ccc%"],
      ],
      [
        "aaa AND NOT bbb OR ccc",
        "(tags LIKE ? AND tags NOT LIKE ? OR tags LIKE ?)",
        ["%aaa%", "%bbb%", "%ccc%"],
      ],
      [
        "zero AND NOT one   OR   blue AND two    OR    NOT five    OR    NOT three AND six",
        "(tags LIKE ? AND tags NOT LIKE ? OR tags LIKE ? AND tags LIKE ? OR tags NOT LIKE ? OR tags NOT LIKE ? AND tags LIKE ?)",
        ["%zero%", "%one%", "%blue%", "%two%", "%five%", "%three%", "%six%"],
      ],
      // The following have 'OR' missing.
      [
        "aaa bbb ccc",
        "(tags LIKE ? OR tags LIKE ? OR tags LIKE ?)",
        ["%aaa%", "%bbb%", "%ccc%"],
      ],
      [
        "aaa bbb AND ccc",
        "(tags LIKE ? OR tags LIKE ? AND tags LIKE ?)",
        ["%aaa%", "%bbb%", "%ccc%"],
      ],
      [
        "aaa bbb OR ccc",
        "(tags LIKE ? OR tags LIKE ? OR tags LIKE ?)",
        ["%aaa%", "%bbb%", "%ccc%"],
      ],
      [
        "aaa bbb NOT ccc",
        "(tags LIKE ? OR tags LIKE ? OR tags NOT LIKE ?)",
        ["%aaa%", "%bbb%", "%ccc%"],
      ],
    ]
    for (const [query, whereResult, paramsResult] of queries) {
      const [wherePart, params] = makeBooleanWherePart(tokenize(query), "tags")
      expect(wherePart).to.equal(whereResult)
      expect(params).to.eql(paramsResult)
    }
  })

  it("'makeWhereClause' makes WHERE clauses for searches", () => {
    const queries: Array<[SearchQuery, string, string[]]> = [
      [
        makeSearchQuery({ title: "The Devil's Backbone" }),
        "WHERE storageStatus IS ? AND title LIKE ?",
        ["in", "%The Devil's Backbone%"],
      ],
      [
        makeSearchQuery({ authors: "Bill Summers" }),
        "WHERE storageStatus IS ? AND authors LIKE ?",
        ["in", "%Bill Summers%"],
      ],
      [
        makeSearchQuery({ formationBase: "Duple Minor - Improper" }),
        "WHERE storageStatus IS ? AND formationBase LIKE ?",
        ["in", "%Duple Minor - Improper%"],
      ],
      [
        makeSearchQuery({
          title: "Mowing the Lawn",
          authors: "James Madison",
          formationBase: "Duple Minor - Becket",
        }),
        "WHERE storageStatus IS ? AND title LIKE ? AND authors LIKE ? AND formationBase LIKE ?",
        [
          "in",
          "%Mowing the Lawn%",
          "%James Madison%",
          "%Duple Minor - Becket%",
        ],
      ],
      [
        makeSearchQuery({ tags: ["red", "blue"] }),
        "WHERE storageStatus IS ? AND (tags LIKE ? OR tags LIKE ?)",
        ["in", "%red%", "%blue%"],
      ],
      [
        makeSearchQuery({ tags: ["red", "OR", "blue"] }),
        "WHERE storageStatus IS ? AND (tags LIKE ? OR tags LIKE ?)",
        ["in", "%red%", "%blue%"],
      ],
      [
        makeSearchQuery({ tags: ["red", "AND", "blue"] }),
        "WHERE storageStatus IS ? AND (tags LIKE ? AND tags LIKE ?)",
        ["in", "%red%", "%blue%"],
      ],
      [
        makeSearchQuery({ tags: ["red", "NOT", "blue"] }),
        "WHERE storageStatus IS ? AND (tags LIKE ? OR tags NOT LIKE ?)",
        ["in", "%red%", "%blue%"],
      ],
    ]
    for (const [query, whereResult, paramsResult] of queries) {
      const [where, params] = makeWhereClause(query)
      expect(where).to.equal(whereResult)
      expect(params).to.eql(paramsResult)
    }
  })
})
