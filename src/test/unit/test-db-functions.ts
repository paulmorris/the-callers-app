/*
Copyright 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

/* globals describe it */

/*
// Commented out because of a pesky version mismatch issue between the
// NODE_MODULE_VERSION of the node used to run the tests and the
// NODE_MODULE_VERSION of electron, for the native module for better-sqlite.
//
// Error: The module '/home/paul/code/callers-app-electron/node_modules/better-sqlite3/build/Release/better_sqlite3.node'
// was compiled against a different Node.js version using
// NODE_MODULE_VERSION 76. This version of Node.js requires
// NODE_MODULE_VERSION 79. Please try re-compiling or re-installing
// the module (for instance, using `npm rebuild` or `npm install`).

import * as chai from "chai"
import rewire from "rewire"

import { dancesData, getSearchResultsByTcbIds } from "../dances-data"
import {
  dbAddDance,
  dbGetDanceById,
  dbGetDanceByTcbId,
  dbGetAllTcbIds,
  dbSearchForDances,
  dbUpdateDance,
  dbClearDancesTable,
  dbCreateDancesTableIfNotExists,
} from "../../background/database/sqlite/dances"
import {
  Dance,
  SearchQuery,
  makeSearchQuery,
  SearchResultsDance,
} from "../../shared/types"

const expect = chai.expect

// Use 'rewire' to unit test functions that are not exported.
const Dances = rewire("../../background/database/sqlite/dances")
const dbGetDanceCount = Dances.__get__("dbGetDanceCount")

describe("Test database functions", () => {
  before(() => {
    dbCreateDancesTableIfNotExists()
    dbClearDancesTable()
  })

  after(() => {
    dbClearDancesTable()
  })

  it("'dbAddDance', 'dbGetDanceById', 'dbGetDanceByTcbId' add and get dances", () => {
    for (const dance of dancesData as Dance[]) {
      const added1 = dbAddDance(dance)
      expect(added1).to.eql(dance)

      const added2 = dance.tcbId && dbGetDanceByTcbId(dance.tcbId)
      expect(added2).to.eql(dance)

      const added3 = added1.danceId && dbGetDanceById(added1.danceId)
      expect(added3).to.eql(dance)
    }
  })

  it("'gdbGetAllTcbIds' gets all the tcbIds", () => {
    const tcbIds = dbGetAllTcbIds()
    expect(tcbIds).to.eql([
      "2057",
      "4836",
      "7308",
      "2826",
      "10263",
      "10305",
      "74",
      "1",
      "7876",
      "133",
      "10382",
    ])
  })

  it("'dbGetDanceCount' gets the correct dance count", () => {
    const count = dbGetDanceCount()
    expect(count).to.equal(dancesData.length)
  })

  it("'dbSearchForDances' searches by title, authors, formationBase", () => {
    const queries: Array<[SearchQuery, SearchResultsDance[]]> = [
      [makeSearchQuery({ title: "ashtnaothnasntaisenth" }), []],
      [
        makeSearchQuery({ title: "nice comb" }),
        getSearchResultsByTcbIds(["7876", "1"]),
        // 1 - "A Nice Combination" by Hubert
        // 7876 - "A Nice Combination" variation by Hoffman
      ],
      [
        makeSearchQuery({ authors: "bob" }),
        getSearchResultsByTcbIds(["10263", "10305"]),
        // 10263 - "Another Slice of Pinewoods" by Bob Isaacs, Chris Weiler
        // 10305 - "Bob and Laura's 35th" by Bob Dalsemer
      ],
      [
        makeSearchQuery({ formationBase: "Circle Mixer" }),
        getSearchResultsByTcbIds(["10305"]),
        // 10305 - "Bob and Laura's 35th" by Bob Dalsemer
      ],
    ]
    for (const [query, result] of queries) {
      const { totalLocalDances, localDances } = dbSearchForDances(query)
      expect(localDances).to.eql(result)
      expect(totalLocalDances).to.equal(dancesData.length)
    }
  })
})
*/
