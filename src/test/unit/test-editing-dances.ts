/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

/* globals describe it */

import * as chai from "chai"
import rewire from "rewire"

import { dancesData } from "../dances-data"
import R from "ramda"

const expect = chai.expect

// Use 'rewire' to unit test functions that are not exported.
const DancesEditStore = rewire("../../foreground/stores/dances-edit-store.ts")

const getSimpleDanceChanges = DancesEditStore.__get__("getSimpleDanceChanges")
const getNewTcbBackup = DancesEditStore.__get__("getNewTcbBackup")

describe("Foreground Dance Editing Functions: getSimpleDanceChanges", () => {
  it("Handles basic changes", () => {
    const dance = dancesData[0]
    const editedDance = R.clone(dance)

    editedDance.title = editedDance.title + " EDITED"
    editedDance.formationBase = editedDance.formationBase + " ANOTHER AUTHOR"

    const changes = getSimpleDanceChanges(dance, editedDance)

    expect(changes).to.eql({
      title: editedDance.title,
      formationBase: editedDance.formationBase,
    })
  })
})

describe("Foreground Dance Editing Functions: getNewTcbBackup", () => {
  it("One change to add, another change is already in the backup", () => {
    const dance = {
      title: "current",
      formationBase: "original",
      callingNotes: "current",
      tcbBackup: { title: "original", callingNotes: "original" },
    }
    const changes = { formationBase: "new", callingNotes: "new" }

    expect(getNewTcbBackup(dance, changes)).to.eql({
      title: "original",
      formationBase: "original",
      callingNotes: "original",
    })
  })

  it("Changing something that's already in the backup", () => {
    const dance = {
      title: "current",
      formationBase: "original",
      callingNotes: "current",
      tcbBackup: { title: "original", callingNotes: "original" },
    }
    const changes = { title: "new" }

    expect(getNewTcbBackup(dance, changes)).to.eql({
      title: "original",
      callingNotes: "original",
    })
  })

  it("Handles undefined tcbBackup", () => {
    const dance = { title: "original", tcbBackup: undefined }
    const changes = { title: "new" }

    expect(getNewTcbBackup(dance, changes)).to.eql({ title: "original" })
  })

  it("Handles empty object tcbBackup", () => {
    const dance = { title: "original", tcbBackup: {} }
    const changes = { title: "new" }

    expect(getNewTcbBackup(dance, changes)).to.eql({ title: "original" })
  })

  it("A change that matches a backup entry removes it from the backup", () => {
    const tcbBackup = {
      title: "current",
      formationBase: "original",
      callingNotes: "current",
      tcbBackup: { title: "original", callingNotes: "original" },
    }
    const changes = { title: "original" }

    expect(getNewTcbBackup(tcbBackup, changes)).to.eql({
      callingNotes: "original",
    })
  })

  it("All backup entries removed", () => {
    const tcbBackup = {
      title: "current",
      formationBase: "original",
      callingNotes: "current",
      tcbBackup: { title: "original", callingNotes: "original" },
    }
    const changes = { title: "original", callingNotes: "original" }

    expect(getNewTcbBackup(tcbBackup, changes)).to.equal(undefined)
  })

  it("All backup entries removed, one added", () => {
    const tcbBackup = {
      title: "current",
      formationBase: "original",
      callingNotes: "current",
      tcbBackup: { title: "original", callingNotes: "original" },
    }
    const changes = {
      title: "original",
      callingNotes: "original",
      formationBase: "new",
    }

    expect(getNewTcbBackup(tcbBackup, changes)).to.eql({
      formationBase: "original",
    })
  })
})
