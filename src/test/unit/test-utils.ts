/*
Copyright 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

/* globals describe it */

import * as chai from "chai"

import { tokenize } from "../../shared/utils"

const expect = chai.expect

describe("Test Utils", () => {
  it("can tokenize tags input", () => {
    expect(tokenize("")).to.eql([])
    expect(tokenize("one")).to.eql(["one"])
    expect(tokenize("    one    two three ")).to.eql(["one", "two", "three"])
    expect(tokenize("one, two")).to.eql(["one,", "two"])
  })
})
