/* globals __dirname global require describe it */

// https://www.wintellect.com/end-end-testing-electron-apps-spectron/

"use strict"

const Application = require("spectron").Application

import path = require("path")

import * as chai from "chai"

import chaiAsPromised = require("chai-as-promised")

import electronPath = require("electron")

const appPath = path.join(__dirname, "../../../dist/main.js")
// this is e.g.: /home/username/code/callers-app-electron/dist/main.js

// Details on testing tools:
// https://www.electronjs.org/spectron
// https://github.com/electron-userland/spectron#application-api
// https://webdriver.io/docs/api.html
// eslint-disable-next-line max-len
// http://www.matthiassommer.it/programming/web/integration-e2e-test-electron-mocha-spectron-chai/

let app = new Application({
  path: electronPath,
  args: [appPath],
  // extras
  env: {
    ELECTRON_ENABLE_LOGGING: true,
    ELECTRON_ENABLE_STACK_DUMPING: true,
    NODE_ENV: "development",
  },
  startTimeout: 20000,
  chromeDriverLogPath: "../chromedriverlog.txt",
})

global.before(() => {
  chaiAsPromised.transferPromiseness = app.transferPromiseness
  chai.should()
  chai.use(chaiAsPromised)
  return app.start()
})

global.after(() => app && app.isRunning() && app.stop())

describe("Test Basics", () => {
  // beforeEach(() => app.start());
  // afterEach(() => app.start());

  it("opens two windows (background and foreground)", () => {
    app.client
      .waitUntilWindowLoaded()
      .getWindowCount()
      .should.eventually.equal(2)
  })

  it("opens the foreground window in the right state", () => {
    const browserWindow = app.client.browserWindow
    browserWindow.isMinimized().should.eventually.be.false
    browserWindow.isVisible().should.eventually.be.true
    browserWindow.isFocused().should.eventually.be.true
    browserWindow
      .getBounds()
      .should.eventually.have.property("width")
      .and.be.above(0)
    browserWindow
      .getBounds()
      .should.eventually.have.property("height")
      .and.be.above(0)
  })

  it("foreground window has the right title", () =>
    app.client
      .waitUntilWindowLoaded()
      .getTitle()
      .should.eventually.equal("The Caller's Application"))

  it("can search by title", async () => {
    await app.client.waitUntilWindowLoaded()
    const titleInput = app.client.$("#title-input")
    const searchButton = app.client.$("#search-button")

    titleInput.addValue("Nice Combination")
    searchButton.click()

    await app.client.waitUntilTextExists(".dances-list-info", "Searching")
    await app.client.waitUntilTextExists(
      ".dances-list-info",
      "dances in the local collection",
    )

    return app.client.waitUntilTextExists(
      ".dances-table .dances-list-dance-title",
      "Nice Combination",
    )
  })

  it("can show 'out' dance details", () => {
    app.client.$(".dances-table .dances-list-dance-title").click()
    return app.client.waitUntilTextExists(
      ".right-column .dance-title",
      "Nice Combination",
    )
  })

  it("can add a dance to the local collection", async () => {
    app.client.$(".dances-table .storage-status-toggle").click()

    await app.client.waitUntil(() =>
      app.client
        .$(".dances-table .storage-status-cell .is-in-toggle")
        .isExisting(),
    )

    app.client.$(".show-all-dances-button").click()

    await app.client.waitUntilTextExists(
      ".dances-list-info",
      "dances in the local collection.",
    )

    await app.client
      .$(".dances-table")
      .$$("tr")
      .should.eventually.have.lengthOf(1)

    await app.client.waitUntilTextExists(
      ".dances-table .dances-list-dance-title",
      "Nice Combination",
    )

    await app.client
      .$(".dances-table .storage-status-cell .is-in-toggle")
      .isExisting()
  })

  it("can show 'in' dance details", () => {
    // Actually this dance's details are already loaded from previous test.
    app.client.$(".dances-table .dances-list-dance-title").click()
    return app.client.waitUntilTextExists(
      ".right-column .dance-title",
      "Nice Combination",
    )
  })

  it("can change a dance from 'in' to 'out' ('removed')", async () => {
    app.client.$(".dances-table .storage-status-toggle").click()

    await app.client.waitUntil(() =>
      app.client
        .$(".dances-table .storage-status-cell .is-removed-toggle")
        .isExisting(),
    )

    app.client.$(".show-all-dances-button").click()

    await app.client.waitUntilTextExists(
      ".dances-list-info",
      "dances in the local collection.",
    )

    await app.client
      .$(".dances-table")
      .$$("tr")
      .should.eventually.have.lengthOf(0)
  })
})
