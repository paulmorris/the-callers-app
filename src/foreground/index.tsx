/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import React from "react"
import ReactDOM from "react-dom"

import { App } from "./components/app"

// Don't delete indexedDB data.
// TS complains that Navigator doesn't have a storage property, so cast.
// ;(navigator as any).storage.persist()

// eslint-disable-next-line no-console
console.log(`We are using node ${process.versions.node},
  Chrome ${process.versions.chrome},
  and Electron ${process.versions.electron}`)

window.addEventListener("load", () => {
  ReactDOM.render(<App />, document.getElementById("app"))
})
