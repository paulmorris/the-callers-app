/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React, { FunctionComponent } from "react"

import { EditDanceView } from "./edit-dance"
import { SearchDancesView } from "./search-dances-view"
import { useStores, StoreProvider } from "../stores/stores-context"
import { ProgramsBrowseView } from "./programs-browse-view"
import { ProgramsEditView } from "./programs-edit-view"
import { ModalDanceDetail } from "./modal-dance-detail"
import { AdminView } from "./admin-view"
import { AdminAddDanceAttributeView } from "./admin-add-dance-attribute-view"

export const App: FunctionComponent = (props) => {
  return (
    <StoreProvider>
      <ViewSwitcher />
      <ModalHandler />
    </StoreProvider>
  )
}

const ViewSwitcher: FunctionComponent = observer(function ViewSwitcher(props) {
  const { uiStore } = useStores()

  switch (uiStore.view) {
    case "editDance":
      return <EditDanceView />
    case "programsBrowse":
      return <ProgramsBrowseView />
    case "programsEdit":
      return <ProgramsEditView />
    case "searchDances":
      return <SearchDancesView />
    case "admin":
      return <AdminView />
    case "adminAddDanceAttribute":
      return <AdminAddDanceAttributeView />
    default:
      return <SearchDancesView />
  }
})

const ModalHandler: FunctionComponent = observer(function ModalHandler() {
  const { uiStore } = useStores()
  const dance = uiStore.modalDance
  return dance ? (
    <ModalDanceDetail
      dance={dance}
      closeModal={() => uiStore.setModalDance(undefined)}
    />
  ) : (
    <></>
  )
})
