/*
Copyright 2019, 2020, 2021, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React, { ChangeEvent } from "react"

import {
  formationOptions,
  HtmlInputElements,
  progressionOptions,
  SearchInput,
} from "../../shared/types"
import { DancesSearchStore } from "../stores/dances-search-store"

export const SearchFormHorizontal = observer(
  function SearchFormHorizontal(props: { store: DancesSearchStore }) {
    const { store } = props

    const onUpdateSearchForm = (
      event: ChangeEvent<HtmlInputElements>,
      searchFormKey: keyof SearchInput,
    ): void => {
      store.updateSearchForm(searchFormKey, event.target.value)
    }

    const onDoSearch = (event: React.MouseEvent): void => {
      event.preventDefault()
      store.doSearch()
    }

    return (
      <form name="searchForm" key="searchFormKey">
        <div className="columns is-marginless">
          <div className="column">
            <div className="field is-horizontal">
              <div className="field-label is-small">
                <label className="label">Title</label>
              </div>
              <div className="field-body">
                <div className="field">
                  <div className="control is-expanded">
                    <input
                      id="title-input"
                      className="input is-small"
                      value={store.searchInput.title}
                      onChange={(event) => onUpdateSearchForm(event, "title")}
                    />
                  </div>
                </div>
              </div>
            </div>

            <div className="field is-horizontal">
              <div className="field-label is-small">
                <label className="label">Author</label>
              </div>
              <div className="field-body">
                <div className="field">
                  <div className="control is-expanded">
                    <input
                      className="input is-small"
                      value={store.searchInput.authors}
                      onChange={(event) => onUpdateSearchForm(event, "authors")}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="column">
            <div className="field is-horizontal">
              <div className={"field-label is-small"}>
                <label className="label">Formation</label>
              </div>
              <div className={"field-body"}>
                <div className={"field is-narrow"}>
                  <div className={"control is-expanded"}>
                    <div className="select is-small is-fullwidth">
                      <select
                        value={store.searchInput.formationBase}
                        onChange={(event) =>
                          onUpdateSearchForm(event, "formationBase")
                        }
                      >
                        {formationOptions.map((val, index) => (
                          <option key={index}>{val}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="field is-horizontal">
              <div className={"field-label is-small"}>
                <label className="label">Progression</label>
              </div>
              <div className={"field-body"}>
                <div className={"field is-narrow"}>
                  <div className={"control is-expanded"}>
                    <div className="select is-small is-fullwidth">
                      <select
                        value={store.searchInput.progression}
                        onChange={(event) =>
                          onUpdateSearchForm(event, "progression")
                        }
                      >
                        {progressionOptions.map((val, index) => (
                          <option key={index}>{val}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="column">
            <div className="field">
              <div className="control">
                <div className="select is-small">
                  <select
                    value={store.searchInput.pos_mode}
                    onChange={(event) => onUpdateSearchForm(event, "pos_mode")}
                  >
                    <option value="any_any">
                      Figures match: any lines, any order
                    </option>
                    <option value="all_any">
                      Figures match: all lines, any order
                    </option>
                    <option value="all_given">
                      Figures match: all lines, given order
                    </option>
                  </select>
                </div>
              </div>
            </div>

            <div className="field">
              <div className="control">
                <textarea
                  className="textarea is-small"
                  rows={2}
                  value={store.searchInput.pos_lines}
                  onChange={(event) => onUpdateSearchForm(event, "pos_lines")}
                />
              </div>
            </div>
          </div>

          <div className="column">
            <div className="field">
              <div className="control">
                <div className="select is-small">
                  <select
                    value={store.searchInput.neg_mode}
                    onChange={(event) => onUpdateSearchForm(event, "neg_mode")}
                  >
                    <option value="any_any">
                      Figures do not match: any lines, any order
                    </option>
                    <option value="all_any">
                      Figures do not match: all lines, any order
                    </option>
                    <option value="all_given">
                      Figures do not match: all lines, given order
                    </option>
                  </select>
                </div>
              </div>
            </div>

            <div className="field">
              <div className="control">
                <textarea
                  className="textarea is-small"
                  rows={2}
                  value={store.searchInput.neg_lines}
                  onChange={(event) => onUpdateSearchForm(event, "neg_lines")}
                />
              </div>
            </div>
          </div>

          {/* TODO: what will be the fate of tags? */}
          {/* {store.searchInput.search_type === "local" && (
            <div className="column">
              <div className="field">
                <label className="label is-small">Tags:</label>
                <div className="control">
                  <input
                    className="input is-small"
                    value={store.searchInput.tags}
                    onChange={(event) => onUpdateSearchForm(event, "tags")}
                  />
                </div>
              </div>
            </div>
          )} */}

          <div className="column">
            <div className="field">
              <div className="select is-small">
                <select
                  value={store.searchInput.search_type}
                  onChange={(event) => onUpdateSearchForm(event, "search_type")}
                >
                  <option value="local_tcb">
                    Search: Local collection + Caller's Box
                  </option>
                  <option value="local">Search: Local collection (only)</option>
                  <option value="tcb">Search: The Caller's Box (only)</option>
                </select>
              </div>
            </div>

            <div className="field">
              <div className="control">
                <button
                  id="search-button"
                  className="button is-small"
                  onClick={onDoSearch}
                >
                  Search
                </button>
              </div>
            </div>
          </div>
        </div>
      </form>
    )
  },
)
