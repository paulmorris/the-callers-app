/*
Copyright 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import React, { FunctionComponent } from "react"
import { observer } from "mobx-react-lite"

import { Program } from "../../shared/types"
import { useStores } from "../stores/stores-context"

const missingValue = "-"

interface ProgramsListProps {
  programs: Program[]
}

export const ProgramsList: FunctionComponent<ProgramsListProps> = observer(
  function ProgramsList(props) {
    const { programs } = props
    const { programsBrowseStore, uiStore } = useStores()

    return (
      <table className="table default-table programs-list-table is-fullwidth is-narrow">
        <tbody>
          <tr>
            <th>Date</th>
            <th>Location</th>
            <th>Music By</th>
          </tr>
          {programs.map((program: Program, index: number) => {
            return (
              <tr
                key={index}
                className="programs-list-row"
                onClick={() => {
                  programsBrowseStore.viewProgram(program.programId)
                  uiStore.setModalDance(undefined)
                }}
                // TODO -
                // onDoubleClick={() => programsBrowseStore.editProgram(program)}
                draggable
              >
                <td>{program.date || missingValue}</td>
                <td>{program.location || missingValue}</td>
                <td>{program.music || missingValue}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    )
  },
)
