/*
Copyright 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React, { FunctionComponent } from "react"

import { useStores } from "../stores/stores-context"
import { ProgramsDancesList } from "./programs-dances-list"
import { ProgramsEditForm } from "./programs-edit-form"
import { ProgramsList } from "./programs-list"
import { ToolbarMain } from "./toolbar-main"

export const ProgramsBrowseView: FunctionComponent = observer(
  function ProgramsBrowseView(props) {
    const { programsBrowseStore } = useStores()
    const programToView = programsBrowseStore.programToView

    const onSearchInputChange = (
      event: React.ChangeEvent<HTMLInputElement>,
    ) => {
      programsBrowseStore.updateSearchInput(event.target.value)
    }

    return (
      <>
        <ToolbarMain activeTab="programs"></ToolbarMain>
        <div className="columns is-marginless">
          <div className="column left-column column-under-toolbar">
            <div>
              <input
                id="programsSearchInput"
                className="input"
                placeholder="Search Programs"
                value={programsBrowseStore.programsSearchInput}
                onChange={onSearchInputChange}
              />
            </div>
            <div>
              <ProgramsList programs={programsBrowseStore.programsList} />
            </div>
          </div>

          <div className="column column-under-toolbar">
            {programToView && (
              <>
                <div className="program-detail-button-container buttons">
                  <button
                    className="button is-small"
                    onClick={() =>
                      programsBrowseStore.editProgram(programToView)
                    }
                  >
                    Edit
                  </button>
                  <button
                    className="button is-small"
                    onClick={() =>
                      programsBrowseStore.deleteProgram(programToView.programId)
                    }
                  >
                    Delete
                  </button>
                </div>
                <ProgramsEditForm program={programToView} isStatic={true} />
                {/* TS placation */}
                {programToView.dances && (
                  <ProgramsDancesList dances={programToView.dances} />
                )}
              </>
            )}
          </div>
        </div>
      </>
    )
  },
)
