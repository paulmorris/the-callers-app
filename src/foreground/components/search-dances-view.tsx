/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React, { FunctionComponent } from "react"

import { DanceDetailView } from "./dance-detail-view"
import { DancesTable } from "./dances-table"
import { SearchForm } from "./search-form"
import { useStores } from "../stores/stores-context"
import { ToolbarMain } from "./toolbar-main"
import { Dance } from "../../shared/types"

export const SearchDancesView: FunctionComponent = observer(
  function SearchDancesView(props) {
    const { dancesSearchStore: store, dancesEditStore } = useStores()
    return (
      <>
        <ToolbarMain activeTab="dances"></ToolbarMain>
        <div className="columns is-marginless">
          <div className="column left-column is-narrow column-under-toolbar">
            <SearchForm store={store} />
          </div>
          <div className="column middle-column column-under-toolbar">
            {store.searchResultsMessages?.header && (
              <div className="dances-list-info">
                {store.searchResultsMessages.header}
              </div>
            )}
            {store.dancesList && (
              <DancesTable
                dances={store.dancesList}
                viewDance={store.viewDance.bind(store)}
                editDance={(dance) =>
                  dancesEditStore.startEditingDance(dance as Dance)
                }
              />
            )}
            {store.searchResultsMessages?.criteria && (
              <div className="dances-list-info">
                {store.searchResultsMessages.criteria}
              </div>
            )}
            {store.searchResultsMessages?.footer && (
              <div className="dances-list-info search-results-footer">
                {store.searchResultsMessages.footer}
              </div>
            )}
          </div>
          <div className="dance column right-column column-under-toolbar">
            {store.danceToView && (
              <DanceDetailView danceToView={store.danceToView} />
            )}
          </div>
        </div>
        {/* The following buttons are only here for the integration tests,
          because using the main electron menu in integration tests is not
          supported. Hopefully a temporary hack. */}
        <button
          className="show-all-dances-button invisible-clickable"
          onClick={() => store.doLocalSearch()}
        />
      </>
    )
  },
)
