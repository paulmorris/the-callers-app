/*
Copyright 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React, { FunctionComponent } from "react"

interface InputBasicHorizontalProps {
  label: string
  labelClass: string
  isStatic?: boolean
}

export const InputBasicHorizontal: FunctionComponent<InputBasicHorizontalProps> = observer(
  function InputBasicHorizontal(props) {
    const { label, labelClass, isStatic } = props
    const fieldClass = isStatic ? " static-field" : ""
    return (
      <div className={"field is-horizontal" + fieldClass}>
        <div className={"field-label " + (labelClass || "")}>
          <label className="label">{label}</label>
        </div>
        <div className="field-body">
          <div className="field">
            <div className="control">{props.children}</div>
          </div>
        </div>
      </div>
    )
  },
)

export const InputBasic = observer(function InputNoLabel(props: {
  children: React.ReactNode
}) {
  return (
    <div className={"field"}>
      <div className="control">{props.children}</div>
    </div>
  )
})
