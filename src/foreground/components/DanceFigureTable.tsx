/*
Copyright 2019, 2020, 2021 Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React from "react"
import { DancePart } from "../../shared/types"

interface Row {
  section: string
  lines: Line[]
}

interface Line {
  text: string
  subpart?: boolean
}

/**
 * Renders a table that displays dance figures.
 */
export const DanceFigureTable = observer(function DanceFigureTable(props: {
  danceParts: DancePart[]
  tableClasses?: string
}) {
  const rows = rowsFromDanceParts(props.danceParts)
  const tableClasses = "figures-table " + (props.tableClasses || "")

  return (
    <table className={tableClasses}>
      <tbody>
        {rows.map((row, index) => (
          <tr key={index}>
            <th className={"figures-table-th"}>{row.section}</th>
            <td className={"figures-table-td"}>
              {row.lines.map((line, index) => (
                <p
                  style={line.subpart ? { marginLeft: "2.5em" } : undefined}
                  key={index}
                >
                  {line.text}
                </p>
              ))}
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  )
})

/**
 * Converts a flat array of dance figure data into nested row data organized
 * by section.
 *
 * @param danceParts - Flat array of dance figure data.
 * @return - Dance figure data organized into nested table row shape.
 */
function rowsFromDanceParts(danceParts: DancePart[]): Row[] {
  return danceParts.reduce((rows: Row[], part: DancePart) => {
    // Set up a new table row if needed.
    if (rows.length === 0 || part.section !== rows[rows.length - 1].section) {
      rows.push({
        section: part.section,
        lines: [],
      })
    }

    // Make line data from a dance part and add it to a table row.
    const line: Line = { text: part.text }
    if (part.subpart) {
      line.subpart = part.subpart
    }
    rows[rows.length - 1].lines.push(line)

    return rows
  }, [])
}
