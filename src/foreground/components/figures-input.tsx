/*
Copyright 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/
import { observer } from "mobx-react-lite"
import React, { FunctionComponent, useState, useRef, useEffect } from "react"
import getCaretCoordinates from "textarea-caret"

import { useStores } from "../stores/stores-context"

const KEY_CODES = {
  ESC: 27,
  UP: 38,
  DOWN: 40,
  LEFT: 37,
  RIGHT: 39,
  ENTER: 13,
  TAB: 9,
}

interface FiguresInputProps {
  value: string
  setValue: (value: string) => void
  rows: number
  suggestions: [string, number][]
  searchForSuggestions: (line: string) => void
}

export const FiguresInput: FunctionComponent<FiguresInputProps> = observer(
  function FiguresInput(props) {
    const { value, setValue, rows, suggestions, searchForSuggestions } = props

    // We have to manage the cursor position (selectionStart/selectionEnd)
    // for figure insertion to work well.
    const textAreaRef = useRef<HTMLTextAreaElement>(null)
    const [textAreaSelectionStart, setTextAreaSelectionStart] = useState(0)

    useEffect(() => {
      const textArea = textAreaRef.current
      if (textArea) {
        textArea.selectionStart = textArea.selectionEnd = textAreaSelectionStart
      }
    }, [textAreaSelectionStart])

    // State for the suggestions box.
    const [suggestionsShown, setSuggestionsShown] = useState(false)
    const [selectedSuggestion, setSelectedSuggestion] = useState(0)
    const [suggestionsTop, setSuggestionsTop] = useState(0)

    const onChangeTextarea = (
      event: React.ChangeEvent<HTMLTextAreaElement>,
    ) => {
      const textArea = event.target
      setValue(textArea.value)
      setTextAreaSelectionStart(textArea.selectionStart)

      const line = getCurrentLine(textArea.value, textArea.selectionStart)
      if (line.trim() === "") {
        setSuggestionsShown(false)
      } else {
        // Do a search and show suggestions.
        searchForSuggestions(line)

        const coords = getCaretCoordinates(textArea, textArea.selectionEnd)
        setSuggestionsTop(coords.top)

        setSelectedSuggestion(0)
        setSuggestionsShown(true)
      }
    }

    const onKeyDownTextarea = (
      event: React.KeyboardEvent<HTMLTextAreaElement>,
    ) => {
      if (suggestionsShown && suggestions.length > 0) {
        if (
          event.keyCode === KEY_CODES.ENTER ||
          event.keyCode === KEY_CODES.TAB
        ) {
          event.preventDefault()
          useSuggestion(suggestions[selectedSuggestion][0])
        } else if (event.keyCode === KEY_CODES.DOWN) {
          event.preventDefault()
          if (selectedSuggestion < suggestions.length - 1) {
            setSelectedSuggestion(selectedSuggestion + 1)
          }
        } else if (event.keyCode === KEY_CODES.UP) {
          if (selectedSuggestion === 0) {
            setSuggestionsShown(false)
          } else {
            event.preventDefault()
            setSelectedSuggestion(selectedSuggestion - 1)
          }
        } else if (event.keyCode === KEY_CODES.ESC) {
          setSuggestionsShown(false)
        }
      }
    }

    const onBlurTextarea = (event: React.FocusEvent) => {
      setSuggestionsShown(false)
    }

    const useSuggestion = (figureText: string): void => {
      const textArea = textAreaRef.current
      if (textArea) {
        const { newSelectionStart, newFigures } = calculateFigureInsertion(
          figureText,
          textArea.value,
          textArea.selectionStart,
          textArea.selectionEnd,
        )
        setTextAreaSelectionStart(newSelectionStart)
        setValue(newFigures)
        setSuggestionsShown(false)
        // After a click on a suggestion, focus the textarea.
        textArea.focus()
      }
    }

    return (
      <div>
        {suggestionsShown && suggestions.length > 0 && (
          <div
            className="menu figure-suggestions-wrapper"
            style={{ top: suggestionsTop + "px" }}
          >
            <ul className="menu-list figure-suggestions-list">
              {suggestions.map(([figureText], index) => {
                const selectedClass =
                  index === selectedSuggestion
                    ? " figure-suggestion-selected"
                    : ""

                return (
                  <li key={index}>
                    <a
                      className={"figure-suggestion" + selectedClass}
                      // The onMouseDown event.preventDefault prevents a blur
                      // event on the textarea. Needed because otherwise the
                      // onClick below never happens because the onBlur removes
                      // the clicked element.
                      onMouseDown={(event) => event.preventDefault()}
                      onClick={(event) => useSuggestion(figureText)}
                    >
                      {figureText}
                    </a>
                  </li>
                )
              })}
            </ul>
          </div>
        )}
        <textarea
          className="textarea is-small"
          onChange={onChangeTextarea}
          onKeyDown={onKeyDownTextarea}
          onBlur={onBlurTextarea}
          ref={textAreaRef}
          rows={rows}
          value={value}
        ></textarea>
      </div>
    )
  },
)

const getCurrentLine = (text: string, selectionStart: number): string => {
  const startOfLine = findStartOfLine(text, selectionStart)
  const endOfLine = findEndOfLine(text, selectionStart)

  const currentLineText = text.slice(startOfLine, endOfLine + 1)
  return currentLineText
}

const findStartOfLine = (text: string, selectionStart: number): number => {
  // selectionStart is the index of the first character after the cursor,
  // so if it points to a "\n" then the cursor is at the end of a line.
  let start = selectionStart
  while (start > 0 && text[start - 1] !== "\n") {
    start -= 1
  }
  return start
}

const findEndOfLine = (text: string, selectionStart: number): number => {
  let end = selectionStart - 1
  const lastIndex = text.length - 1

  while (end < lastIndex && text[end + 1] !== "\n") {
    end += 1
  }
  return end
}

/**
 * Calculates how to insert a figure string into a figures textarea.
 * Recalculates and sets the new selectionStart/selectionEnd, which we have to
 * track somewhat manually.
 *
 * @param figure - The figure, e.g. "(8) Partner Balance".
 * @param textAreaValue - The full text of the textarea.
 * @param selectionStart - The selectionStart value of the textarea.
 * @param selectionEnd - The selectionEnd value of the textarea.
 * @return The new value for the textarea and its new selectStart.
 */
const calculateFigureInsertion = (
  figure: string,
  textAreaValue: string,
  selectionStart: number,
  selectionEnd: number,
): { newFigures: string; newSelectionStart: number } => {
  const startOfLine = findStartOfLine(textAreaValue, selectionStart)
  const endOfLine = findEndOfLine(textAreaValue, selectionEnd)

  const before = textAreaValue.substring(0, startOfLine)
  const after = textAreaValue.substring(endOfLine + 1)

  const newlineBefore =
    before.length > 0 && before[before.length - 1] !== "\n" ? "\n" : ""

  const newlineAfter = after[0] !== "\n" ? "\n" : ""

  const newSelectionStart =
    startOfLine + newlineBefore.length + figure.length + newlineAfter.length + 1

  const newFigures = before + newlineBefore + figure + newlineAfter + after

  return { newFigures, newSelectionStart }
}
