/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React, { FunctionComponent } from "react"
import ReactMarkdown from "react-markdown"

import { StorageStatusToggle } from "./storage-status-toggle"
import { Dance, SearchResultsDance } from "../../shared/types"
import { danceUrl, newline } from "../../shared/utils"
import { useStores } from "../stores/stores-context"
import { ProgramsList } from "./programs-list"
import { DanceFigureTable } from "./DanceFigureTable"

interface DanceDetailViewProps {
  danceToView: Dance | SearchResultsDance
}

export const DanceDetailView: FunctionComponent<DanceDetailViewProps> = observer(
  function DanceDetailView(props) {
    const { dancesEditStore, danceAttributesStore } = useStores()
    const dance = props.danceToView

    const formationEtc =
      dance.formationBase +
      ("formationDetail" in dance && dance.formationDetail
        ? " - " + dance.formationDetail
        : "") +
      ("mixer" in dance && dance.mixer === "yes" ? " - Mixer" : "") +
      ("progression" in dance && dance.progression
        ? ` - ${dance.progression} Progression`
        : "")

    // Show dance.figures by default, else show dance.tcbFigures.
    const figuresToShow =
      "figures" in dance && dance.figures.length
        ? dance.figures
        : "tcbFigures" in dance && dance.tcbFigures.length
        ? dance.tcbFigures
        : undefined

    // onClick must be a ternary with undefined (not 'x && y').
    // TS placation.
    const editButtonOnClick =
      dance.storageStatus === "in"
        ? () => dancesEditStore.startEditingDance(dance as Dance)
        : undefined

    const editButtonTitle =
      dance.storageStatus === "in"
        ? "Edit Dance"
        : "Add to the local collection first and then edit."

    return (
      <>
        <div className="field is-grouped dance-view-buttons-container">
          <StorageStatusToggle dance={dance} button={true} />
          <div className="control">
            {/* Needs to be a <button> so the 'disabled' prop is allowed. */}
            <button
              className="button is-small edit-dance-button"
              onClick={editButtonOnClick}
              title={editButtonTitle}
              disabled={dance.storageStatus !== "in"}
            >
              Edit
            </button>
          </div>
          {dance.tcbId && (
            <div className="control">
              <a
                className="button is-small"
                href={danceUrl(dance.tcbId)}
                target="_blank"
                title="Open Dance in the Caller's Box"
              >
                Open in TCB
              </a>
            </div>
          )}
        </div>
        <div className="our-box">
          <h2 className="is-size-4 has-text-weight-medium dance-title">
            {dance.title}
          </h2>
          <p>by {dance.authors.join(", ")}</p>
        </div>

        <p>{formationEtc}</p>

        {"direction" in dance && dance.direction && (
          <p>Direction: {dance.direction}</p>
        )}
        {"phraseStructure" in dance && dance.phraseStructure && (
          <p>Phrase Structure: {dance.phraseStructure}</p>
        )}
        {"music" in dance && dance.music.length > 0 && (
          <p>Music: {dance.music.join(", ")}</p>
        )}
        {dance.status !== "none" && <p>Status: {dance.status}</p>}
        {"basedOn" in dance && dance.basedOn.length > 0 && (
          <p>Based on: {dance.basedOn.join(", ")}</p>
        )}

        {figuresToShow && (
          <div className="our-box">
            <DanceFigureTable
              danceParts={figuresToShow}
              tableClasses="table is-narrow is-fullwidth has-background-white-bis"
            />
          </div>
        )}

        {!figuresToShow &&
          "permission" in dance &&
          (dance.permission === "search" ||
            dance.permission === "no_figures") && (
            <div className="our-box">
              <p style={{ paddingLeft: 0 }}>
                The permission for this dance in the Caller's Box is{" "}
                {`'${dance.permission}'`}, so its figures{" "}
                {dance.permission === "search"
                  ? "can be searched but not displayed."
                  : "can neither be searched nor displayed."}
                {dance.storageStatus === "in"
                  ? "  Edit the dance to add figures to it."
                  : ""}
              </p>
            </div>
          )}

        {"callingNotes" in dance && dance.callingNotes.length > 0 && (
          <div className="our-box">
            <p className="dance-detail-view-subheading">Calling Notes</p>
            <div className="calling-notes-text">
              <ReactMarkdown
                source={dance.callingNotes.join(newline)}
                linkTarget="_blank"
              />
            </div>
          </div>
        )}

        {"tags" in dance && dance.tags.length > 0 && (
          <div className="our-box">
            <p className="dance-detail-view-subheading">Tags</p>
            <p>{dance.tags.join(" ")}</p>
          </div>
        )}

        {"otherTitles" in dance && dance.otherTitles.length > 0 && (
          <div className="our-box">
            <p className="dance-detail-view-subheading">Other Titles</p>
            <ReactMarkdown
              source={dance.otherTitles.join(newline)}
              linkTarget="_blank"
            />
          </div>
        )}

        {"appearances" in dance && dance.appearances.length > 0 && (
          <div className="our-box">
            <p className="dance-detail-view-subheading">Appearances</p>
            <ReactMarkdown
              source={dance.appearances.join(newline)}
              linkTarget="_blank"
            />
          </div>
        )}

        {"videos" in dance && dance.videos.length > 0 && (
          <div className="our-box videos-section">
            <p className="dance-detail-view-subheading">Videos</p>
            <ReactMarkdown
              source={dance.videos.join(newline)}
              linkTarget="_blank"
            />
          </div>
        )}

        {"variantVideos" in dance && dance.variantVideos.length > 0 && (
          <div className="our-box variant-videos-section">
            <p className="dance-detail-view-subheading">Variant Videos</p>
            <ReactMarkdown
              source={dance.variantVideos.join(newline)}
              linkTarget="_blank"
            />
          </div>
        )}

        {"programs" in dance && dance.programs.length ? (
          <div className="our-box">
            <p className="dance-detail-view-subheading">Programs</p>
            <ProgramsList programs={dance.programs} />
          </div>
        ) : null}

        <div className="our-box">
          <p className="dance-detail-view-subheading">Attributes</p>
          <table className="table default-table">
            <tbody>
              <tr>
                <th></th>
                <th></th>
              </tr>
              {danceAttributesStore.attributes.map((attribute) => {
                // TS Placation
                const rawValue =
                  "attributes" in dance && dance.attributes?.[attribute.id]

                const valueText = rawValue || ""

                // TODO multi-type-attributes
                // let valueText
                // const inputType = attribute.inputType

                // if (inputType === "field" || inputType === "field-with-menu") {
                //   valueText = rawValue || ""
                // } else if (inputType === "menu" || inputType === "checkbox") {
                //   const valueId = rawValue

                //   const value = attribute.values.find(
                //     valueId ? (v) => v.id === valueId : (v) => v.isDefault,
                //   )

                //   valueText = value ? getValueText(value) : ""
                // }

                return (
                  <tr key={attribute.id}>
                    <td>{attribute?.name}</td>
                    <td>{valueText}</td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>

        {dance.storageStatus === "out-partial" && (
          <div className="our-box">
            <p className="fetching-dance-message">
              Fetching from the Caller's Box...
            </p>
          </div>
        )}
      </>
    )
  },
)
