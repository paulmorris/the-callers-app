/*
Copyright 2020, 2021 Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React, { useEffect, useRef, useState } from "react"
import {
  Dance,
  DancePart,
  getFormationAbbreviation,
  isProgramDance,
  isProgramSpacer,
  Program,
  ProgramDance,
  ProgramItem,
  ProgramSpacer,
  SearchResultsDance,
} from "../../shared/types"
import { useStores } from "../stores/stores-context"
import { useDragAndDrop } from "../hooks/use-drag-and-drop"
import { DanceFigureTable } from "./DanceFigureTable"

/**
 * Dances grid that shows all dances in a program and search results dances
 * below that. Supports drag and drop within program dances.
 */
export const ProgramsDancesGrid = observer(function ProgramsDancesGrid(props: {
  program: Program
  moveDance: (newIndex: number, oldIndex: number) => Promise<void>
  showProgramDanceFigures: (programDanceId: number) => void
  showSearchResultsDanceFigures: (id: number) => void
  programDancesShowingFigures: Set<number>
  searchResultsDancesShowingFigures: Set<number>
  removeItemFromProgram: (
    programDanceId: number,
    danceId: number | undefined,
  ) => void
  addDanceToProgram: (dance: SearchResultsDance) => void
  // The search results dances may or may not have attributes, so type is
  // Dance or SearchResultsDance.
  searchResultsDances?: (Dance | SearchResultsDance)[]
  separatorText?: string
}) {
  const {
    program,
    moveDance,
    showProgramDanceFigures,
    showSearchResultsDanceFigures,
    programDancesShowingFigures,
    searchResultsDancesShowingFigures,
    removeItemFromProgram,
    addDanceToProgram,
    searchResultsDances,
    separatorText,
  } = props
  const { dancesStore, danceAttributesStore, uiStore } = useStores()
  const { attributes } = danceAttributesStore
  const gridAttributes = attributes.filter((attr) => attr.showInGrid)

  const programDanceIds = new Set(
    program.dances
      ? program.dances
          .filter(isProgramDance)
          .map((programDance) => programDance.dance.danceId)
      : [],
  )

  // So the header cell height matches the text size while the text is also
  // rotated and positioned at the bottom... We have to:
  // - programmatically determine the height of the th row
  // - set the th cells to that height
  // - use `writing-direction: vertical-lr;` on the child div to rotate text
  // - use the relative/absolute/bottom:0; dance to position text at bottom
  // At least until chrome supports `writing-direction: sideways-lr`.

  const [headerSize, setHeaderSize] = useState<number | null>(null)
  const ref = useRef(null)

  useEffect(() => {
    const element = (ref.current as unknown) as HTMLElement
    setHeaderSize(Math.ceil(element.scrollHeight))
  }, [ref])

  const align = headerSize !== null
  const thClass = align ? "dances-grid-th-aligned" : undefined
  const thStyle = align ? { height: headerSize + "px" } : undefined
  const thContentClass =
    "dances-grid-th-content" + (align ? " dances-grid-th-content-aligned" : "")

  const textButtonClasses = "button is-small is-ghost dances-grid-text-button"

  const makeAddCell = (dance: Dance | SearchResultsDance): JSX.Element => {
    return (
      <td
        className="cursor-pointer table-cell-button"
        onClick={(event) => {
          event.stopPropagation()
          addDanceToProgram(dance)
        }}
      >
        <button className={textButtonClasses}>Add</button>
      </td>
    )
  }

  const makeDeleteCell = (item: ProgramDance | ProgramSpacer): JSX.Element => {
    return (
      <td
        className="cursor-pointer table-cell-button"
        onClick={(event) => {
          event.stopPropagation()
          removeItemFromProgram(
            item.programDanceId,
            "dance" in item ? item.dance.danceId : undefined,
          )
        }}
      >
        <button className={textButtonClasses}>X</button>
      </td>
    )
  }

  const makeViewCell = (dance: Dance | SearchResultsDance): JSX.Element => {
    return (
      <td
        className="cursor-pointer table-cell-button"
        onClick={(event) => {
          event.stopPropagation()
          uiStore.setModalDance(dance)
        }}
      >
        <button className={textButtonClasses}>View</button>
      </td>
    )
  }

  const makeExpandCellForProgram = (item: ProgramItem): JSX.Element => {
    return (
      <td
        className="cursor-pointer table-cell-button"
        onClick={(event) => {
          event.stopPropagation()
          showProgramDanceFigures(item.programDanceId)
        }}
      >
        <button className={textButtonClasses}>
          {programDancesShowingFigures.has(item.programDanceId) ? "v" : ">"}
        </button>
      </td>
    )
  }

  const makeExpandCellForSearchResult = (
    dance: Dance | SearchResultsDance,
  ): JSX.Element => {
    return (
      <td
        className="cursor-pointer table-cell-button"
        onClick={(event) => {
          event.stopPropagation()
          const showFigs = () => showSearchResultsDanceFigures(dance.danceId)

          if (!getFiguresToShow(dance) && dance.hasFigures) {
            const { danceId, tcbId } = dance
            // TS Placation, out-partial dances should always have a tcbId.
            if (dance.storageStatus === "out-partial" && tcbId) {
              dancesStore
                .fetchTcbDanceByTcbId({ danceId, tcbId })
                .then(showFigs)
            } else {
              dancesStore.getDanceById({ danceId }).then(showFigs)
            }
          } else {
            showFigs()
          }
        }}
      >
        <button className={textButtonClasses}>
          {searchResultsDancesShowingFigures.has(dance.danceId) ? "v" : ">"}
        </button>
      </td>
    )
  }

  /**
   * Return dance.figures by default, else return dance.tcbFigures.
   */
  const getFiguresToShow = (
    dance: Dance | SearchResultsDance,
  ): DancePart[] | undefined => {
    return "figures" in dance && dance.figures.length
      ? dance.figures
      : "tcbFigures" in dance && dance.tcbFigures.length
      ? dance.tcbFigures
      : undefined
  }

  const { items: programItems, dragState, getDragAttributes } = useDragAndDrop(
    program.dances || [],
    moveDance,
  )

  return (
    <div className="table-container">
      <table className="table default-table is-bordered is-narrow dances-grid-table">
        <tbody>
          <tr ref={ref}>
            <th colSpan={4}></th>

            <th className={thClass} style={thStyle}>
              <div className={thContentClass}>Formation</div>
            </th>

            {gridAttributes.map((attribute) => (
              <th className={thClass} style={thStyle} key={attribute.id}>
                <div className={thContentClass}>{attribute.name}</div>
              </th>
            ))}
          </tr>

          {programItems.map((item, index) => {
            const trClasses =
              "dances-grid-row cursor-grab" +
              (dragState.draggingOver && dragState.to === index
                ? " programs-dance-list-item-dragging"
                : "")

            if (isProgramSpacer(item)) {
              return (
                <tr
                  {...getDragAttributes(index)}
                  className={trClasses}
                  key={item.programDanceId}
                >
                  {makeDeleteCell(item)}
                  <td>{/* view button */}</td>
                  <td>{/* expand button */}</td>
                  <td>{/* title */}</td>
                  <td>{/* formation */}</td>
                  {gridAttributes.map((a) => (
                    <td key={a.id}></td>
                  ))}
                </tr>
              )
            }

            const dance = item.dance
            const figuresToShow = getFiguresToShow(dance)

            return (
              <tr
                {...getDragAttributes(index)}
                className={trClasses}
                key={item.programDanceId}
              >
                {makeDeleteCell(item)}
                {makeViewCell(dance)}
                {figuresToShow ? makeExpandCellForProgram(item) : <td></td>}
                <td title={`By ${dance.authors.join(", ")}`}>
                  {dance.title}
                  {figuresToShow &&
                    programDancesShowingFigures.has(item.programDanceId) && (
                      <DanceFigureTable danceParts={figuresToShow} />
                    )}
                </td>

                <td title={dance.formationBase}>
                  {getFormationAbbreviation(dance.formationBase)}
                </td>

                {gridAttributes.map((attribute) => {
                  return (
                    <td key={attribute.id}>{dance.attributes[attribute.id]}</td>
                  )
                })}
              </tr>
            )
          })}

          {searchResultsDances && searchResultsDances.length && (
            <>
              {/* Separator row. */}
              <tr className={"dances-grid-row"}>
                <td
                  colSpan={5 + gridAttributes.length}
                  className={"dances-grid-separator-row"}
                >
                  {separatorText}
                </td>
              </tr>

              {/* Repeated header row */}
              {/* <tr className={"dances-grid-row"}>
                <td colSpan={4}></td>
                <td className={thClass} style={thStyle}>
                  <div className={thContentClass}>Formation</div>
                </td>
                {gridAttributes.map((attribute) => (
                  <td className={thClass} style={thStyle} key={attribute.id}>
                    <div className={thContentClass}>{attribute.name}</div>
                  </td>
                ))}
              </tr> */}

              {/* Search results rows. */}
              {searchResultsDances.map((dance) => {
                const figuresToShow = getFiguresToShow(dance)
                const danceIsInProgram = programDanceIds.has(dance.danceId)

                const trClasses =
                  "dances-grid-row" +
                  (danceIsInProgram ? " dances-grid-row-in-program" : "")

                return (
                  <tr className={trClasses} key={dance.danceId}>
                    {makeAddCell(dance)}
                    {makeViewCell(dance)}

                    {figuresToShow || dance.hasFigures ? (
                      makeExpandCellForSearchResult(dance)
                    ) : (
                      <td></td>
                    )}

                    <td title={`By ${dance.authors.join(", ")}`}>
                      {dance.title}
                      {figuresToShow &&
                        searchResultsDancesShowingFigures.has(
                          dance.danceId,
                        ) && <DanceFigureTable danceParts={figuresToShow} />}
                    </td>
                    <td title={dance.formationBase}>
                      {getFormationAbbreviation(dance.formationBase)}
                    </td>

                    {gridAttributes.map((attribute) => {
                      return (
                        <td key={attribute.id}>
                          {"attributes" in dance
                            ? dance.attributes[attribute.id]
                            : ""}
                        </td>
                      )
                    })}
                  </tr>
                )
              })}
            </>
          )}
        </tbody>
      </table>
    </div>
  )
})
