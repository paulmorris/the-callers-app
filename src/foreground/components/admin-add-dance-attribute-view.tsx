/*
Copyright 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React, { FC, useState } from "react"
import {
  DanceAttributesStore,
  makeDefaultAttribute,
} from "../stores/dance-attributes-store"
import { useStores } from "../stores/stores-context"
import { AdminDanceAttributeForm } from "./admin-dance-attribute-form"
import { ToolbarEdit } from "./toolbar-edit"

export const AdminAddDanceAttributeView: FC = observer(
  function AdminAddDanceAttributeView() {
    const { uiStore, danceAttributesStore } = useStores()

    const [newAttributeStore] = useState(
      new DanceAttributesStore(
        uiStore.rootStore,
        [makeDefaultAttribute()],
        true,
      ),
    )

    const onCancel = () => {
      uiStore.restoreView()
    }
    const onSave = () => {
      const newAttribute = newAttributeStore.attributes[0]
      danceAttributesStore.addAttribute(newAttribute)
      uiStore.restoreView()
    }

    return (
      <>
        <ToolbarEdit
          pageTitle={"New Dance Attribute"}
          rightButtons={[
            {
              label: "Cancel",
              onClick: onCancel,
              classes: "cancel-edit-dance-attribute-button",
            },
            {
              label: "Save",
              onClick: onSave,
              classes: "is-link save-edit-dance-attribute-button",
            },
          ]}
        />
        <div className="columns is-marginless">
          <div className="column left-column column-under-toolbar">
            <AdminDanceAttributeForm
              attributesStore={newAttributeStore}
              forNewAttribute={true}
            />
          </div>
        </div>
      </>
    )
  },
)
