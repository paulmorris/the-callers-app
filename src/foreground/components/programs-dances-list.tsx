/*
Copyright 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React, { FC } from "react"

import {
  getFormationAbbreviation,
  ProgramDance,
  ProgramSpacer,
  ProgramItem,
  isProgramSpacer,
} from "../../shared/types"
import { useStores } from "../stores/stores-context"
import { DanceFigureTable } from "./DanceFigureTable"
import { useDragAndDrop } from "../hooks/use-drag-and-drop"

interface ProgramsDancesListProps {
  dances: ProgramItem[]
  viewDance?: (dance: ProgramDance) => void
  removeItemFromProgram?: (
    programDanceId: number,
    danceId: number | undefined,
  ) => void
  moveDance?: (newIndex: number, oldIndex: number) => Promise<void>
}

export const ProgramsDancesList: FC<ProgramsDancesListProps> = observer(
  function ProgramsDancesList(props) {
    const { dances, removeItemFromProgram, moveDance } = props
    const { uiStore } = useStores()

    const {
      items: programItems,
      dragState,
      getDragAttributes,
    } = useDragAndDrop([...dances], moveDance || (async (_, __) => {}))

    const [expandedSet, setExpandedSet] = React.useState(new Set())

    const expandToggle = (programDanceId: number) => {
      if (expandedSet.has(programDanceId)) {
        expandedSet.delete(programDanceId)
        setExpandedSet(new Set(expandedSet))
      } else {
        setExpandedSet(new Set(expandedSet.add(programDanceId)))
      }
    }

    const makeListItem = (index: number, body: JSX.Element) => {
      return moveDance ? (
        <li
          key={index}
          {...getDragAttributes(index)}
          className={
            "dances-list-item dances-list-item-editable" +
            (dragState.draggingOver && dragState.to === index
              ? " programs-dance-list-item-dragging"
              : "")
          }
        >
          {body}
        </li>
      ) : (
        <li key={index} className={"dances-list-item"}>
          {body}
        </li>
      )
    }

    const makeSpacerBody = (spacer: ProgramSpacer): JSX.Element => {
      return (
        <div className="program-spacer">
          {removeItemFromProgram && (
            <div className="dances-list-button-wrapper">
              <button
                className="button is-small"
                onClick={(event) => {
                  event.stopPropagation()
                  removeItemFromProgram?.(spacer.programDanceId, undefined)
                }}
              >
                X
              </button>
            </div>
          )}
        </div>
      )
    }

    const makeDanceBody = (programDance: ProgramDance): JSX.Element => {
      const dance = programDance.dance

      // const badgesTitle =
      //   "Has " +
      //   (dance.hasFigures ? "figures" : "") +
      //   (dance.hasFigures && dance.hasLink ? ", " : "") +
      //   (dance.hasLink ? "link to source for figures" : "") +
      //   (dance.hasLink && dance.hasVideo ? ", " : "") +
      //   (dance.hasVideo ? "link to video" : "")

      // Show dance.figures by default, else show dance.tcbFigures.
      const figuresToShow =
        "figures" in dance && dance.figures.length
          ? dance.figures
          : "tcbFigures" in dance && dance.tcbFigures.length
          ? dance.tcbFigures
          : undefined

      return (
        <>
          <div className="programs-dance-list-item-body">
            <div className="figures-toggle-container">
              {figuresToShow && (
                <button
                  className="plain-text-button"
                  onClick={(event) => {
                    event.stopPropagation()
                    expandToggle(programDance.programDanceId)
                  }}
                >
                  {expandedSet.has(programDance.programDanceId) ? "v" : ">"}
                </button>
              )}
            </div>

            <div className="title-author-box">
              <div className="dances-list-dance-title">
                {dance.title}
                {(dance.status === "deprecated" ||
                  dance.status === "broken") && (
                  <span
                    className="deprecated-or-broken"
                    title={
                      dance.status === "deprecated"
                        ? "The author of this dance advises that you not use it."
                        : "Something about this dance doesn't work."
                    }
                  >
                    ({dance.status})
                  </span>
                )}
              </div>
              <div className="dance-list-authors">
                {dance.authors.join(", ")}
              </div>
            </div>

            <div
              className="dances-list-formation-base"
              title={dance.formationBase}
            >
              {dance.formationBase
                ? getFormationAbbreviation(dance.formationBase)
                : ""}
            </div>
            {/* 
            <div className="dances-table-cell storage-status-cell">
              <StorageStatusToggle dance={dance} button={false} />
            </div>

            <div className="dances-table-cell badge-cell badge-cell-figures">
              {dance.hasFigures && <span title={badgesTitle}>Ⓕ</span>}
            </div>

            <div className="dances-table-cell badge-cell badge-cell-links">
              {dance.hasLink && <span title={badgesTitle}>Ⓛ</span>}
            </div>

            <div className="dances-table-cell badge-cell badge-cell-videos">
              {dance.hasVideo && <span title={badgesTitle}>Ⓥ</span>}
            </div> */}

            <div className="dances-list-button-wrapper">
              <button
                className="button is-small"
                onClick={(event) => {
                  event.stopPropagation()
                  uiStore.setModalDance(dance)
                }}
              >
                View
              </button>
            </div>

            {removeItemFromProgram && (
              <div className="dances-list-button-wrapper">
                <button
                  className="button is-small"
                  onClick={(event) => {
                    event.stopPropagation()
                    removeItemFromProgram?.(
                      programDance.programDanceId,
                      programDance.dance.danceId,
                    )
                  }}
                >
                  X
                </button>
              </div>
            )}
          </div>

          {figuresToShow && expandedSet.has(programDance.programDanceId) && (
            <div className="dances-list-figure-table-wrapper">
              <DanceFigureTable danceParts={figuresToShow} />
            </div>
          )}
        </>
      )
    }

    return (
      <ol className={"dances-list"}>
        {programItems.map((dance, index) => {
          return makeListItem(
            index,
            isProgramSpacer(dance)
              ? makeSpacerBody(dance)
              : makeDanceBody(dance),
          )
        })}
      </ol>
    )
  },
)
