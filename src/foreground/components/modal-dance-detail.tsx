/*
Copyright 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React, { FunctionComponent } from "react"
import { Dance, SearchResultsDance } from "../../shared/types"
import { DanceDetailView } from "./dance-detail-view"

interface ModalDanceDetailProps {
  dance: Dance | SearchResultsDance
  closeModal: () => void
}

export const ModalDanceDetail: FunctionComponent<ModalDanceDetailProps> = observer(
  function ModalDanceDetail(props) {
    const closeModal = () => props.closeModal()
    return (
      <div className="modal is-active dance-detail-modal">
        <div className="modal-background" onClick={closeModal}></div>
        <div className="modal-content dance-detail-modal-content has-background-white-bis">
          <DanceDetailView danceToView={props.dance}></DanceDetailView>
        </div>
        <button
          className="modal-close is-large"
          aria-label="close"
          onClick={closeModal}
        ></button>
      </div>
    )
  },
)
