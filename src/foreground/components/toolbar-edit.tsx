/*
Copyright 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React, { FunctionComponent, ChangeEvent } from "react"

interface ToolbarEditProps {
  pageTitle: string
  leftControls?: React.ReactElement
  rightButtons: Array<{
    label: string
    onClick: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
    classes?: string
  }>
}

export const ToolbarEdit: FunctionComponent<ToolbarEditProps> = observer(
  function ToolbarEdit(props) {
    return (
      <nav className="level is-mobile toolbar-main">
        <div className="level-left">
          <div className="level-item">
            <h1 className="title is-5" style={{ marginRight: "12px" }}>
              {props.pageTitle}
            </h1>
            {props.leftControls && props.leftControls}
          </div>
        </div>

        <div className="level-right">
          <div className="level-item">
            <div className="field is-grouped">
              {props.rightButtons.map((button, index) => (
                <div key={button.label + index} className="control">
                  <button
                    className={`button is-small ${button.classes}`}
                    onClick={button.onClick}
                  >
                    {button.label}
                  </button>
                </div>
              ))}
            </div>
          </div>
        </div>
      </nav>
    )
  },
)
