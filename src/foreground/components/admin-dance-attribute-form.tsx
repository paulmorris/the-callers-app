/*
Copyright 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React, { FC } from "react"
import {
  DanceAttributesStore,
  DanceAttributeValue,
} from "../stores/dance-attributes-store"

const inputClass = "input is-small"
const checkboxClass = "checkbox is-small"

interface AdminDanceAttributeFormProps {
  attributesStore: DanceAttributesStore
  forNewAttribute: boolean
}

export const AdminDanceAttributeForm: FC<AdminDanceAttributeFormProps> = observer(
  function AdminDanceAttributeForm(props) {
    const store = props.attributesStore

    const onChangeInput = (
      index: number,
      key: string,
      value: string | boolean,
    ) => {
      store.updateAttribute(index, { [key]: value })
    }

    const addValue = (event: React.MouseEvent, index: number) => {
      event.preventDefault()
      store.addValue(index)
    }

    const removeValue = (
      event: React.MouseEvent,
      attributeIndex: number,
      valueIndex: number,
    ) => {
      event.preventDefault()
      store.removeValue(attributeIndex, valueIndex)
    }

    const onChangeValueInput = (
      attributeIndex: number,
      valueIndex: number,
      patch: Partial<DanceAttributeValue>,
    ) => {
      store.updateValue(attributeIndex, valueIndex, patch)
    }

    const onChangeDefaultValue = (
      attributeIndex: number,
      valueIndexClicked: number,
    ) => {
      store.updateDefaultValue(attributeIndex, valueIndexClicked)
    }

    const onRemoveAttribute = (
      event: React.MouseEvent,
      attributeIndex: number,
    ) => {
      event.preventDefault()
      store.removeAttribute(attributeIndex)
    }

    return (
      <form>
        <table className="table is-narrow default-table">
          <tbody>
            <tr>
              <th>
                <br />
                Name
              </th>
              <th>
                <br />
                Description
              </th>
              <th>
                Program
                <br />
                Grid?
              </th>
              {/* TODO: multi-type-attributes */}
              {/* Only allow field-with-menu type for now, and maybe forever.
              <th>
                <br />
                Input Type
              </th>
              */}
              <th>
                Value
                <br />
                (Shown in Grid)
              </th>
              <th>
                <br />
                Value Label
              </th>
              <th>
                Default
                <br />
                Value?
              </th>
              <th></th>
              <th></th>
              <th></th>
            </tr>
            {store.attributes.map((attribute, attributeIndex) => {
              // TODO: multi-type-attributes
              // const valuesCount = attribute.values.length
              // const inputType = attribute.inputType
              //
              // const showAddValueButton =
              // inputType === "menu" ||
              // inputType === "field-with-menu" ||
              // (inputType === "checkbox" && valuesCount === 0)
              //
              // const showRemoveValueButton =
              // inputType === "menu" ||
              // inputType === "field-with-menu" ||
              // (inputType === "checkbox" && valuesCount > 1) ||
              // (inputType === "field" && valuesCount > 0)
              //
              // Let users remove all values if they want to. Initially, when
              // we were supporting multiple input types, removing the last
              // value for menu attribute types was unsupported.
              // const disableRemoveValueButton =
              //   (inputType === "menu" || inputType === "field-with-menu") &&
              //   valuesCount === 1

              return (
                <tr key={attribute.id}>
                  <td>
                    {/* Use a div to ensure consistent minimum row height. */}
                    <div className="attribute-cell">
                      <input
                        className={inputClass}
                        value={attribute.name}
                        placeholder="Name"
                        onChange={(event) =>
                          onChangeInput(
                            attributeIndex,
                            "name",
                            event.target.value,
                          )
                        }
                      />
                    </div>
                  </td>
                  <td>
                    <input
                      className={inputClass}
                      value={attribute.description}
                      placeholder="Description"
                      onChange={(event) =>
                        onChangeInput(
                          attributeIndex,
                          "description",
                          event.target.value,
                        )
                      }
                    />
                  </td>
                  <td>
                    <input
                      className={checkboxClass}
                      checked={attribute.showInGrid}
                      type="checkbox"
                      onChange={(event) =>
                        onChangeInput(
                          attributeIndex,
                          "showInGrid",
                          event.target.checked,
                        )
                      }
                    />
                  </td>
                  {/* TODO multi-type-attributes */}
                  {/* Only offer field-with-menu for now and maybe forever.
                  <td>
                    <div className="select is-small">
                      <select
                        onChange={(event) =>
                          onChangeInput(
                            attributeIndex,
                            "inputType",
                            event.target.value,
                          )
                        }
                        value={attribute.inputType}
                      >
                        <option value="checkbox">Checkbox</option>
                        <option value="menu">Menu</option>
                        <option value="field">Field</option>
                        <option value="field-with-menu">Field + Menu</option>
                      </select>
                    </div>
                  </td>
                  */}
                  <td className="value-td-name">
                    {attribute.values.map((value, valueIndex) => {
                      return (
                        <div className="value-cell" key={value.id}>
                          <input
                            className={inputClass}
                            value={value.val}
                            placeholder="Name"
                            onChange={(event) =>
                              onChangeValueInput(attributeIndex, valueIndex, {
                                val: event.target.value,
                              })
                            }
                          />
                        </div>
                      )
                    })}
                  </td>
                  <td className="value-td-label">
                    {attribute.values.map((value, valueIndex) => {
                      return (
                        <div className="value-cell" key={value.id}>
                          <input
                            className={inputClass}
                            value={value.label}
                            placeholder="Value Label"
                            onChange={(event) =>
                              onChangeValueInput(attributeIndex, valueIndex, {
                                label: event.target.value,
                              })
                            }
                          />
                        </div>
                      )
                    })}
                  </td>
                  <td className="value-td-default-value">
                    {attribute.values.map((value, valueIndex) => {
                      return (
                        <div className="value-cell" key={value.id}>
                          <input
                            className={checkboxClass}
                            checked={value.isDefault}
                            onChange={() =>
                              onChangeDefaultValue(attributeIndex, valueIndex)
                            }
                            type="checkbox"
                          />
                        </div>
                      )
                    })}
                  </td>
                  <td className="value-td-remove-value">
                    {attribute.values.map((value, valueIndex) => {
                      return (
                        <div className="value-cell" key={value.id}>
                          <button
                            className="button is-small"
                            onClick={(event) =>
                              removeValue(event, attributeIndex, valueIndex)
                            }
                          >
                            Remove Value
                          </button>
                        </div>
                      )
                    })}
                  </td>
                  <td>
                    <button
                      className="button is-small"
                      onClick={(event) => addValue(event, attributeIndex)}
                    >
                      Add Value
                    </button>
                  </td>
                  <td>
                    {!props.forNewAttribute && (
                      <button
                        className="button is-small"
                        onClick={(event) =>
                          onRemoveAttribute(event, attributeIndex)
                        }
                      >
                        Remove Attribute
                      </button>
                    )}
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </form>
    )
  },
)
