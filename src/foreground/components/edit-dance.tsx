/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React, { FunctionComponent, ChangeEvent } from "react"

import {
  formationOptions,
  EditableDance,
  HtmlInputElements,
} from "../../shared/types"
import { splitAtNewlines } from "../../shared/utils"
import { DanceAttributeValue } from "../stores/dance-attributes-store"
import { useStores } from "../stores/stores-context"
import { FiguresInput } from "./figures-input"
import { ToolbarEdit } from "./toolbar-edit"

const figuresExampleString = `# A1
(8) figure
(8) figure
# A2
(8) figure
(8) figure
# B1
(16) figure X
> (8) sub figure X1
> (8) sub figure X2
# B2
(8) figure
(8) figure`

interface HorizontalInputProps {
  label: string
  disabled?: boolean
  helpText?: string
  selectOptions?: { name: string; value: string }[]
  selectDisabled?: boolean
  selectOnChange?: (event: ChangeEvent<HTMLSelectElement>) => void
}

export const HorizontalInput: FunctionComponent<HorizontalInputProps> = observer(
  function HorizontalInput(props) {
    return (
      <div className="field is-horizontal">
        <div className="field-label is-small">
          <label className="label has-text-weight-normal">{props.label}</label>
        </div>
        <div className="field-body">
          <fieldset className="width-100pct" disabled={props.disabled}>
            <div className="field is-expanded">
              <div className="field is-grouped">
                <div className="control is-expanded">{props.children}</div>
                {props.selectOptions && props.selectOptions.length > 0 ? (
                  <fieldset disabled={props.selectDisabled}>
                    <p className="control">
                      <span className="select is-small">
                        <select
                          className="text-input-select-width"
                          onChange={props.selectOnChange}
                          disabled={props.selectDisabled}
                          value=""
                        >
                          <option hidden></option>
                          {props.selectOptions?.map((opt, index) => (
                            <option value={opt.value} key={index}>
                              {opt.name}
                            </option>
                          ))}
                        </select>
                      </span>
                    </p>
                  </fieldset>
                ) : (
                  // For consistent spacing purposes.
                  <span className="text-input-select-width"></span>
                )}
              </div>
              {props.helpText && <p className="help">Help text goes here.</p>}
            </div>
          </fieldset>
        </div>
      </div>
    )
  },
)

const getRowCount = (min: number, str: string): number => {
  return Math.max(min, 1 + splitAtNewlines(str).length)
}

/**
 * The dance being edited could be:
 * - a callers-box dance with figures
 * - a callers-box dance without figures
 * - a brand new non-callers-box dance
 * - an existing non-callers-box dance
 */
export const EditDanceView: FunctionComponent = observer(
  function EditDanceView() {
    const { dancesEditStore, danceAttributesStore } = useStores()
    const dance = dancesEditStore.danceDuringEdit

    // TS placation
    if (!dance) {
      return <div>Sorry, an error has occurred (no dance to edit).</div>
    }

    const editingNewDance = dancesEditStore.editingNewDance
    const pageTitle = editingNewDance ? "New Dance" : "Edit Dance"

    const onCancel = (event: React.MouseEvent): void => {
      event.preventDefault()
      if (editingNewDance) {
        dancesEditStore.cancelNewDance()
      } else {
        dancesEditStore.cancelEditDance()
      }
    }

    const onSave = (event: React.MouseEvent): void => {
      event.preventDefault()
      dancesEditStore.saveDanceEdits()
    }

    // TCB figures are disabled if they are from the Caller's Box site (dance has
    // a TCB id and a permission is "search" or "no_figures").
    const isTcbDanceWithFigures = !dance.tcbId
      ? false
      : dance.permission !== "search" && dance.permission !== "no_figures"

    const onUpdateForm = (
      event: ChangeEvent<HtmlInputElements>,
      searchFormKey: keyof EditableDance,
    ): void => {
      dancesEditStore.updateEditDanceForm(searchFormKey, event.target.value)
    }

    const onChangeCheckboxAttribute = (
      attributeId: string,
      valueId: string,
    ) => {
      if (dance.attributes[attributeId]) {
        delete dance.attributes[attributeId]
      } else {
        dance.attributes[attributeId] = valueId
      }
    }

    const onChangeMenuAttribute = (attributeId: string, valueId: string) => {
      dance.attributes[attributeId] = valueId
    }

    const onChangeFieldAttribute = (attributeId: string, value: string) => {
      dance.attributes[attributeId] = value
    }

    return (
      <>
        <ToolbarEdit
          pageTitle={pageTitle}
          rightButtons={[
            {
              label: "Cancel",
              onClick: onCancel,
              classes: "cancel-edit-dance-button",
            },
            {
              label: "Save",
              onClick: onSave,
              classes: "is-link save-edit-dance-button",
            },
          ]}
        />
        <div className="columns edit-dance-body">
          <div className="column">
            <HorizontalInput label={"Title"}>
              <input
                className="input is-small"
                type="text"
                value={dance.title}
                onChange={(event) => onUpdateForm(event, "title")}
              />
            </HorizontalInput>

            <HorizontalInput
              label="Figures"
              selectOptions={[
                {
                  name: "Use Default Template",
                  value: figuresExampleString,
                },
                {
                  name: "Set to TCB figures",
                  value: dance.editableTcbFigures,
                },
              ]}
              selectOnChange={(event) => onUpdateForm(event, "editableFigures")}
            >
              <FiguresInput
                value={dance.editableFigures}
                setValue={(value) => {
                  dancesEditStore.updateEditDanceForm("editableFigures", value)
                }}
                rows={getRowCount(12, dance.editableFigures)}
                suggestions={dancesEditStore.figureSuggestions}
                searchForSuggestions={dancesEditStore.searchFigures.bind(
                  dancesEditStore,
                )}
              />
            </HorizontalInput>

            <HorizontalInput
              label="TCB Figures"
              disabled={isTcbDanceWithFigures}
              selectOptions={[
                {
                  name: "Use Default Template",
                  value: figuresExampleString,
                },
              ]}
              selectOnChange={(event) =>
                onUpdateForm(event, "editableTcbFigures")
              }
            >
              <FiguresInput
                value={dance.editableTcbFigures}
                setValue={(value) => {
                  dancesEditStore.updateEditDanceForm(
                    "editableTcbFigures",
                    value,
                  )
                }}
                rows={getRowCount(12, dance.editableTcbFigures)}
                suggestions={dancesEditStore.tcbFigureSuggestions}
                searchForSuggestions={dancesEditStore.searchTcbFigures.bind(
                  dancesEditStore,
                )}
              />
            </HorizontalInput>

            <HorizontalInput label="Tags">
              <input
                className="input is-small"
                type="text"
                value={dance.editableTags}
                onChange={(event) => onUpdateForm(event, "editableTags")}
              />
            </HorizontalInput>

            <HorizontalInput label={"Calling Notes"}>
              <textarea
                className="textarea is-small"
                value={dance.editableCallingNotes}
                rows={getRowCount(5, dance.editableCallingNotes)}
                onChange={(event) =>
                  onUpdateForm(event, "editableCallingNotes")
                }
              />
            </HorizontalInput>
          </div>

          <div className="column">
            <HorizontalInput label={"Authors"}>
              <textarea
                className="textarea is-small"
                value={dance.editableAuthors}
                rows={getRowCount(2, dance.editableAuthors)}
                onChange={(event) => onUpdateForm(event, "editableAuthors")}
              />
            </HorizontalInput>

            <HorizontalInput
              label="Formation Base"
              selectOptions={formationOptions
                .filter((val) => !["", "Duple Minor", "Other"].includes(val))
                .map((val) => ({ name: val, value: val }))}
              selectOnChange={(event) => onUpdateForm(event, "formationBase")}
            >
              <input
                className="input is-small"
                type="text"
                value={dance.formationBase}
                onChange={(event) => onUpdateForm(event, "formationBase")}
              />
            </HorizontalInput>

            <HorizontalInput label={"Formation Detail"}>
              <input
                className="input is-small"
                type="text"
                value={dance.formationDetail}
                onChange={(event) => onUpdateForm(event, "formationDetail")}
              />
            </HorizontalInput>

            <HorizontalInput label="Mixer">
              <div className="select is-small">
                <select
                  value={dance.mixer}
                  onChange={(event) => onUpdateForm(event, "mixer")}
                >
                  {[
                    { name: "Mixer", value: "yes" },
                    { name: "Not a Mixer", value: "" },
                  ].map((opt, index) => (
                    <option value={opt.value} key={index}>
                      {opt.name}
                    </option>
                  ))}
                </select>
              </div>
            </HorizontalInput>

            <HorizontalInput
              label={"Progression"}
              selectOptions={[
                "None",
                "Single",
                "Double",
                "Triple",
                "Quadruple",
                "Quintuple",
                "Sextuple",
                "Septuple",
                "Octuple",
              ].map((val) => ({ name: val, value: val }))}
              selectOnChange={(event) => onUpdateForm(event, "progression")}
            >
              <input
                className="input is-small"
                type="text"
                value={dance.progression}
                onChange={(event) => onUpdateForm(event, "progression")}
              />
            </HorizontalInput>

            <HorizontalInput
              label={"Direction"}
              selectOptions={[
                { name: "CW", value: "CW" },
                { name: "CCW", value: "CCW" },
              ]}
              selectOnChange={(event) => onUpdateForm(event, "direction")}
            >
              <input
                className="input is-small"
                type="text"
                value={dance.direction}
                onChange={(event) => onUpdateForm(event, "direction")}
              />
            </HorizontalInput>

            <HorizontalInput label={"Phrase Structure"}>
              <input
                className="input is-small"
                type="text"
                value={dance.phraseStructure}
                onChange={(event) => onUpdateForm(event, "phraseStructure")}
              />
            </HorizontalInput>

            <HorizontalInput label={"Music"}>
              <textarea
                className="textarea is-small"
                value={dance.editableMusic}
                rows={getRowCount(2, dance.editableMusic)}
                onChange={(event) => onUpdateForm(event, "editableMusic")}
              />
            </HorizontalInput>

            <HorizontalInput label={"Based On"}>
              <textarea
                className="textarea is-small"
                value={dance.editableBasedOn}
                rows={getRowCount(2, dance.editableBasedOn)}
                onChange={(event) => onUpdateForm(event, "editableBasedOn")}
              />
            </HorizontalInput>

            <HorizontalInput label={"Other Titles"}>
              <textarea
                className="textarea is-small"
                value={dance.editableOtherTitles}
                rows={getRowCount(2, dance.editableOtherTitles)}
                onChange={(event) => onUpdateForm(event, "editableOtherTitles")}
              />
            </HorizontalInput>

            <HorizontalInput label="Status">
              <div className="select is-small">
                <select
                  value={dance.status}
                  onChange={(event) => onUpdateForm(event, "status")}
                >
                  {[
                    { name: "(None)", value: "none" },
                    { name: "Deprecated", value: "deprecated" },
                    { name: "Broken", value: "broken" },
                  ].map((opt, index) => (
                    <option value={opt.value} key={index}>
                      {opt.name}
                    </option>
                  ))}
                </select>
              </div>
            </HorizontalInput>
          </div>
          <div className="column">
            <h3>Attributes</h3>
            {danceAttributesStore.attributes.map((attribute) => {
              // TODO: multi-type-attributes
              // TS Placation.
              // const selectedValueId =
              //   dance.attributes[attribute.id] ||
              //   attribute.values.find((v) => v.isDefault)?.id

              return (
                <React.Fragment key={attribute.id}>
                  {/* TODO: multi-type-attributes */}
                  {/*
                  {attribute.inputType === "checkbox" && (
                    <HorizontalInput label={attribute.name}>
                      <label className="checkbox">
                        <input
                          type="checkbox"
                          checked={!!dance.attributes[attribute.id]}
                          onChange={(event) => {
                            onChangeCheckboxAttribute(
                              attribute.id,
                              attribute.values[0].id,
                            )
                          }}
                        />
                        {getValueText(attribute.values[0])}
                      </label>
                    </HorizontalInput>
                  )}
                  */}
                  {/*
                  {attribute.inputType === "menu" && (
                    <HorizontalInput label={attribute.name}>
                      <div className="select is-small">
                        <select
                          value={selectedValueId}
                          onChange={(event) =>
                            onChangeMenuAttribute(
                              attribute.id,
                              event.target.value,
                            )
                          }
                        >
                          {attribute.values.map((value) => {
                            return (
                              <option value={value.id} key={value.id}>
                                {getValueText(value)}
                              </option>
                            )
                          })}
                        </select>
                      </div>
                    </HorizontalInput>
                  )}
                  */}
                  {/*
                  {attribute.inputType === "field" && (
                    <HorizontalInput label={attribute.name}>
                      <input
                        className="input is-small"
                        type="text"
                        value={dance.attributes[attribute.id]}
                        onChange={(event) => {
                          onChangeFieldAttribute(
                            attribute.id,
                            event.target.value,
                          )
                        }}
                      />
                    </HorizontalInput>
                  )}
                  */}
                  {/* {attribute.inputType === "field-with-menu" && ( */}

                  <HorizontalInput
                    label={attribute.name}
                    selectOptions={attribute.values.map((value) => {
                      return {
                        name: getValueText(value),
                        value: value.val,
                      }
                    })}
                    selectOnChange={(event) => {
                      onChangeFieldAttribute(attribute.id, event.target.value)
                    }}
                  >
                    <input
                      className="input is-small"
                      type="text"
                      value={dance.attributes[attribute.id]}
                      onChange={(event) => {
                        onChangeFieldAttribute(attribute.id, event.target.value)
                      }}
                    />
                  </HorizontalInput>
                </React.Fragment>
              )
            })}
          </div>
        </div>

        <div className="columns edit-dance-appearances-videos">
          <div className="column">
            <HorizontalInput label={"Appearances"}>
              <textarea
                className="textarea is-small"
                value={dance.editableAppearances}
                rows={getRowCount(2, dance.editableAppearances)}
                onChange={(event) => onUpdateForm(event, "editableAppearances")}
              />
            </HorizontalInput>

            <HorizontalInput label={"Videos"}>
              <textarea
                className="textarea is-small"
                value={dance.editableVideos}
                rows={getRowCount(2, dance.editableVideos)}
                onChange={(event) => onUpdateForm(event, "editableVideos")}
              />
            </HorizontalInput>

            <HorizontalInput label={"Variant Videos"}>
              <textarea
                className="textarea is-small"
                value={dance.editableVariantVideos}
                rows={getRowCount(2, dance.editableVariantVideos)}
                onChange={(event) =>
                  onUpdateForm(event, "editableVariantVideos")
                }
              />
            </HorizontalInput>
          </div>
        </div>
      </>
    )
  },
)

export function getValueText(value: DanceAttributeValue): string {
  const text = value.label ? `${value.val} (${value.label})` : value.val
  return text
}
