/*
Copyright 2021, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React, { ChangeEvent } from "react"

import { HtmlInputElements, Program } from "../../shared/types"
import { useStores } from "../stores/stores-context"
import { InputBasic } from "./form-components"

export const ProgramsEditFormRow = observer(
  function ProgramsEditFormRow(props: {
    program: Program
    isStatic?: boolean
  }) {
    const { programsEditStore } = useStores()
    const { program, isStatic } = props

    const sizeClass = " is-small"
    const staticClass = isStatic ? " is-static" : ""
    const inputClass = "input is-small" + sizeClass + staticClass
    const readOnly = isStatic ? true : undefined

    const onChangeProgram = (
      event: ChangeEvent<HtmlInputElements>,
      fieldKey: keyof Program,
    ): void => {
      programsEditStore.changeProgram(fieldKey, event.target.value)
    }

    return (
      <div className="columns is-marginless programs-edit-form-row">
        <div className="column" style={{ maxWidth: "10rem" }}>
          <InputBasic>
            <input
              className={inputClass}
              value={program.date || ""}
              placeholder="Date"
              onChange={(event) => onChangeProgram(event, "date")}
              type="date"
              readOnly={readOnly}
            />
          </InputBasic>
        </div>
        <div className="column">
          <InputBasic>
            <input
              className={inputClass}
              value={program.location || ""}
              placeholder="Location"
              onChange={(event) => onChangeProgram(event, "location")}
              readOnly={readOnly}
            />
          </InputBasic>
        </div>
        <div className="column">
          <InputBasic>
            <input
              className={inputClass}
              value={program.music || ""}
              placeholder="Music"
              onChange={(event) => onChangeProgram(event, "music")}
              readOnly={readOnly}
            />
          </InputBasic>
        </div>
        <div className="column">
          <InputBasic>
            <input
              className={inputClass}
              value={program.notes || ""}
              placeholder="Notes"
              onChange={(event) => onChangeProgram(event, "notes")}
              readOnly={readOnly}
            />
          </InputBasic>
        </div>
      </div>
    )
  },
)
