/*
Copyright 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React, { FunctionComponent } from "react"

import { useStores } from "../stores/stores-context"
import { AdminDanceAttributeForm } from "./admin-dance-attribute-form"
import { ToolbarMain } from "./toolbar-main"

export const AdminView: FunctionComponent = observer(function AdminView() {
  const { uiStore, danceAttributesStore } = useStores()

  const addAttribute = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ) => {
    uiStore.setView("adminAddDanceAttribute")
  }

  return (
    <>
      <ToolbarMain activeTab="admin"></ToolbarMain>
      <div className="columns is-marginless">
        <div className="column left-column column-under-toolbar">
          <h1>Dance Attributes</h1>
          <button className="button is-small" onClick={addAttribute}>
            New Attribute
          </button>
          <AdminDanceAttributeForm
            attributesStore={danceAttributesStore}
            forNewAttribute={false}
          />
        </div>
      </div>
    </>
  )
})
