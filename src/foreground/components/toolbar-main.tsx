/*
Copyright 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React, { FunctionComponent } from "react"
import { useStores } from "../stores/stores-context"

interface ToolbarMainProps {
  activeTab: "dances" | "programs" | "admin"
}

export const ToolbarMain: FunctionComponent<ToolbarMainProps> = observer(
  function ToolbarMain(props) {
    const { uiStore, dancesEditStore, programsBrowseStore } = useStores()
    const dancesClasses = props.activeTab === "dances" ? "is-active" : ""
    const programsClasses = props.activeTab === "programs" ? "is-active" : ""
    const adminClasses = props.activeTab === "admin" ? "is-active" : ""

    return (
      <nav className="level is-mobile toolbar-main">
        <div className="level-left">
          <div className="level-item">
            <div className="tabs is-small is-toggle">
              <ul>
                <li className={dancesClasses}>
                  <a onClick={() => uiStore.setView("searchDances")}>Dances</a>
                </li>
                <li className={programsClasses}>
                  <a onClick={() => uiStore.setView("programsBrowse")}>
                    Programs
                  </a>
                </li>
                <li className={adminClasses}>
                  <a onClick={() => uiStore.setView("admin")}>Admin</a>
                </li>
              </ul>
            </div>
          </div>
          <div className="level-item">
            <button
              className="button is-small"
              onClick={() => dancesEditStore.createNewDance()}
              title={"Create a New Dance"}
            >
              New Dance
            </button>
          </div>
          <div className="level-item">
            <button
              className="button is-small"
              onClick={() => {
                programsBrowseStore.createProgram()
              }}
              title={"Create a New Program"}
            >
              New Program
            </button>
          </div>
        </div>
        <div className="level-right"></div>
      </nav>
    )
  },
)
