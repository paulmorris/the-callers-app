/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React, { FunctionComponent } from "react"

import { StorageStatusToggle } from "./storage-status-toggle"
import {
  SearchResultsDance,
  getFormationAbbreviation,
} from "../../shared/types"

interface DancesTableProps {
  dances: SearchResultsDance[]
  viewDance: (dance: SearchResultsDance) => void
  editDance?: (dance: SearchResultsDance) => void
  addDanceToProgram?: (dance: SearchResultsDance) => void
}

export const DancesTable: FunctionComponent<DancesTableProps> = observer(
  function DancesTable(props) {
    return (
      <table className="table default-table dances-table is-fullwidth is-narrow">
        <tbody>
          {props.dances.map((dance, index) => {
            const badgesTitle =
              "Has " +
              (dance.hasFigures ? "figures" : "") +
              (dance.hasFigures && dance.hasLink ? ", " : "") +
              (dance.hasLink ? "link to source for figures" : "") +
              (dance.hasLink && dance.hasVideo ? ", " : "") +
              (dance.hasVideo ? "link to video" : "")

            return (
              <tr
                key={index}
                className="dances-table-row"
                onClick={() => props.viewDance(dance)}
                onDoubleClick={() => props.editDance?.(dance)}
              >
                <td className="dances-table-cell storage-status-cell">
                  <StorageStatusToggle dance={dance} button={false} />
                </td>

                <td className="dances-table-cell badge-cell badge-cell-figures">
                  {dance.hasFigures && <span title={badgesTitle}>Ⓕ</span>}
                </td>

                <td className="dances-table-cell badge-cell badge-cell-links">
                  {dance.hasLink && <span title={badgesTitle}>Ⓛ</span>}
                </td>

                <td className="dances-table-cell badge-cell badge-cell-videos">
                  {dance.hasVideo && <span title={badgesTitle}>Ⓥ</span>}
                </td>

                <td className="dances-table-cell dances-list-dance-title">
                  {dance.title}
                  {(dance.status === "deprecated" ||
                    dance.status === "broken") && (
                    <span
                      className="deprecated-or-broken"
                      title={
                        dance.status === "deprecated"
                          ? "The author of this dance advises that you not use it."
                          : "Something about this dance doesn't work."
                      }
                    >
                      ({dance.status})
                    </span>
                  )}
                </td>

                <td className="dances-table-cell authors-cell">
                  {dance.authors.join(", ")}
                </td>

                <td
                  className="dances-table-cell formation-base-cell"
                  title={dance.formationBase}
                >
                  {dance.formationBase
                    ? getFormationAbbreviation(dance.formationBase)
                    : ""}
                </td>

                {props.addDanceToProgram && (
                  <td>
                    <button
                      className="button is-small"
                      onClick={(event) => {
                        event.stopPropagation()
                        // Typescript placation
                        props.addDanceToProgram?.(dance)
                      }}
                    >
                      Add
                    </button>
                  </td>
                )}
              </tr>
            )
          })}
        </tbody>
      </table>
    )
  },
)
