export const figureButtonsData = [
  { label: "A1", value: "# A1" },
  { label: "A2", value: "# A2" },
  { label: "B1", value: "# B1" },
  { label: "B2", value: "# B2" },
  {
    label: "(4) Partner balance (12) Partner swing",
    value: "(4) Partner balance\n(12) Partner swing",
  },
  {
    label: "(4) Neighbor balance (12) Neighbor swing",
    value: "(4) Neighbor balance\n(12) Neighbor swing",
  },
  {
    label: "(4) Ones balance (12) Ones swing",
    value: "(4) Ones balance\n(12) Ones swing",
  },
  {
    label: "(8) Partner swing",
    value: "(8) Partner swing",
  },
  {
    label: "(8) Neighbor swing",
    value: "(8) Neighbor swing",
  },
  {
    label: "(8) Ones swing",
    value: "(8) Ones swing",
  },
  {
    label: "(8) In long lines, go forward and back",
    value: "(8) In long lines, go forward and back",
  },
  {
    label: "(8) Right and left through with partner",
    value: "(8) Right and left through with partner",
  },
  {
    label: "(8) Right and left through with neighbor",
    value: "(8) Right and left through with neighbor",
  },
  {
    label: "(8) Ladies chain to partner",
    value: "(8) Ladies chain to partner",
  },
  {
    label: "(8) Ladies chain to neighbor",
    value: "(8) Ladies chain to neighbor",
  },
  {
    label: "(8) Partner gypsy right",
    value: "(8) Partner gypsy right",
  },
  {
    label: "(8) Neighbor gypsy right",
    value: "(8) Neighbor gypsy right",
  },
  {
    label: "(8) Men allemande left 1",
    value: "(8) Men allemande left 1",
  },
  {
    label: "(8) Women allemande left 1",
    value: "(8) Women allemande left 1",
  },
  {
    label: "(8) Neighbor allemande left 1",
    value: "(8) Neighbor allemande left 1",
  },
  {
    label: "(8) Men allemande left 1 & 1/2",
    value: "(8) Men allemande left 1 & 1/2",
  },
  {
    label: "(8) Women allemande left 1 & 1/2",
    value: "(8) Women allemande left 1 & 1/2",
  },
  {
    label: "(8) Neighbor allemande left 1 & 1/2",
    value: "(8) Neighbor allemande left 1 & 1/2",
  },
  {
    label: "(4) Balance ring (4) Petronella turn",
    value: "(4) Balance ring\n(4) Petronella turn",
  },
  {
    label: "(8) Circle left 1",
    value: "(8) Circle left 1",
  },
  {
    label: "(8) Circle right 1",
    value: "(8) Circle right 1",
  },
  {
    label: "(8) Circle left 3/4",
    value: "(8) Circle left 3/4",
  },
  {
    label: "(8) Circle right 3/4",
    value: "(8) Circle right 3/4",
  },
  {
    label: "(8) Star left 1",
    value: "(8) Star left 1",
  },
  {
    label: "(8) Star right 1",
    value: "(8) Star right 1",
  },
  {
    label: "(8) Star left 3/4",
    value: "(8) Star left 3/4",
  },
  {
    label: "(8) Star right 3/4",
    value: "(8) Star right 3/4",
  },
  {
    label: "(8) Partner promenade across",
    value: "(8) Partner promenade across",
  },
  {
    label: "(8) Neighbor promenade across",
    value: "(8) Neighbor promenade across",
  },
  {
    label: "(8) Partner do-si-do",
    value: "(8) Partner do-si-do",
  },
  {
    label: "(8) Neighbor do-si-do",
    value: "(8) Neighbor do-si-do",
  },
  {
    label: "(8) Men do-si-do",
    value: "(8) Men do-si-do",
  },
  {
    label: "(8) Women do-si-do",
    value: "(8) Women do-si-do",
  },
]
