/*
Copyright 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React, { useState } from "react"

import { useStores } from "../stores/stores-context"
import { DancesTable } from "./dances-table"
import { ProgramsDancesGrid } from "./programs-dances-grid"
import { ProgramsDancesList } from "./programs-dances-list"
import { ProgramsEditFormRow } from "./programs-edit-form-row"
import { SearchForm } from "./search-form"
import { SearchFormHorizontal } from "./search-form-horizontal"
import { ToolbarEdit } from "./toolbar-edit"

export const ProgramsEditView = observer(function ProgramsEditView() {
  const {
    programsStore,
    programsEditStore,
    programsDancesSearchStore,
    uiStore,
    programsBrowseStore,
    dancesSearchStore,
  } = useStores()

  const [showGrid, setShowGrid] = useState(true)
  const [showSearchForm, setShowSearchForm] = useState(false)

  // Allow showing/hiding inline dance figures etc.
  // A set of program dance IDs.
  const [
    programDancesShowingFigures,
    setProgramDancesShowingFigures,
  ] = useState(new Set<number>())

  const [
    searchResultsDancesShowingFigures,
    setSearchResultsDancesShowingFigures,
  ] = useState(new Set<number>())

  const programToEdit = programsEditStore.programToEdit
  const editingNewProgram = programsEditStore.editingNewProgram
  const pageTitle = editingNewProgram ? "New Program" : "Edit Program"

  const allProgramDanceFiguresAreShown =
    programToEdit.dances &&
    programToEdit.dances.length === programDancesShowingFigures.size

  const showProgramDanceFigures = (programDanceId: number) => {
    if (programDancesShowingFigures.has(programDanceId)) {
      programDancesShowingFigures.delete(programDanceId)
    } else {
      programDancesShowingFigures.add(programDanceId)
    }
    setProgramDancesShowingFigures(new Set(programDancesShowingFigures))
  }

  const showSearchResultsDanceFigures = (id: number) => {
    if (searchResultsDancesShowingFigures.has(id)) {
      searchResultsDancesShowingFigures.delete(id)
    } else {
      searchResultsDancesShowingFigures.add(id)
    }
    setSearchResultsDancesShowingFigures(
      new Set(searchResultsDancesShowingFigures),
    )
  }

  const toggleAllProgramDanceFigures = () => {
    if (programToEdit.dances) {
      setProgramDancesShowingFigures(
        new Set(
          allProgramDanceFiguresAreShown
            ? []
            : programToEdit.dances.map((dance) => dance.programDanceId),
        ),
      )
    }
  }

  const onSave = (event: React.MouseEvent) => {
    event.preventDefault()
    programsBrowseStore.setProgramToView(programToEdit)
    uiStore.restoreView()
  }

  const onCancel = (event: React.MouseEvent) => {
    event.preventDefault()
    if (editingNewProgram) {
      programsEditStore.cancelNewProgram()
    } else {
      programsEditStore.cancelEditProgram()
    }
  }

  const onGridButtonClick = (event: React.MouseEvent) => {
    event.preventDefault()
    setShowGrid(!showGrid)
  }

  return (
    <>
      <ToolbarEdit
        pageTitle={pageTitle}
        leftControls={
          <div className={"field is-grouped"}>
            <div className={"control"}>
              <button className={"button is-small"} onClick={onGridButtonClick}>
                Grid
              </button>
            </div>
            <div className={"control"}>
              <button
                className={"button is-small"}
                onClick={() => programsStore.addSpacerToProgram(programToEdit)}
              >
                Add Space
              </button>
            </div>
            <div className={"control"}>
              <button
                className={"button is-small"}
                onClick={toggleAllProgramDanceFigures}
              >
                {allProgramDanceFiguresAreShown
                  ? "Hide All Figures"
                  : "Show All Figures"}
              </button>
            </div>
            {showGrid && (
              <div className={"control"}>
                <button
                  className={"button is-small"}
                  onClick={() => setShowSearchForm((previous) => !previous)}
                >
                  Toggle Search Form
                </button>
              </div>
            )}
          </div>
        }
        rightButtons={[
          {
            label: "Cancel",
            onClick: onCancel,
            classes: "cancel-edit-program-button",
          },
          {
            label: "Save",
            onClick: onSave,
            classes: "is-link save-edit-program-button",
          },
        ]}
      />
      <div className="row-under-toolbar">
        <ProgramsEditFormRow program={programToEdit} />
      </div>
      <div className="columns is-marginless">
        {!showGrid && (
          <>
            <div className="column left-column is-narrow">
              <SearchForm store={programsDancesSearchStore} />
            </div>

            <div className="column middle-column">
              {programsDancesSearchStore.searchResultsMessages?.header && (
                <div className="dances-list-info">
                  {programsDancesSearchStore.searchResultsMessages.header}
                </div>
              )}
              {programsDancesSearchStore.dancesList && (
                <DancesTable
                  dances={programsDancesSearchStore.dancesList}
                  viewDance={uiStore.setModalDance.bind(uiStore)}
                  addDanceToProgram={programsStore.addDanceToProgram.bind(
                    programsStore,
                    programToEdit,
                  )}
                />
              )}
              {programsDancesSearchStore.searchResultsMessages?.criteria && (
                <div className="dances-list-info">
                  {programsDancesSearchStore.searchResultsMessages.criteria}
                </div>
              )}
              {programsDancesSearchStore.searchResultsMessages?.footer && (
                <div className="dances-list-info search-results-footer">
                  {programsDancesSearchStore.searchResultsMessages.footer}
                </div>
              )}
            </div>

            <div className="column right-column">
              {programsEditStore.programToEdit.dances && (
                <ProgramsDancesList
                  dances={programsEditStore.programToEdit.dances}
                  viewDance={programsDancesSearchStore.viewDance.bind(
                    programsDancesSearchStore,
                  )}
                  removeItemFromProgram={programsStore.removeItemFromProgram.bind(
                    programsStore,
                    programToEdit,
                  )}
                  moveDance={programsStore.moveDance.bind(
                    programsStore,
                    programToEdit,
                  )}
                />
              )}
            </div>
          </>
        )}

        {showGrid && (
          <>
            {showSearchForm && (
              <div className={"horizontal-search-form-panel"}>
                <SearchFormHorizontal store={dancesSearchStore} />
              </div>
            )}
            <div style={{ marginTop: showSearchForm ? "65px" : "0px" }}>
              {dancesSearchStore.dancesList && (
                <ProgramsDancesGrid
                  program={programToEdit}
                  moveDance={programsStore.moveDance.bind(
                    programsStore,
                    programToEdit,
                  )}
                  showProgramDanceFigures={showProgramDanceFigures}
                  showSearchResultsDanceFigures={showSearchResultsDanceFigures}
                  programDancesShowingFigures={programDancesShowingFigures}
                  searchResultsDancesShowingFigures={
                    searchResultsDancesShowingFigures
                  }
                  removeItemFromProgram={programsStore.removeItemFromProgram.bind(
                    programsStore,
                    programToEdit,
                  )}
                  addDanceToProgram={programsStore.addDanceToProgram.bind(
                    programsStore,
                    programToEdit,
                  )}
                  separatorText={
                    dancesSearchStore.searchResultsMessages?.header || ""
                  }
                  searchResultsDances={dancesSearchStore.dancesList}
                />
              )}
              {dancesSearchStore.searchResultsMessages?.criteria && (
                <div className="dances-list-info dances-list-info-grid">
                  {dancesSearchStore.searchResultsMessages.criteria}
                </div>
              )}
              {dancesSearchStore.searchResultsMessages?.footer && (
                <div className="dances-list-info search-results-footer">
                  {dancesSearchStore.searchResultsMessages.footer}
                </div>
              )}
            </div>
          </>
        )}
      </div>
    </>
  )
})
