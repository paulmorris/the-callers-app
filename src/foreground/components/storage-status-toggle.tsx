/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observer } from "mobx-react-lite"
import React, { FunctionComponent } from "react"

import { SearchResultsDance, Dance } from "../../shared/types"
import { useStores } from "../stores/stores-context"

interface StorageStatusToggleProps {
  dance: SearchResultsDance | Dance
  button: boolean
}

export const StorageStatusToggle: FunctionComponent<StorageStatusToggleProps> = observer(
  function StorageStatusToggle(props) {
    const { dancesEditStore } = useStores()
    const storageStatus = props.dance.storageStatus

    const onClick =
      storageStatus === "incoming"
        ? undefined
        : (event: React.MouseEvent): void => {
            event.preventDefault()
            event.stopPropagation()
            dancesEditStore.toggleStorageStatus(props.dance)
          }

    const className =
      "storage-status-toggle " +
      (storageStatus === "in"
        ? "is-in-toggle"
        : storageStatus === "incoming"
        ? "is-incoming-in-toggle"
        : storageStatus === "out-partial" || storageStatus === "out-full"
        ? "is-out-toggle"
        : "is-removed-toggle")

    // We don't differentiate between "out-partial", "out-full", and "removed"
    // here in the UI.
    const title =
      storageStatus === "in"
        ? "Click to remove this dance from the local collection."
        : storageStatus === "incoming"
        ? "Fetching from the Caller's Box..."
        : "Click to add this dance to the local collection."

    return props.button ? (
      <div className="control">
        <button className="button is-small" onClick={onClick} title={title}>
          <span className="icon">
            <a className={className} />
          </span>
        </button>
      </div>
    ) : (
      <a title={title} className={className} onClick={onClick} />
    )
  },
)
