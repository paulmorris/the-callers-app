/*
Copyright 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import { action, observable, makeObservable } from "mobx"
import R from "ramda"

import {
  isProgramSpacer,
  makeProgram,
  Program,
  ProgramItem,
  ProgramPatch,
  SearchResultsDance,
} from "../../shared/types"
import { toBackground } from "./inter-process-communication"
import { RootStore } from "./root-store"

export class ProgramsStore {
  rootStore: RootStore

  constructor(rootStore: RootStore) {
    makeObservable(this, {
      programs: observable,
      updateProgram: action.bound,
      deleteProgram: action.bound,
      setDancesOnProgram: action.bound,
      addDanceToProgram: action,
      addSpacerToProgram: action,
      removeItemFromProgram: action,
      moveDance: action,
    })

    this.rootStore = rootStore
  }

  // A map from programId to Program. Programs are updated here so we only ever
  // have one copy of a given program in memory.
  // TODO: once TypeScript supports WeakRef, use WeakRef to Program.
  programs: Map<number, Program> = new Map()

  updateProgram(newProgram: Program): Program {
    const id = newProgram.programId
    const oldProgram = this.programs.get(id)

    if (oldProgram) {
      Object.assign(oldProgram, R.omit(["dances"], newProgram))
    } else {
      this.programs.set(id, newProgram)
    }

    const program = this.programs.get(id)
    if (program && newProgram.dances) {
      this.setDancesOnProgram(program, newProgram.dances)
    }
    return program || newProgram
  }

  setDancesOnProgram(program: Program, dances: ProgramItem[]): Program {
    program.dances = dances.map((item) => {
      if (!isProgramSpacer(item)) {
        item.dance = this.rootStore.dancesStore.updateDance(item.dance)
      }
      return item
    })
    return program
  }

  async patchProgramWithDb(patch: ProgramPatch) {
    const program = await toBackground.updateProgram(patch)
    return this.updateProgram(program)
  }

  // TODO: needs to handle dance updates properly if we're going to use this.
  // patchProgram(patch: ProgramPatch): Program | undefined {
  //   const program = this.programs.get(patch.programId)
  //   if (program) {
  //     Object.assign(program, patch.changes)
  //   }
  //   return program
  // }

  async createProgram(): Promise<Program> {
    const programId = await toBackground.insertDefaultItem("Programs")
    return this.updateProgram(makeProgram(programId))
  }

  async deleteProgram(programId: number): Promise<boolean> {
    const result = await toBackground.deleteProgram(programId)
    if (result === 1) {
      return this.programs.delete(programId)
    }
    return false
  }

  async getProgram(programId: number): Promise<Program> {
    const program = await toBackground.getProgram(programId)
    return this.updateProgram(program)
  }

  async getAllPrograms(): Promise<Program[]> {
    const programs = await toBackground.getAllPrograms(null)
    return programs.map(this.updateProgram)
  }

  async searchPrograms(text: string): Promise<Program[]> {
    const programs = await toBackground.searchPrograms(text)
    return programs.map(this.updateProgram)
  }

  async addDanceToProgram(
    program: Program,
    dance: SearchResultsDance,
  ): Promise<void> {
    // TODO: UI unresponsiveness when adding a dance that needs to be fetched.
    const dances = await toBackground.addDanceToProgram({
      programId: program.programId,
      danceId: dance.danceId,
      tcbId: dance.tcbId,
    })
    this.setDancesOnProgram(program, dances)
  }

  async addSpacerToProgram(program: Program): Promise<void> {
    const dances = await toBackground.addSpacerToProgram({
      programId: program.programId,
    })
    this.setDancesOnProgram(program, dances)
  }

  async removeItemFromProgram(
    program: Program,
    programDanceId: number,
    danceId: number | undefined,
  ): Promise<void> {
    const { items, removedItem } = await toBackground.removeItemFromProgram({
      programId: program.programId,
      programDanceId,
      danceId,
    })
    if (removedItem) {
      this.rootStore.dancesStore.updateDance(removedItem)
    }
    this.setDancesOnProgram(program, items)
  }

  async moveDance(
    program: Program,
    newIndex: number,
    oldIndex: number,
  ): Promise<void> {
    const dances = await toBackground.moveDanceInProgram({
      programId: program.programId,
      newIndex,
      oldIndex,
    })
    this.setDancesOnProgram(program, dances)
  }
}
