/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import marked from "marked"
import { toJS, action, observable, makeObservable } from "mobx"
import R from "ramda"

import {
  Dance,
  SearchResultsDance,
  danceFromEditableDance,
  EditableDance,
  makeEditableDance,
  makeDance,
  isDance,
} from "../../shared/types"
import { DancePatch, isTcbBackupField } from "../../shared/types"
import { newline } from "../../shared/utils"
import { toBackground } from "./inter-process-communication"
import { RootStore } from "./root-store"

type FigureSuggestion = [string, number]

export class DancesEditStore {
  rootStore: RootStore

  danceBeforeEdit: Dance | undefined
  danceDuringEdit: EditableDance | undefined
  editingNewDance: boolean

  figureSuggestions: FigureSuggestion[]
  tcbFigureSuggestions: FigureSuggestion[]

  constructor(rootStore: RootStore) {
    makeObservable(this, {
      danceDuringEdit: observable,
      editingNewDance: observable,
      figureSuggestions: observable,
      tcbFigureSuggestions: observable,
      toggleStorageStatus: action,
      startEditingDance: action,
      createNewDance: action.bound,
      cancelEditDance: action.bound,
      cancelNewDance: action.bound,
      updateEditDanceForm: action,
      saveDanceEdits: action,
      setFigureSuggestions: action.bound,
      setTcbFigureSuggestions: action.bound,
    })

    this.rootStore = rootStore
    this.editingNewDance = false
    this.figureSuggestions = []
    this.tcbFigureSuggestions = []
  }

  /**
   * Toggle the storage status for a dance, whether its status is "in", "out",
   * or "removed". Should never be called for a dance with a status of
   * "incoming".
   */
  toggleStorageStatus(dance: Dance | SearchResultsDance): void {
    const oldStorageStatus = dance.storageStatus

    if (oldStorageStatus === "out-partial") {
      // Only show "incoming" color if the full dance needs to be fetched.
      dance.storageStatus = "incoming"
    }

    // (in -> removed) (out -> in) (removed -> in)
    const newStorageStatus = oldStorageStatus === "in" ? "removed" : "in"

    toBackground
      .updateStorageStatus({
        newStorageStatus,
        oldStorageStatus,
        danceId: dance.danceId,
        tcbId: dance.tcbId,
      })
      .then(this.rootStore.dancesStore.patchDance)
      .catch((payload) => {
        // Clears "incoming" status.
        this.rootStore.dancesStore.patchDance({
          danceId: dance.danceId,
          changes: { storageStatus: oldStorageStatus },
        })
        console.error(payload.error || payload)
      })
  }

  /**
   * Called when the user starts to edit a dance.
   */
  startEditingDance(dance: Dance, isNewDance: boolean = false): void {
    this.danceBeforeEdit = dance
    // If we deep clone the dance with `R.clone(dance)` it leads to an infinite
    // loop that exceeds the max call stack if the dance is in a program
    // (dance -> program -> dance -> program -> ...). So clone without the
    // programs, then set programs shallowly to avoid the death spiral.
    const partialClone = R.omit(["programs"], dance)
    const clone: Dance = { ...partialClone, programs: dance.programs }
    this.danceDuringEdit = makeEditableDance(clone)
    this.editingNewDance = isNewDance
    this.rootStore.uiStore.setView("editDance")
  }

  createNewDance(): void {
    toBackground
      .insertDefaultItem("Dances")
      .then((danceId) => {
        const dance = makeDance(danceId, "in")
        toBackground
          .updateExistingDance({ danceId, changes: dance })
          .then((dance) => {
            // TODO - is there a better way?
            if (dance && isDance(dance)) {
              this.startEditingDance(dance, true)
            }
          })
          .catch(console.error)
      })
      .catch(console.error)
  }

  cancelEditDance() {
    this.danceBeforeEdit = undefined
    this.danceDuringEdit = undefined
    this.editingNewDance = false
    this.rootStore.uiStore.restoreView()
  }

  async cancelNewDance() {
    // TS placation.
    if (!this.danceDuringEdit) {
      throw new Error("Missing danceDuringEdit on cancel create new dance")
    }
    toBackground
      .deleteItem({
        tableName: "Dances",
        idColumn: "danceId",
        id: this.danceDuringEdit.danceId,
      })
      .then(this.cancelEditDance)
  }

  updateEditDanceForm(name: keyof EditableDance, value: string): void {
    // TS placation.
    if (!this.danceDuringEdit) {
      throw new Error("Missing danceDuringEdit on editing a dance")
    }
    switch (name) {
      // "Editable" properties.
      case "editableFigures":
      case "editableTcbFigures":
      case "editableTags":
      case "editableAuthors":
      case "editableCallingNotes":
      case "editableOtherTitles":
      case "editableBasedOn":
      case "editableMusic":
      case "editableAppearances":
      case "editableVideos":
      case "editableVariantVideos":
      // Regular properties.
      case "title":
      case "formationBase":
      case "formationDetail":
      case "progression":
      case "direction":
      case "phraseStructure":
        this.danceDuringEdit[name] = value
        break
      case "mixer":
        // TS placation.
        if (value === "yes" || value === "") {
          this.danceDuringEdit[name] = value
        }
        break
      case "status":
        // TS placation.
        if (value === "deprecated" || value === "broken" || value === "none") {
          this.danceDuringEdit[name] = value
        }
        break
    }
  }

  saveDanceEdits() {
    // TS placation.
    if (!this.danceDuringEdit || !this.danceBeforeEdit) {
      throw new Error("Missing danceDuringEdit or danceBeforeEdit")
    }
    const oldDance = toJS(this.danceBeforeEdit)
    const newDance = danceFromEditableDance(this.danceDuringEdit)
    const changes = getAllDanceChanges(oldDance, newDance)

    // TODO - this works for saving attribute changes, but is not pretty.
    changes.attributes = toJS(newDance.attributes)

    if (R.equals(changes, {})) {
      // Nothing to change.
      this.rootStore.uiStore.restoreView()
    } else {
      // The changes have accurate hasLink, hasVideo, etc.
      this.updateExistingDance({ danceId: oldDance.danceId, changes })
      this.rootStore.dancesSearchStore.redoLocalSearch()
    }
    this.danceBeforeEdit = undefined
    this.danceDuringEdit = undefined
    this.editingNewDance = false
  }

  searchFigures(query: string): void {
    toBackground.searchFigures(query).then(this.setFigureSuggestions)
  }

  searchTcbFigures(query: string): void {
    toBackground.searchTcbFigures(query).then(this.setTcbFigureSuggestions)
  }

  setFigureSuggestions(suggestions: FigureSuggestion[]): void {
    this.figureSuggestions = suggestions
  }

  setTcbFigureSuggestions(suggestions: FigureSuggestion[]): void {
    this.tcbFigureSuggestions = suggestions
  }

  updateExistingDance(patch: DancePatch): void {
    toBackground
      .updateExistingDance(patch)
      .then((dance) => {
        // TODO - a better way?
        if (dance && isDance(dance)) {
          const storedDance = this.rootStore.dancesStore.updateDance(dance)
          this.rootStore.dancesSearchStore.setDanceToView(storedDance)
        }
      })
      .catch(() => {})
      .finally(() => {
        this.rootStore.uiStore.restoreView()
      })
  }
}

const getAllDanceChanges = (
  oldDance: Dance,
  newDance: Dance,
): Partial<Dance> => {
  const changes = getSimpleDanceChanges(oldDance, newDance)

  // Only TCB dances have a tcbBackup property.
  if (oldDance.tcbId) {
    changes.tcbBackup = getNewTcbBackup(oldDance, changes)
  }

  // Add changes to "has properties" like hasLink etc.
  return Object.assign(changes, getHasPropertyChanges(changes))
}

/**
 * Returns the properties of a dance that have changed.
 *
 * @param oldDance - The older version of a dance.
 * @param newDance - The newer version of a dance.
 * @return - Just the properties from the newer version that are different.
 */
const getSimpleDanceChanges = (
  oldDance: Dance,
  newDance: Dance,
): Partial<Dance> => {
  const propHasChanged = (value: any, key: string): boolean =>
    !R.equals(
      // TS placation.
      newDance[key as keyof Dance],
      oldDance[key as keyof Dance],
    )

  const changes: Partial<Dance> = R.pickBy(propHasChanged, newDance)

  return changes
}

/**
 * Calculate the new tcbBackup property to set on a dance that is being changed.
 *
 * @param dance - Current version of a dance.
 * @param changes - Changes that will be made to the dance.
 * @return - The new tcbBackup property that should be set on the dance.
 */
const getNewTcbBackup = (
  dance: Dance,
  changes: Partial<Dance>,
): Partial<Dance> | undefined => {
  const changesKeys = Object.keys(changes).filter(isTcbBackupField)

  const oldBackup = dance.tcbBackup || {}
  const newBackup = R.clone(oldBackup)

  for (const key of changesKeys) {
    if (!(key in oldBackup)) {
      // Add to new backup.
      ;(newBackup[key] as any) = dance[key]
    } else if (R.equals(oldBackup[key], changes[key])) {
      // Omit from new backup.
      delete newBackup[key]
    }
  }
  return Object.keys(newBackup).length ? newBackup : undefined
}

type HasProperties = {
  hasFigures?: boolean
  hasLink?: boolean
  hasVideo?: boolean
}

/**
 * For some given changes to a dance, get the 'has' meta properties like
 * hasFigures, hasLink, and hasVideo that might also change. Detecting links in
 * markdown is not as simple as it looks, so convert to HTML to make detecting
 * the links more foolproof.
 *
 * @param changes - Changes to be made to a dance.
 * @return - An object with these 'has' boolean properties.
 */
const getHasPropertyChanges = (changes: Partial<Dance>): HasProperties => {
  const linkRegex = /<\/a>/
  const hasProps: HasProperties = {}

  if ("appearances" in changes) {
    hasProps.hasLink = changes.appearances?.length
      ? linkRegex.test(marked(changes.appearances.join(newline)))
      : false
  }

  if ("videos" in changes) {
    hasProps.hasVideo = changes.videos?.length
      ? linkRegex.test(marked(changes.videos.join(newline)))
      : false
  }

  // If hasVideos is already true then variantVideos does not matter.
  if (!hasProps.hasVideo && "variantVideos" in changes) {
    hasProps.hasVideo = changes.variantVideos?.length
      ? linkRegex.test(marked(changes.variantVideos.join(newline)))
      : false
  }

  if ("figures" in changes || "tcbFigures" in changes) {
    hasProps.hasFigures = Boolean(
      changes.figures?.length || changes.tcbFigures?.length,
    )
  }

  return hasProps
}
