/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import { ipcRenderer } from "electron"
import React, { FunctionComponent } from "react"

import { dbClearDancesTable } from "../../background/database/sqlite/dances"
import { dbExport } from "../../background/database/sqlite/database"
import { isDance, makeDance } from "../../shared/types"
import {
  createMessageToForegroundListener,
  toBackground,
} from "./inter-process-communication"
import { RootStore } from "./root-store"

const storesContext = React.createContext<RootStore | null>(null)

/**
 * React context provider component that provides store access to children.
 */
export const StoreProvider: FunctionComponent<{ children: React.ReactNode }> = (
  props,
) => {
  const rootStore = new RootStore()

  // Set up IPC channel. The 'if' check is needed for unit tests.
  if (ipcRenderer) {
    ipcRenderer.on(
      "message-to-foreground",
      createMessageToForegroundListener(rootStore),
    )
  }

  // Now that IPC is set up, initialize the search results to show all
  // collected dances on startup.
  rootStore.dancesSearchStore.doLocalSearch()

  setUpDocumentKeyDownListener(rootStore)

  return (
    <storesContext.Provider value={rootStore}>
      {props.children}
    </storesContext.Provider>
  )
}

/**
 * React hook that provides access to stores from within a given component.
 */
export const useStores = (): RootStore => {
  const stores = React.useContext(storesContext)
  if (!stores) {
    throw new Error("useStores must be used within a StoreProvider.")
  }
  return stores
}

/**
 * Add keydown listeners to the document, done here for access to stores.
 * Most of this is just temporary, for development purposes.
 */
const setUpDocumentKeyDownListener = (rootStore: RootStore) => {
  document.addEventListener("keydown", (event: KeyboardEvent) => {
    if (event.ctrlKey && event.shiftKey && event.altKey) {
      if (event.key === "E") {
        // Export the database.
        dbExport()
      } else if (event.key === "D") {
        // Delete the database.
        dbClearDancesTable()
        console.log("Dances table deleted and recreated.")
      } else if (event.key === "P") {
        // Populate the database with some dances fetched from TCB.
        function fetchByIds(idsToFetch: string[]): void {
          if (idsToFetch.length > 0) {
            const tcbId = idsToFetch[0]
            console.log(`Fetching tcbId: ${tcbId}`)

            toBackground
              .insertDefaultItem("Dances")
              .then((danceId) => {
                const dance = makeDance(danceId, "out-partial")
                dance.tcbId = tcbId
                toBackground
                  .updateExistingDance({
                    danceId,
                    changes: dance,
                  })
                  .then((dance2) => {
                    // TODO - a better way?
                    if (dance2 && isDance(dance2)) {
                      rootStore.dancesEditStore.toggleStorageStatus(dance2)
                    }
                  })
                  .catch(() => {})
              })
              .catch(() => {})

            setTimeout(() => fetchByIds(idsToFetch.slice(1)), 500)
            setTimeout(() => rootStore.dancesSearchStore.doLocalSearch(), 1000)
          } else {
            console.log("Done fetching dances")
            return
          }
        }
        console.log("Fetching dances")
        fetchByIds([
          "2058",
          "4836",
          "7308",
          "2826",
          "10263",
          "10305",
          "74",
          "1",
          "7876",
          "133",
          "10382",
        ])
      }
    }
  })
}
