/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import { ipcRenderer, IpcRendererEvent } from "electron"
import { action } from "mobx"

import { ToBackground } from "../../background/background"
import { CommandToMain } from "../../main"
import { RootStore } from "./root-store"

/**
 * Send a message to the background, create a promise and store its resolve and
 * reject functions, using a unique ID, to be able to call them when a response
 * comes back from the background with that ID.
 *
 * @param command
 * @param payload
 * @return Promise with the payload from the backend.
 */
async function send(command: keyof ToBackground, payload: any): Promise<any> {
  const ipcId = getNextIpcId()

  const promise = new Promise((resolve, reject) => {
    ipcPromiseSettlers.set(ipcId, { resolve, reject })
  })

  ipcRenderer.send("message-to-background", {
    command,
    payload,
    ipcId,
  })

  return promise
}

/**
 * Foreground instantiation of message passing handler functions, in parallel
 * with the same functions in the background.
 */
export const toBackground: ToBackground = {
  addDanceToProgram: send.bind(null, "addDanceToProgram"),
  addSpacerToProgram: send.bind(null, "addSpacerToProgram"),
  deleteItem: send.bind(null, "deleteItem"),
  deleteProgram: send.bind(null, "deleteProgram"),
  fetchTcbDanceByTcbId: send.bind(null, "fetchTcbDanceByTcbId"),
  getAllPrograms: send.bind(null, "getAllPrograms"),
  getDanceById: send.bind(null, "getDanceById"),
  getDancesForProgramId: send.bind(null, "getDancesForProgramId"),
  getProgram: send.bind(null, "getProgram"),
  getSettings: send.bind(null, "getSettings"),
  insertDefaultItem: send.bind(null, "insertDefaultItem"),
  moveDanceInProgram: send.bind(null, "moveDanceInProgram"),
  removeItemFromProgram: send.bind(null, "removeItemFromProgram"),
  searchCallersBoxDances: send.bind(null, "searchCallersBoxDances"),
  searchFigures: send.bind(null, "searchFigures"),
  searchLocalDances: send.bind(null, "searchLocalDances"),
  searchPrograms: send.bind(null, "searchPrograms"),
  searchTcbFigures: send.bind(null, "searchTcbFigures"),
  setDanceAttributes: send.bind(null, "setDanceAttributes"),
  updateExistingDance: send.bind(null, "updateExistingDance"),
  updateProgram: send.bind(null, "updateProgram"),
  updateStorageStatus: send.bind(null, "updateStorageStatus"),
}

/**
 * A map where resolve and reject functions for promises are stored.
 */
const ipcPromiseSettlers: Map<
  number,
  { resolve: Function; reject: Function }
> = new Map()

const getNextIpcId = (() => {
  let counter = 0
  return () => {
    counter += 1
    if (counter === 9007199254740991) {
      // Maximum JS integer value reached. (!)
      counter = 1
    }
    return counter
  }
})()

export function sendToMain(command: CommandToMain, payload: any): void {
  ipcRenderer.send("message-to-main", { command, payload })
}

export type CommandToForeground =
  | "resolveIpcPromise"
  | "rejectIpcPromise"
  | "setView"
  | "createNewDance"
  | "createNewProgram"
  | "startLocalSearch"

/**
 * Returns a listener function to pass to Electron's `ipcRenderer.on()`.
 */
export const createMessageToForegroundListener = (rootStore: RootStore) =>
  action(
    (
      event: IpcRendererEvent,
      message: { command: CommandToForeground; ipcId: number; payload: any },
    ): void => {
      const { command, ipcId, payload } = message

      switch (command) {
        case "resolveIpcPromise": {
          ipcPromiseSettlers.get(ipcId)?.resolve(payload)
          ipcPromiseSettlers.delete(ipcId)
          break
        }
        case "rejectIpcPromise": {
          console.error(payload)
          ipcPromiseSettlers.get(ipcId)?.reject(payload)
          ipcPromiseSettlers.delete(ipcId)
          break
        }
        case "setView": {
          // Can be sent from main.
          rootStore.uiStore.setView(payload)
          break
        }
        case "createNewDance": {
          // Can be sent from main.
          rootStore.dancesEditStore.createNewDance()
          break
        }
        case "createNewProgram": {
          rootStore.programsBrowseStore.createProgram()
          break
        }
        case "startLocalSearch": {
          // Can be sent from main.
          rootStore.dancesSearchStore.doLocalSearch()
          break
        }
        default: {
          console.error("Unexpected message:", command, payload)
        }
      }
    },
  )
