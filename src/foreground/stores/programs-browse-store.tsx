/*
Copyright 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observable, action, makeObservable, toJS, runInAction } from "mobx"

import { Program } from "../../shared/types"
import { RootStore } from "./root-store"

export class ProgramsBrowseStore {
  rootStore: RootStore

  programsList: Program[] = []
  programToView: Program | undefined
  programsSearchInput: string = ""

  constructor(rootStore: RootStore) {
    makeObservable(this, {
      programsList: observable,
      programToView: observable,
      programsSearchInput: observable,
      createProgram: action,
      deleteProgram: action,
      viewProgram: action,
      setProgramToView: action,
      editProgram: action,
      finishEditProgram: action,
      setProgramsList: action.bound,
      deleteProgramFromList: action.bound,
      addProgramToList: action.bound,
      updateSearchInput: action.bound,
      searchPrograms: action.bound,
    })

    this.rootStore = rootStore
    this.initialize()
  }

  async initialize() {
    // Populate the programs list and view the first program in the list.
    // TODO: maybe this doesn't need to be done at construction.
    await this.getAllPrograms()
    const programId = this.programsList[0].programId
    const program = await this.rootStore.programsStore.getProgram(programId)
    this.setProgramToView(program)
  }

  async getAllPrograms(): Promise<void> {
    const programs = await this.rootStore.programsStore.getAllPrograms()
    this.setProgramsList(programs)
  }

  async searchPrograms(text: string) {
    const programs = await this.rootStore.programsStore.searchPrograms(text)
    this.setProgramsList(programs)
  }

  async viewProgram(programId: number): Promise<void> {
    const program = await this.rootStore.programsStore.getProgram(programId)
    this.setProgramToView(program)
    this.rootStore.uiStore.setView("programsBrowse")
  }

  setProgramToView(program: Program | undefined) {
    this.programToView = program
  }

  async createProgram(): Promise<void> {
    const program = await this.rootStore.programsStore.createProgram()
    runInAction(() => {
      this.addProgramToList(program)
      this.rootStore.programsEditStore.editingNewProgram = true
      this.rootStore.programsEditStore.setProgramToEdit(program)
      this.finishEditProgram()
    })
  }

  editProgram(program: Program): void {
    this.rootStore.programsEditStore.editingNewProgram = false
    this.rootStore.programsEditStore.setProgramToEdit(program)

    // If we ever want to edit a program without viewing it first, we
    // have to get the dances for it at this point.

    this.rootStore.programsEditStore.programBeforeEdit = toJS(program)
    this.finishEditProgram()
  }

  finishEditProgram(): void {
    this.rootStore.programsDancesSearchStore.doSearch()
    this.rootStore.uiStore.setView("programsEdit")
  }

  async deleteProgram(programId: number): Promise<void> {
    await this.rootStore.programsStore.deleteProgram(programId)
    this.deleteProgramFromList(programId)
    this.setProgramToView(undefined)
  }

  setProgramsList(programs: Program[]): void {
    this.programsList = programs
  }

  deleteProgramFromList(programId: number) {
    const index = this.programsList.findIndex((p) => p.programId === programId)
    if (index !== -1) {
      this.programsList.splice(index, 1)
    }
  }

  addProgramToList(program: Program) {
    this.programsList.unshift(program)
    return this.programsList[0]
  }

  updateSearchInput(text: string) {
    this.programsSearchInput = text
    this.searchPrograms(text)
  }
}
