/*
Copyright 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import { observable, action, makeObservable, runInAction } from "mobx"

import { Program } from "../../shared/types"
import { toBackground } from "./inter-process-communication"
import { RootStore } from "./root-store"

export class ProgramsEditStore {
  rootStore: RootStore

  constructor(rootStore: RootStore) {
    makeObservable(this, {
      programToEdit: observable,
      editingNewProgram: observable,
      setProgramToEdit: action,
      changeProgram: action,
      cancelEditProgram: action,
    })

    this.rootStore = rootStore
  }

  programToEdit: Program
  editingNewProgram: boolean

  // Non-observable deep clone of programToEdit, for reverting changes on cancel.
  programBeforeEdit: Program

  setProgramToEdit(program: Program): void {
    this.programToEdit = program
  }

  changeProgram(name: keyof Program, value: string): void {
    switch (name) {
      case "date":
      case "location":
      case "music":
      case "notes":
        this.programToEdit[name] = value
        const patch = {
          programId: this.programToEdit.programId,
          changes: { [name]: value },
        }
        toBackground.updateProgram(patch)
        break
    }
  }

  async cancelNewProgram(): Promise<void> {
    const programId = this.programToEdit.programId
    await this.rootStore.programsStore.deleteProgram(programId)
    runInAction(() => {
      this.rootStore.programsBrowseStore.deleteProgramFromList(programId)
      this.rootStore.uiStore.restoreView()
    })
  }

  async cancelEditProgram(): Promise<void> {
    await this.rootStore.programsStore.patchProgramWithDb({
      programId: this.programBeforeEdit.programId,
      changes: this.programBeforeEdit,
    })
    this.rootStore.uiStore.restoreView()
  }
}
