/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import { configure } from "mobx"

import { DancesEditStore } from "./dances-edit-store"
import { ProgramsEditStore } from "./programs-edit-store"
import { ProgramsBrowseStore } from "./programs-browse-store"
import { DancesSearchStore } from "./dances-search-store"
import { UiStore } from "./ui-store"
import { DancesStore } from "./dances-store"
import { ProgramsStore } from "./programs-store"
import { DanceAttribute, DanceAttributesStore } from "./dance-attributes-store"
import { toBackground } from "./inter-process-communication"

// MobX configuration.
configure({
  enforceActions: "observed",
  computedRequiresReaction: true,
  reactionRequiresObservable: true,
  observableRequiresReaction: true,
  disableErrorBoundaries: true,
})

export class RootStore {
  uiStore: UiStore = new UiStore(this)
  dancesStore: DancesStore = new DancesStore(this)
  dancesSearchStore: DancesSearchStore = new DancesSearchStore(
    this,
    "local_tcb",
  )
  dancesEditStore: DancesEditStore = new DancesEditStore(this)
  programsStore: ProgramsStore = new ProgramsStore(this)
  programsBrowseStore = new ProgramsBrowseStore(this)
  programsEditStore = new ProgramsEditStore(this)
  programsDancesSearchStore: DancesSearchStore = new DancesSearchStore(
    this,
    "local",
  )

  danceAttributesStore: DanceAttributesStore

  constructor() {
    this.initializeSettings()
  }

  async initializeSettings() {
    const settings = await toBackground.getSettings()
    const attributes: DanceAttribute[] = JSON.parse(settings.danceAttributes)

    this.danceAttributesStore = new DanceAttributesStore(
      this,
      attributes,
      false,
    )
  }
}
