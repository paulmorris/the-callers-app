/*
Copyright 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import { action, observable, makeObservable } from "mobx"
import R from "ramda"

import {
  Dance,
  DancePatch,
  isDance,
  LocalSearchResults,
  Program,
  SearchQuery,
  SearchResults,
  SearchResultsDance,
  SearchResultsState,
} from "../../shared/types"
import { toBackground } from "./inter-process-communication"
import { RootStore } from "./root-store"

export class DancesStore {
  rootStore: RootStore

  constructor(rootStore: RootStore) {
    makeObservable(this, {
      dances: observable,
      updateDance: action.bound,
      setProgramsOnDance: action.bound,
      patchDance: action.bound,
    })

    this.rootStore = rootStore
  }

  // A map from danceId to Dance. Dances are updated here so we only ever have
  // one copy of a given dance in memory.
  // TODO: once TypeScript supports WeakRef, use WeakRef to Dance.
  dances: Map<number, Dance | SearchResultsDance> = new Map()

  /**
   * Adds a dance to, or updates a dance in, the stored dances Map.
   * @param newDance
   * @return The stored dance, now updated.
   */
  updateDance(newDance: Dance): Dance
  updateDance(newDance: SearchResultsDance): SearchResultsDance
  updateDance(
    newDance: Dance | SearchResultsDance,
  ): Dance | SearchResultsDance {
    const id = newDance.danceId
    const oldDance = this.dances.get(id)

    if (oldDance) {
      Object.assign(oldDance, R.omit(["programs"], newDance))
    } else {
      this.dances.set(id, newDance)
    }

    const dance = this.dances.get(id)
    // TS placation.
    // Contra TS, newDance.programs can be undefined, so we check for that.
    if (dance && isDance(dance) && isDance(newDance) && newDance.programs) {
      this.setProgramsOnDance(dance, newDance.programs)
    }

    return dance || newDance
  }

  patchDance(patch: DancePatch): Dance | SearchResultsDance | undefined {
    const dance = this.dances.get(patch.danceId)
    if (dance) {
      Object.assign(dance, patch.changes)
    }
    return dance
  }

  setProgramsOnDance(dance: Dance, programs: Program[]): Dance {
    dance.programs = programs.map((program) => {
      return this.rootStore.programsStore.updateProgram(program)
    })
    return dance
  }

  searchLocalDances(payload: {
    query: SearchQuery
    state: SearchResultsState
  }): Promise<LocalSearchResults> {
    return toBackground.searchLocalDances(payload).then((results) => {
      results.localDances = results.localDances.map(this.updateDance)
      return results
    })
  }

  // TODO: do we run the risk of overwriting local dances with TCB dances
  // where the local version has changes the user made to the TCB dance?
  searchCallersBoxDances(payload: {
    url: string
    state: SearchResultsState
  }): Promise<Partial<SearchResults>> {
    return toBackground.searchCallersBoxDances(payload).then((results) => {
      results.tcbDances = results.tcbDances?.map(this.updateDance)
      return results
    })
  }

  fetchTcbDanceByTcbId(payload: {
    danceId: number
    tcbId: string
  }): Promise<Dance> {
    return toBackground
      .fetchTcbDanceByTcbId(payload)
      .then((dance) => this.updateDance(dance))
  }

  getDanceById(payload: {
    danceId: number
  }): Promise<Dance | SearchResultsDance | undefined> {
    return toBackground
      .getDanceById(payload)
      .then((dance) => (dance ? this.updateDance(dance) : dance))
  }
}
