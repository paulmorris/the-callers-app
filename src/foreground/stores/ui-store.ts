/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import { action, observable, reaction, makeObservable } from "mobx"

import { Dance, SearchResultsDance, View } from "../../shared/types"
import { RootStore } from "./root-store"
import { sendToMain } from "./inter-process-communication"

const viewsToRestoreFrom: View[] = [
  "editDance",
  "programsEdit",
  "adminAddDanceAttribute",
]

export class UiStore {
  rootStore: RootStore

  view: View = "searchDances"
  viewToRestore: View = "searchDances"

  modalDance: Dance | SearchResultsDance | undefined

  constructor(rootStore: RootStore) {
    makeObservable(this, {
      view: observable,
      setView: action,
      restoreView: action,
      modalDance: observable,
      simplySetModalDance: action,
      setModalDance: action,
    })

    this.rootStore = rootStore

    /**
     * Whenever the view changes:
     * Notify the main process when the view changes, so its UI can be updated.
     * Clear any modal that might be in effect.
     * Possibly update the viewToRestore.
     */
    reaction(
      () => this.view,
      (view, previousView) => {
        this.modalDance = undefined

        sendToMain("viewChanged", view)

        if (viewsToRestoreFrom.includes(view)) {
          this.viewToRestore = previousView
        }
      },
    )
  }

  setView(view: View): void {
    this.view = view
  }

  simplySetModalDance(dance: Dance | SearchResultsDance | undefined): void {
    this.modalDance = dance
  }

  setModalDance(dance: Dance | SearchResultsDance | undefined): void {
    this.simplySetModalDance(dance)
    if (!dance) {
      return
    }
    const { danceId, tcbId } = dance

    // If necessary fetch the full dance record from the Caller's Box.
    // TS Placation, out-partial dances should always have a tcbId.
    if (dance.storageStatus === "out-partial" && tcbId) {
      this.rootStore.dancesStore
        .fetchTcbDanceByTcbId({ danceId, tcbId })
        .then(this.simplySetModalDance)
    } else {
      // TODO: Could check first whether we already have a full dance record.
      this.rootStore.dancesStore.getDanceById({ danceId }).then((gotDance) => {
        if (gotDance) {
          this.simplySetModalDance(gotDance)
        }
      })
    }
  }

  restoreView() {
    this.view = this.viewToRestore
  }
}
