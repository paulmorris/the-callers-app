/*
Copyright 2019, 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import R from "ramda"
import { observable, computed, action, makeObservable } from "mobx"

import {
  isProgressionOption,
  SearchInput,
  SearchQuery,
  makeSearchInput,
  makeSearchQuery,
  SearchResultsState,
  SearchResults,
  SearchResultsDance,
  SearchResultsMessages,
  FiguresSearchMode,
  SearchType,
  DanceToView,
} from "../../shared/types"
import { tcbBaseUrl, tokenize, splitAtNewlines } from "../../shared/utils"
import { RootStore } from "./root-store"

export class DancesSearchStore {
  rootStore: RootStore

  searchResults: SearchResults | undefined
  searchInput: SearchInput = makeSearchInput()
  danceToView: DanceToView | undefined

  constructor(rootStore: RootStore, searchType: SearchType) {
    makeObservable(this, {
      searchResults: observable,
      searchInput: observable,
      danceToView: observable,
      dancesList: computed,
      searchResultsMessages: computed,
      viewDance: action,
      setDanceToView: action.bound,
      updateSearchResults: action.bound,
      updateSearchForm: action,
      doSearch: action,
      doLocalSearch: action,
      doCallersBoxSearch: action,
      doCombinedSearch: action,
      redoLocalSearch: action,
      clearSearchResults: action,
      redoCurrentSearch: action,
    })

    this.rootStore = rootStore
    this.updateSearchForm("search_type", searchType)
  }

  get dancesList(): SearchResultsDance[] {
    if (!this.searchResults) {
      return []
    }
    const localDances = this.searchResults.localDances
    const tcbDances = this.searchResults.tcbDances

    if (localDances.length && !tcbDances.length) {
      return localDances
    } else if (!localDances.length && tcbDances.length) {
      return tcbDances
    } else {
      // Combined Search
      let localTcbIds = new Set()
      for (const dance of localDances) {
        if (dance.tcbId) {
          localTcbIds.add(dance.tcbId)
        }
      }

      const tcbDancesShown = tcbDances.filter(
        (dance) => !localTcbIds.has(dance.tcbId!),
      )

      return [...localDances, ...tcbDancesShown]
    }
  }

  get searchResultsMessages(): SearchResultsMessages | undefined {
    if (!this.searchResults) {
      return undefined
    }
    const localCount = this.searchResults.localDances.length
    const tcbCount = this.searchResults.tcbDances.length

    const {
      state,
      totalTcbDances,
      totalLocalDances,
      criteria,
      tcbDatabaseUpdated,
      tcbSiteCodeUpdated,
    } = this.searchResults

    switch (state) {
      case "callersBoxSearching":
        return {
          header: "Searching the Caller's Box...",
        }
      case "callersBox":
        return {
          header: `${tcbCount} of ${totalTcbDances} dances in the Caller's Box.`,
          criteria: `Search criteria: ${criteria}`,
          footer:
            `The Caller's Box database was updated on ${tcbDatabaseUpdated} ` +
            `and the site code was updated on ${tcbSiteCodeUpdated}.`,
        }
      case "combinedStillSearching":
        return {
          header:
            `${localCount} of ${totalLocalDances} dances in the local collection.` +
            " Searching the Caller's Box...",
        }
      case "combined":
        return {
          header:
            `${localCount} of ${totalLocalDances} dances ` +
            `in the local collection and ` +
            `${tcbCount} more of ${totalTcbDances} in the Caller's Box ` +
            `(${localCount + tcbCount} total).`,
          criteria: `Search criteria: ${criteria}`,
          footer:
            `The Caller's Box database was updated on ${tcbDatabaseUpdated} ` +
            `and the site code was updated on ${tcbSiteCodeUpdated}.`,
        }
      case "local":
        return {
          header: `${localCount} of ${totalLocalDances} dances in the local collection.`,
        }
      default:
        return {}
    }
  }

  viewDance(dance: SearchResultsDance): void {
    this.danceToView = dance

    if (dance.storageStatus === "out-partial") {
      // TS Placation, out-partial dances should always have a tcbId.
      if (dance.tcbId) {
        this.rootStore.dancesStore
          .fetchTcbDanceByTcbId({ danceId: dance.danceId, tcbId: dance.tcbId })
          .then(this.setDanceToView)
      }
    } else {
      this.rootStore.dancesStore
        .getDanceById({ danceId: dance.danceId })
        .then((dance) => {
          if (dance) {
            this.setDanceToView(dance)
          }
        })
    }
  }

  setDanceToView(dance: DanceToView | SearchResultsDance): void {
    this.danceToView = dance
  }

  updateSearchResults(results: Partial<SearchResults>): void {
    this.searchResults = Object.assign(this.searchResults, results)
  }

  /**
   * Handles user input in the search form by updating the search form state.
   * Local search is done/refreshed as the user types or otherwise modifies the
   * query.
   */
  updateSearchForm(name: keyof SearchInput, value: string): void {
    if (name === "pos_mode" || name === "neg_mode") {
      this.searchInput[name] = value as FiguresSearchMode
    } else if (name === "search_type") {
      this.searchInput[name] = value as SearchType
    } else if (name === "progression") {
      if (isProgressionOption(value)) {
        this.searchInput.progression = value
      }
    } else {
      this.searchInput[name] = value
    }

    if (this.searchInput.search_type === "local") {
      this.doLocalSearch(this.searchInput)
    }
  }

  /**
   * Handles user click of search button, starts a search.
   */
  doSearch() {
    switch (this.searchInput.search_type) {
      case "local":
        this.doLocalSearch(this.searchInput)
        break
      case "tcb":
        this.doCallersBoxSearch(this.searchInput)
        break
      case "local_tcb":
      default:
        this.doCombinedSearch(this.searchInput)
        break
    }
  }

  /**
   * Prepare user input and perform a search of the local dance database.
   */
  doLocalSearch(
    searchInput: SearchInput = makeSearchInput({ search_type: "local" }),
  ): void {
    this.clearSearchResults("local", searchInput)

    this.rootStore.dancesStore
      .searchLocalDances({
        query: getLocalSearchQuery(searchInput),
        state: "local",
      })
      .then(this.updateSearchResults)
  }

  /**
   * Prepare user input and perform a search of the Caller's Box.
   */
  doCallersBoxSearch(searchInput: SearchInput): void {
    this.clearSearchResults("callersBoxSearching", searchInput)

    this.rootStore.dancesStore
      .searchCallersBoxDances({
        url: getTcbUrlForSearch(searchInput),
        state: "callersBox",
      })
      .then(this.updateSearchResults)
  }

  /**
   * Do a combined search of the local dance database and the Caller's Box.
   */
  doCombinedSearch(searchInput: SearchInput): void {
    this.clearSearchResults("callersBoxSearching", searchInput)

    this.rootStore.dancesStore
      .searchLocalDances({
        query: getLocalSearchQuery(searchInput),
        state: "combinedStillSearching",
      })
      .then(this.updateSearchResults)

    this.rootStore.dancesStore
      .searchCallersBoxDances({
        url: getTcbUrlForSearch(searchInput),
        state: "combined",
      })
      .then(this.updateSearchResults)
  }

  /**
   * If the current search is local, redo the whole search. If it is combined,
   * preserve the Caller's Box results, and just redo the local part.
   */
  redoLocalSearch(): void {
    if (this.searchResults) {
      switch (this.searchResults.state) {
        case "local": {
          this.doLocalSearch(this.searchResults.searchInput)
          break
        }
        case "combined": {
          // We don't clear the results in the combined case, just update them.
          this.rootStore.dancesStore
            .searchLocalDances({
              query: getLocalSearchQuery(this.searchResults.searchInput),
              state: "local",
            })
            .then(this.updateSearchResults)
          break
        }
      }
    }
  }

  clearSearchResults(
    state: SearchResultsState,
    searchInput: SearchInput,
  ): void {
    this.searchResults = {
      state,
      searchInput,
      localDances: [],
      tcbDances: [],
      totalLocalDances: 0,
      totalTcbDances: 0,
    }
  }

  redoCurrentSearch = (): void => {
    if (this.searchResults) {
      switch (this.searchResults.state) {
        case "local":
          this.doLocalSearch(this.searchResults.searchInput)
          break
        case "callersBox":
        case "callersBoxSearching":
          this.doCallersBoxSearch(this.searchResults.searchInput)
          break
        case "combined":
        default:
          this.doCombinedSearch(this.searchResults.searchInput)
          break
      }
    }
  }
}

/**
 * Used to identify strings that are not just whitespace or empty strings.
 *
 * @param string - The string to check.
 * @return False for only whitespace or an empty string, else true.
 */
const isNotWhiteSpace = (string: string) => !RegExp(/^\s*$/).test(string)

/**
 * Prepare figures search term input. Each line of text becomes an item in the
 * returned array, whitespace is dealt with, etc.
 *
 * @param searchLines - User input from figures text area.
 */
const parseSearchlines = (searchLines: string): string[] | undefined => {
  const array = splitAtNewlines(searchLines).filter(isNotWhiteSpace)

  return array.length ? array : undefined
}

/**
 * Convert input from the search form into a search query object. Convert any
 * properties that need it.
 *
 * @param input - Input from the search form.
 * @return A query object.
 */
const getLocalSearchQuery = (input: SearchInput): SearchQuery => {
  const query: Partial<SearchQuery> = R.pick(
    [
      "title",
      "authors",
      "formationBase",
      "progression",
      "pos_mode",
      "neg_mode",
    ],
    input,
  )

  if (input.pos_lines) query.pos_lines = parseSearchlines(input.pos_lines)
  if (input.neg_lines) query.neg_lines = parseSearchlines(input.neg_lines)
  if (input.tags) query.tags = tokenize(input.tags)

  return makeSearchQuery(query)
}

const getTcbUrlForSearch = (searchInput: SearchInput): string => {
  const tcbParamData: Record<string, string> = R.omit(
    ["search_type"],
    searchInput,
  )

  // The URL param is "author" not "authors".
  tcbParamData.author = tcbParamData.authors
  delete tcbParamData.authors

  // `formationBase` is now `formation` in the params.
  tcbParamData.formation = tcbParamData.formationBase
  delete tcbParamData.formationBase

  const rawParams = new URLSearchParams(tcbParamData).toString()
  const params = windowsNewlines(rawParams)
  return tcbBaseUrl + "index.php?" + params + "&show_all"
}

/**
 * Returns a string using Windows ("%0D%0A") newlines, rather
 * than Linux/Unix/Mac ("%0A") newlines.
 */
const windowsNewlines = (str: string): string =>
  str.match(/%0D%0A/g) ? str : str.replace(/%0A/g, "%0D%0A")
