/*
Copyright 2020, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

import { debounce } from "lodash"
import {
  action,
  observable,
  makeObservable,
  reaction,
  IReactionDisposer,
} from "mobx"
import { nanoid } from "nanoid"
import { toBackground } from "./inter-process-communication"

import { RootStore } from "./root-store"

export interface DanceAttribute {
  id: string
  name: string
  description: string
  showInGrid: boolean
  values: DanceAttributeValue[]
  // TODO: multi-type-attributes
  // inputType: DanceAttributeInputType
}

// TODO: multi-type-attributes
// type DanceAttributeInputType = "checkbox" | "menu" | "field" | "field-with-menu"

export interface DanceAttributeValue {
  id: string
  val: string
  label: string
  isDefault: boolean
}

export const makeDefaultAttribute = (): DanceAttribute => ({
  id: nanoid(),
  name: "",
  description: "",
  showInGrid: true,
  values: [
    {
      id: nanoid(),
      val: "Y",
      label: "Yes",
      isDefault: false,
    },
  ],
  // TODO: multi-type-attributes
  // inputType: "field-with-menu",
})

const makeDefaultAttributeValue = (): DanceAttributeValue => ({
  id: nanoid(),
  val: "",
  label: "",
  isDefault: false,
})

export class DanceAttributesStore {
  rootStore: RootStore
  attributes: DanceAttribute[]
  saveHandler: null | IReactionDisposer = null

  constructor(
    rootStore: RootStore,
    attributes: DanceAttribute[],
    forNewAttribute: boolean,
  ) {
    makeObservable(this, {
      attributes: observable,
      addAttribute: action.bound,
      removeAttribute: action.bound,
      updateAttribute: action.bound,
      addValue: action.bound,
      removeValue: action.bound,
      updateValue: action.bound,
      updateDefaultValue: action.bound,
    })

    this.rootStore = rootStore
    this.attributes = attributes

    // Observe everything that is used in the JSON, and save it to the database
    // when it changes.
    if (!forNewAttribute) {
      this.saveHandler = reaction(
        () => this.asJson(),
        (json) => this.debouncedSetDanceAttributes(json),
      )
    }
  }

  asJson() {
    return JSON.stringify(this.attributes)
  }

  debouncedSetDanceAttributes = debounce(
    (json) => toBackground.setDanceAttributes(json),
    1000,
  )

  addAttribute(attribute: DanceAttribute) {
    this.attributes.push(attribute)
  }

  removeAttribute(attributeIndex: number) {
    this.attributes.splice(attributeIndex, 1)
  }

  updateAttribute(index: number, patch: Partial<DanceAttribute>) {
    const attribute = this.attributes[index]
    Object.assign(attribute, patch)
  }

  addValue(index: number) {
    this.attributes[index].values.push(makeDefaultAttributeValue())
  }

  removeValue(attributeIndex: number, valueIndex: number) {
    const attribute = this.attributes[attributeIndex]
    const value = attribute.values[valueIndex]

    attribute.values.splice(valueIndex, 1)

    if (value.isDefault) {
      attribute.values[0].isDefault = true
    }
  }

  updateValue(
    attributeIndex: number,
    valueIndex: number,
    patch: Partial<DanceAttributeValue>,
  ) {
    const attribute = this.attributes[attributeIndex]
    const value = attribute.values[valueIndex]
    Object.assign(value, patch)
  }

  updateDefaultValue(attributeIndex: number, valueIndexClicked: number) {
    const attribute = this.attributes[attributeIndex]

    if (attribute.values[valueIndexClicked].isDefault) {
      attribute.values[valueIndexClicked].isDefault = false
    } else {
      attribute.values = attribute.values.map((value, index) => {
        value.isDefault = index === valueIndexClicked
        return value
      })
    }
  }
}
