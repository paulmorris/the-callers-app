/*
Copyright 2021 Paul Morris

This file is part of The Caller's Application.

The Caller's Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Application.  If not, see <https://www.gnu.org/licenses/>.
*/

import React, { useCallback } from "react"
import { isNotNaN } from "../../shared/utils"
import { debounce } from "lodash"

/**
 * A hook to provide drag and drop functionality.
 *
 * @param originalItems Array of items to be rendered as draggable elements.
 * @param onDropCallback Function to be called on successful drop of an item.
 * @return An object containing:
 * - An array of items to display (either the transient "during dragging" state
 * or the original array of items).
 * - An object representing the current drag state.
 * - A function that takes a position/index and returns an object of attributes
 * to be set on the draggable elements, e.g. the various drag event handlers.
 */
export function useDragAndDrop<T>(
  originalItems: T[],
  onDropCallback: (newIndex: number, oldIndex: number) => Promise<void>,
) {
  interface State {
    from: number
    to: number
    items: T[]
    draggingOver: boolean
  }

  const [state, setState] = React.useState<State>({
    // Dummy values to avoid nulls, real values assigned in onDragStart.
    from: 0,
    to: 0,
    items: [],
    draggingOver: false,
  })

  const onDragStart = (event: React.DragEvent<HTMLElement>) => {
    const from = Number(event.currentTarget.dataset.position)
    if (isNotNaN(from)) {
      setState({
        from,
        to: from,
        items: originalItems,
        draggingOver: true,
      })
    }
  }

  const onDragEnterOrOver = (event: React.DragEvent<HTMLElement>) => {
    // For the onDrop event to fire, we have to cancel this one.
    event.preventDefault()
    debouncedDragLeave.cancel()

    const to = Number(event.currentTarget.dataset.position)
    if (isNotNaN(to)) {
      if (!state.draggingOver || to !== state.to) {
        // Because there may be more than one instance of an item, recreate
        // the current state starting from the original items. Remove the item
        // being dragged and then add it back at the current `to`.
        const newItems = originalItems.filter((_, i) => i !== state.from)
        newItems.splice(to, 0, originalItems[state.from])

        setState({
          ...state,
          to,
          items: newItems,
          draggingOver: true,
        })
      }
    }
  }

  const onDrop = async (event: React.DragEvent<HTMLElement>) => {
    event.stopPropagation()
    debouncedDragLeave.cancel()
    try {
      const to = Number(event.currentTarget.dataset.position)
      if (isNotNaN(to) && to !== state.from) {
        await onDropCallback(to, state.from)
      }
    } catch (e) {
      console.error("Error dragging an item:", e)
    } finally {
      setState({ ...state, draggingOver: false })
    }
  }

  const onDragEnd = (event: React.DragEvent<HTMLElement>) => {
    if (event.dataTransfer.dropEffect === "none") {
      // A dropEffect of "none" means not dropped on a drop target.
      debouncedDragLeave.cancel()
      setState({ ...state, draggingOver: false })
    }
  }

  const debouncedDragLeave = useCallback(
    debounce(() => {
      setState({ ...state, draggingOver: false })
    }, 150),
    [state],
  )

  // When an item is being dragged, state.items is the transient array with the
  // current order to be shown in the UI. Otherwise, the original items in the
  // original order should be shown, i.e. the current state of the database.
  const items = state.draggingOver ? state.items : originalItems

  return {
    items,
    dragState: state,
    getDragAttributes(position: number) {
      return {
        "data-position": position,
        draggable: true,
        onDragStart,
        onDragEnter: onDragEnterOrOver,
        onDragOver: onDragEnterOrOver,
        onDrop,
        onDragEnd,
        onDragLeave: debouncedDragLeave,
      }
    },
  }
}
